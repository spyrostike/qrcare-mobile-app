
import { useStoreActions, useStoreState, useStoreDispatch } from 'easy-peasy'

export default class {
    // authentication = useStoreState(state => state.register.authentication)
    setAuthenticationAction = useStoreActions(actions => actions.register.setAuthentication)
    setProfileAction = useStoreActions(actions => actions.register.setProfile)
    setLocationAction = useStoreActions(actions => actions.register.setLocation)
    setCreatureInfoAction = useStoreActions(actions => actions.register.setCreatureInfo)
    setCreatureMDInfoAction = useStoreActions(actions => actions.register.setCreatureMDInfo)

    setAuthentication = (value) => {
        this.setAuthenticationAction(value)
    }

    setProfile = (value) => {
        this.setProfileAction(value)
    }

    setLocation = (value) => {
        this.setLocationAction(value)
    }

    setCreatureInfo = (value) => {
        this.setCreatureInfoAction(value)
    }

    setCreatureMDInfo = (value) => {
        this.setCreatureMDInfoAction(value)
    }
}