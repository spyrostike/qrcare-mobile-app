import { API_IMAGE_PATH } from '../../appConfig'

export const genenrateFileObjToServer = (payload) => {
    var formData = new FormData()

    if (payload.id) { formData.append('id', payload.id) }
    if (payload.refId) { formData.append('refId', payload.refId) }
    formData.append('tag', payload.tag)
    formData.append('type', 'image')

    formData.append('file-data', {
        uri: payload.uri,
        name: payload.name,
        type: payload.type
    })

    return formData
}

export const convertFileObjFromServerToApp = (payload) => {
    return {
        id: payload.id,
        path: payload.path,
        name: payload.name,
        type: payload.type,
        sub_type: payload.sub_type,
        size: payload.size,
        refId: payload.ref_id,
        tag: payload.tag,
        status: payload.status,
        createDate: payload.create_date,
        createBy: payload.create_by,
        editDate: payload.edit_date,
        editBy: payload.edit_by,
        src: API_IMAGE_PATH + '/' + payload.path + '/' + payload.name
    }
}

export const genenrateRegisterObjToServer = (qrcodeItem, authentication, profile, location) => {
    return {
        qrcodeSetId: qrcodeItem.set_id,
        qrcodeItemId: qrcodeItem.id,
        username: authentication.username,
        password: authentication.password,
        firstName: profile.firstName,
        lastName: profile.lastName,
        gender: profile.gender,
        email: profile.email,
        bloodType: profile.bloodType,
        birthDate: profile.birthDate,
        relationship: profile.relationship,
        identificationNo: profile.identificationNo,
        contactNo1: profile.contactNo1,
        contactNo2: profile.contactNo2,
        address1: profile.address1,
        address2: profile.address2,
        address3: profile.address3,
        placeLatitude: location.coordinate.latitude,
        placeLongitude: location.coordinate.longitude,
        profileImageId: profile.profileImageId,
        locationImageId: location.locationImageId
    }
}

export const convertMemberProfileFromServerToApp = (payload) => {

    const profile = {
        id: payload.id,
        firstName: payload.first_name,
        lastName: payload.last_name,
        gender: payload.gender,
        genderName: payload.gender_name,
        email: payload.email,
        bloodType: payload.blood_type,
        bloodTypeName: payload.blood_type_name,
        birthDate: payload.birth_date,
        relationship: payload.relationship,
        identificationNo: payload.identification_no,
        contactNo1: payload.contact_no1,
        contactNo2: payload.contact_no2,
        address1: payload.address1,
        address2: payload.address2,
        address3: payload.address3,
        profileImageId: payload.profile_image_id,
        profileImage: API_IMAGE_PATH + '/' + payload.profile_image_path+ '/' + payload.profile_image_name,
        locationImageId: payload.location_image_id,
        locationImage: API_IMAGE_PATH + '/' + payload.location_image_path + '/' + payload.location_image_name,
        status: payload.status
    }

    const authentication = {
        username: payload.username
    }

    const location = {
        coordinate: {
            latitude: payload.place_latitude,
            longitude: payload.place_longitude
        }
    }

    
    return {
        authentication: authentication,
        profile: profile,
        location: location
    }
}

export const genenrateCreatureObjToServer = (qrcodeItem, creature) => {
    
    return {
        qrcodeSetId: qrcodeItem.set_id,
        qrcodeItemId: qrcodeItem.id,
        firstName: creature.firstName,
        lastName: creature.lastName,
        gender: creature.gender,
        bloodType: creature.bloodType,
        birthDate: creature.birthDate,
        weight: creature.weight,
        height: creature.height,
        creatureImageId: creature.creatureImageId,
        birthCertificateId: creature.birthCertificateId
    }
}

export const convertCreatureProfileFromServerToApp = (payload) => {
    
    return {
        id: payload.id,
        firstName: payload.first_name,
        lastName: payload.last_name,
        gender: payload.gender,
        bloodType: payload.blood_type,
        birthDate: payload.birth_date,
        weight: payload.weight.toString(),
        height: payload.height.toString(),
        creatureImageId: payload.creatureImageId,
        creatureImage: payload.creatureImage ? API_IMAGE_PATH + '/' + payload.creatureImage : null,
        birthCertificateId: payload.birthCertificateId,
        birthCertificate: payload.birthCertificate ? API_IMAGE_PATH + '/' + payload.birthCertificate : null,
        genderName: payload.gender_name,
        bloodTypeName: payload.blood_type_name,
        memberId: payload.member_id
    }
}

export const convertTreatmentHistoryFromServerToApp = (payload) => {

    const result = {
        id: payload.id,
        date: payload.date,
        time: payload.time,
        description: payload.description,
        department: payload.department,
        fileCategory: payload.file_category,
        fileCategoryOther: payload.file_category_other,
        fileCategoryName: payload.file_category_name,
        departmentName: payload.department_name,
        creatureId: payload.creatureId,
        imageId: payload.imageId,
        image: payload.image ? API_IMAGE_PATH + '/' + payload.image : null,
        imageCount: payload.imageCount
    }
   
    return result
}

export const convertMedicalFromServerToApp = (payload) => {

    if (typeof payload === 'undefined' || payload === null) {
        return {
            id: null,
            hospitalName: null,
            hospitalTel: null, 
            patientId: null, 
            bloodType: 1,
            bloodTypeName: null,
            doctorName: null, 
            doctorContactNo: null,
            congenitalDisorder: null,
            creatureId: null,
            birthCertificate: null
        }
    } else {
        return {
            id: payload.id ,
            hospitalName: payload.hospital_name,
            hospitalTel: payload.hospital_tel, 
            patientId: payload.patient_id, 
            bloodType: payload.blood_type, 
            bloodTypeName: payload.blood_type_name,
            doctorName: payload.doctor_name, 
            doctorContactNo: payload.doctor_contact_no,
            congenitalDisorder: payload.congenital_disorder,
            creatureId: payload.creature_id,
            birthCertificate: payload.birth_certificate_image_path && payload.birth_certificate_image_name ? API_IMAGE_PATH + '/' + payload.birth_certificate_image_path + '/' + payload.birth_certificate_image_name : null
        }
    }
}

export const convertVaccineBookFromServerToApp = (payload) => {
    return {
        id: payload.id,
        vaccineType: payload.vaccine_type,
        vaccineTypeName: payload.vaccine_type_name,
        other: payload.other
    }
}

export const convertVaccineFromServerToApp = payload => {
    return {
        id: payload.id ,
        hospitalName: payload.hospital_name,
        date: payload.date,
        vaccineBookId: payload.vaccineBookId,
        imageId: payload.imageId,
        image: payload.image ? API_IMAGE_PATH + '/' + payload.image : null,
        description: payload.description
    }
}

export const convertDrugFromServerToApp = payload => {
    return {
        id: payload.id ,
        name: payload.name,
        description: payload.description,
        creatureId: payload.creature_id,
        imageId: payload.imageId,
        image: payload.image ? API_IMAGE_PATH + '/' + payload.image : null
    }
}