import { storeData, retrieveData, removeData } from './Storage'

export const setUserToken = async (token) => {
    try {
        return await storeData('@UserToken', JSON.stringify(token))
    } catch (e) {
        return null
    }
}

export const getUserToken = async () => {
    try {
        return JSON.parse( await retrieveData('@UserToken'))
    } catch (e) {
        return null
    }
}

export const removeUserToken = async () => {
    try {
        return await removeData('@UserToken')
    } catch (e) {
        return false
    }
}
