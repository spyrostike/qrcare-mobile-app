import AsyncStorage from '@react-native-community/async-storage'

export const storeData = async (key, value) => {
    try {
        await AsyncStorage.setItem(key, value);
        return true
    } catch (error) {
        return false
        // Error saving data
    }
}

export const retrieveData = async (key) => {
    try {
        return await AsyncStorage.getItem(key);
    } catch (error) {
        return false
        // Error retrieving data
    }
}

export const removeData = async (key) => {
    try {
        await AsyncStorage.removeItem(key)
        return true;
    }
    catch(exception) {
        return false;
    }
}