import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch'
import { Transition } from 'react-native-reanimated'
import Navigator from './services/Navigator'

import  IHeaderApp from './components/UI/header/IHeaderApp'

import DescriptionMore from './scenes/description/DescriptionMore'

import Splash from './scenes/splash/Index'
import Entrance from './scenes/entrance/Index'
import Scan from './scenes/scan/Index'

import MemberAuthentication from './scenes/member/MemberAuthentication'
import MemberChangePassword from './scenes/member/MemberChangePassword'
import MemberForgetPassword from './scenes/member/MemberForgetPassword'
import MemberProfile from './scenes/member/MemberProfile'
import MemberLocation from './scenes/member/MemberLocation'

import CreaturePreview from './scenes/creature/CreaturePreview'
import CreatureList from './scenes/creature/CreatureList'
import CreatureInfo from './scenes/creature/CreatureInfo'

import DrugList from './scenes/drug/DrugList'
import DrugInfo from './scenes/drug/DrugInfo'

import DrugAllergyList from './scenes/drug-allergy/DrugAllergyList'
import DrugAllergyInfo from './scenes/drug-allergy/DrugAllergyInfo'

import FoodAllergyList from './scenes/food-allergy/FoodAllergyList'
import FoodAllergyInfo from './scenes/food-allergy/FoodAllergyInfo'

import MedicalInfo from './scenes/medical/MedicalInfo'

import SettingList from './scenes/setting/SettingList'

import TreatmentHistoryList from './scenes/treatment-history/TreatmentHistoryList'
import TreatmentHistoryInfo from './scenes/treatment-history/TreatmentHistoryInfo'
import TreatmentHistoryEmail from './scenes/treatment-history/TreatmentHistoryEmail'
import TreatmentHistoryImageList from './scenes/treatment-history/TreatmentHistoryImageList'

import VaccineBookList from './scenes/vaccine/VaccineBookList'
import VaccineBookInfo from './scenes/vaccine/VaccineBookInfo'
import VaccineInfo from './scenes/vaccine/VaccineInfo'

const SplashNavigator = createStackNavigator(
    {
        Splash: {
            screen: Splash,
        },
    },
    { 
        initialRouteName: 'Splash',
        headerMode: 'none'
    }
)

const EntranceStackNavigator = createStackNavigator(
    {
        Entrance: {
            screen: Entrance,
        },
        EntranceScan: {
            screen: Scan
        }
    },
    { 
        mode: 'modal',
        initialRouteName: 'Entrance',
        headerMode: 'none'
        // defaultNavigationOptions: ({ navigation }) => ({
        //     header: <IHeaderApp />
        // })
    }
)

const ViewOnlyNavigator = createStackNavigator(
    {
        ViewOnlyCreaturePreview: {
            screen: CreaturePreview,
            params: { viewOnly: true }
        },
        ViewOnlyDrugList: {
            screen: DrugList,
            params: { viewOnly: true }
        },
        ViewOnlyDrugInfo: {
            screen: DrugInfo,
            params: { viewOnly: true }
        },
        ViewOnlyTreatmentHistoryList: {
            screen: TreatmentHistoryList,
            params: { viewOnly: true }
        },
        ViewOnlyTreatmentHistoryInfo: {
            screen: TreatmentHistoryInfo,
            params: { viewOnly: true }
        },
        ViewOnlyTreatmentHistoryImageList: {
            screen: TreatmentHistoryImageList,
            params: { viewOnly: true }
        },
        ViewOnlyVaccineBookList: {
            screen: VaccineBookList,
            params: { viewOnly: true }
        },
        ViewOnlyVaccineBookInfo: {
            screen: VaccineBookInfo,
            params: { viewOnly: true }
        },
        ViewOnlyVaccineInfo: {
            screen: VaccineInfo,
            params: { viewOnly: true }
        },
        ViewOnlyDrugAllergyList: {
            screen: DrugAllergyList,
            params: { viewOnly: true }
        },
        ViewOnlyDrugAllergyInfo: {
            screen: DrugAllergyInfo,
            params: { viewOnly: true }
        },
        ViewOnlyFoodAllergyList: {
            screen: FoodAllergyList,
            params: { viewOnly: true }
        },
        ViewOnlyFoodAllergyInfo: {
            screen: FoodAllergyInfo,
            params: { viewOnly: true }
        },
        ViewOnlyMedicalInfo: {
            screen: MedicalInfo,
            params: { viewOnly: true }
        },
        ViewOnlyParentInfo: {
            screen: MemberProfile,
            params: { viewOnly: true }
        },
        ViewOnlyLocationInfo: {
            screen: MemberLocation,
            params: { viewOnly: true }
        },
        ViewOnlyDescriptionMore: {
            screen: DescriptionMore
        }
    },
    { 
        initialRouteName: 'ViewOnlyCreaturePreview',
        defaultNavigationOptions: ({ navigation }) => ({
            header: <IHeaderApp />
        }),
        transitionConfig: () => Platform.OS === 'android' ? ({
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene
                const width = layout.initWidth
            
                return {
                    opacity: position.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [ 0, 1, 0]
                    }),
                    transform: [{
                        translateX: position.interpolate({
                            inputRange: [index - 1, index, index + 1],
                            outputRange: [-width, 0, width]
                        })
                    }]
                }
            }
        }) : null
    } 
)

const VisitorStackNavigator = createStackNavigator(
    {
        VisitorMemberAuthentication: {
            screen: MemberAuthentication
        },
        VisitorMemberForgetPassword: {
            screen: MemberForgetPassword
        },
        VisitorMemberProfile: {
            screen: MemberProfile
        },
        VisitorMemberLocation: {
            screen: MemberLocation
        }
    },
    { 
        initialRouteName: 'VisitorMemberAuthentication',
        headerMode: 'none'
    }
)

const MemberStackNavidator = createStackNavigator(
    {
        MemberCreatureList: {
            screen: CreatureList
        },
        MemberCreaturePreview: {
            screen: CreaturePreview
        },
        MemberCreatureInfo: {
            screen: CreatureInfo
        },
        MemberMedicalInfo: {
            screen: MedicalInfo
        }, 
        MemberDrugList: {
            screen: DrugList
        },
        MemberDrugInfo: {
            screen: DrugInfo
        },
        MemberDrugAllergyList: {
            screen: DrugAllergyList
        },
        MemberDrugAllergyInfo: {
            screen: DrugAllergyInfo
        },
        MemberFoodAllergyList: {
            screen: FoodAllergyList
        },
        MemberFoodAllergyInfo: {
            screen: FoodAllergyInfo
        },
        MemberTreatmentHistoryList: {
            screen: TreatmentHistoryList,
        },
        MemberTreatmentHistoryInfo: {
            screen: TreatmentHistoryInfo
        },
        MemberTreatmentHistoryEmail: {
            screen: TreatmentHistoryEmail
        },
        MemberTreatmentHistoryImageList: {
            screen: TreatmentHistoryImageList
        },
        MemberScan: {
            screen: Scan,
        },
        MemberSettingList: {
            screen: SettingList
        },
        MemberVaccineBookList: {
            screen: VaccineBookList
        },
        MemberVaccineBookInfo: {
            screen: VaccineBookInfo
        },
        MemberVaccineInfo: {
            screen: VaccineInfo
        },
        MemberProfile: {
            screen: MemberProfile
        },
        MemberLocation: {
            screen: MemberLocation
        },
        MemberChangePassword: {
            screen: MemberChangePassword
        },
        MemberDescriptionMore: {
            screen: DescriptionMore
        }
    },
    { 
        initialRouteName: 'MemberCreatureList',
        defaultNavigationOptions: ({ navigation }) => ({
            header: <IHeaderApp />
        }),
        transitionConfig: () => Platform.OS === 'android' ? ({
            screenInterpolator: sceneProps => {
              const { layout, position, scene } = sceneProps;
              const { index } = scene;
              const width = layout.initWidth;
        
              return {
                opacity: position.interpolate({
                  inputRange: [index - 1, index, index + 1],
                  outputRange: [ 0, 1, 0],
                }),
                transform: [{
                  translateX: position.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [-width, 0, width],
                  }),
                }]
              };
            }
        }) : null
    }
)

const SwitchNavigator = createAnimatedSwitchNavigator(
    {
        Splash: SplashNavigator,
        Entrance: EntranceStackNavigator,
        ViewOnly: ViewOnlyNavigator,
        Visitor: VisitorStackNavigator,
        Member: MemberStackNavidator
    },
    {
        initialRouteName: 'Splash',
        transition: (
            <Transition.Together>
                <Transition.Out
                    type='slide-left'
                    durationMs={100}
                    interpolation='linear' />
                <Transition.In type='fade' durationMs={100} />
            </Transition.Together>
        ) 
    }
)

const AppContainer = createAppContainer(SwitchNavigator)

class AppNavigator extends React.Component {
    render () {  
        return (
            <AppContainer ref={ navigatorRef => { Navigator.setContainer(navigatorRef) } } />
        )
    }
}

export default AppNavigator