import React, { useState } from 'react'
import { Alert } from 'react-native'
import SettingListView from './SettingListView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const { viewModel } = props
    
    const _listItems = [
        {   
            title: 'version 0.0.23 (190620)', 
            iconName: 'tablet-mobile-combo',
            iconType: 'entypo',
            iconStyle: { width: 25, height: 25, alignSelf: 'center', color: '#FFFFFF' },
            size: 50,
            methodType: 'function',
            methodName: null,
            screen: null
        },
        {   
            title: 'Profile', 
            iconName: 'account-card-details',
            iconType: 'material-community',
            iconStyle: { width: 25, height: 25, alignSelf: 'center', color: '#FFFFFF' },
            size: 50,
            methodType: 'new-scene',
            methodName: null,
            screen: 'MemberProfile'
        },
        {   
            title: 'Location', 
            iconName: 'location-on',
            iconType: 'material-icons',
            iconStyle: { width: 25, height: 25, alignSelf: 'center', color: '#FFFFFF' },
            size: 50,
            methodType: 'new-scene',
            methodName: null,
            screen: 'MemberLocation'
        },
        {   
            title: 'Change password', 
            iconName: 'key',
            iconType: 'foundation',
            iconStyle: { width: 25, height: 25, alignSelf: 'center', color: '#FFFFFF' },
            size: 50,
            methodType: 'new-scene',
            methodName: null,
            screen: 'MemberChangePassword'
        },
        {   
            title: 'Logout', 
            iconName: 'power-off',
            iconType: 'font-awesome',
            iconStyle: { width: 25, height: 25, alignSelf: 'center', color: '#FFFFFF' },
            size: 50,
            methodType: 'function',
            methodName: 'logout',
            screen: 'Entrance'
        }
    ]

    const _onPress = async (item) => {
        if (item.methodType === 'function') {
            if (item.methodName === 'logout') {
                _confirmAlertDialog(item)
            } 
        } else if (item.methodType === 'new-scene') {
            _gotoScreen(item)
        }
    }

    const _confirmAlertDialog = (item) => Alert.alert(
        'Announcer',
        'Are you sure to logout?',
        [
            {text: 'cancel', onPress: () => {}, style: 'cancel'},
            {text: 'confirm', onPress: () => _logout(item)}
        ],
        { cancelable: false }
    )

    const _gotoScreen = item => {
        Navigator.navigate(item.screen, { methodType: 'edit' })
    }

    const _logout = async (item) => {

        const isRemove = await viewModel.logout()

        isRemove ? Navigator.navigate(item.screen) : null
    }

    return (    
        <SettingListView list={_listItems} onPress={_onPress} />
    )
} 
