import React from 'react'
import SettingListController from './SettingListController'
import SettingListViewModel from './SettingListViewModel'
import SettingListAction from './SettingListAction'

export default () => {
    const action = new SettingListAction()
    const viewModel = new SettingListViewModel(action)

    return (
        <SettingListController viewModel={viewModel} />
    )
} 
