import React from 'react'
import SettingListForm from '../UI/setting/SettingListForm'

export default (props) => {

    const { list, onPress } = props

    return (
        <SettingListForm items={list} onPress={onPress} />
    )
} 
