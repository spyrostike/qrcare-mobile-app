import { removeUserToken } from '../../services/Token'

export default class {
    
    constructor(store) {
        this.store = store
    }

    logout = async () => {
        return await removeUserToken()
    }

}
