import { useStoreActions } from 'easy-peasy'

export default class {
    addTreatmentHistoryAction = useStoreActions(actions => actions.treatmentHistory.addTreatmentHistory)
    editTreatmentHistoryAction = useStoreActions(actions => actions.treatmentHistory.editTreatmentHistory)
    getTreatmentHistoryByIdAction = useStoreActions(actions => actions.treatmentHistory.getTreatmentHistoryById)
    addFileAction = useStoreActions(actions => actions.file.addFile)
    setFilesRefIdAction = useStoreActions(actions => actions.file.setFilesRefId)

    addTreatmentHistory = (payload) => this.addTreatmentHistoryAction(payload)

    editTreatmentHistory = payload => this.editTreatmentHistoryAction(payload)

    getTreatmentHistoryById = (payload) => this.getTreatmentHistoryByIdAction(payload)

    addFile = payload => this.addFileAction(payload)

    setFilesRefId = payload => this.setFilesRefIdAction(payload)
}