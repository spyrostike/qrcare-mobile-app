import { genenrateFileObjToServer } from '../../utils/ObjectUtil'

export default class {
    
    constructor(store) {
        this.store = store
    }

    getTreatmentHistory = () => {
        return this.store.getTreatmentHistory()
    }

    
    addTreatmentHistory = payload => {
        return new Promise((resolve, reject) => {
            this.store.addTreatmentHistory(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editTreatmentHistory = payload => {
        return new Promise((resolve, reject) => {
            this.store.editTreatmentHistory(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getTreatmentHistoryById = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getTreatmentHistoryById(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    addFile = (payload) => {
        const formData = genenrateFileObjToServer(payload)
        return new Promise((resolve, reject) => {
            this.store.addFile(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    setFilesRefId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.setFilesRefId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
