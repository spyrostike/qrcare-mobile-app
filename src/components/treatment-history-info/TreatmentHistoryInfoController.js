import React, { useMemo, useState } from 'react'
import moment from 'moment'
import TreatmentHistoryInfoView from './TreatmentHistoryInfoView'
import Navigator from '../../services/Navigator'
import dayjs from 'dayjs'

export default (props) => {
    const [ id, setId ] = useState(null)
    const [ date, setDate ] = useState(dayjs().format('DD/MM/YYYY'))
    const [ time, setTime ] = useState(dayjs().format('HH:mm'))
    const [ image, setImage ] = useState(null)
    const [ imageId, setImageId ] = useState(null)
    const [ imageCount, setImageCount ] = useState(null)
    const [ oldImageId, setOldImageId ] = useState(null)
    const [ isImageChange, setIsImageChange ] = useState(false)
    const [ imageList, setImageList ] = useState([])
    const [ description, setDescription ] = useState(null)
    const [ department, setDepartment ] = useState(1)
    const [ fileCategory, setFileCategory] = useState(1)
    const [ fileCategoryOther, setFileCategoryOther] = useState(null)
    const [ departmentName, setDepartmentName ] = useState(null)
    const [ fileCategoryName, setFileCategoryName] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ dialogMessage, setDialogMessage ] = useState(null)
    const [ isEdit, setIsEdit ] = useState(null)

    const { routes, index } = Navigator.getCurrentRoute()
    let { methodType, creatureId, treatmentHistoryId, onComplete, viewOnly } = routes[index].params

    const { viewModel } = props

    const _getTreatmentHistoryById = async (updateImageItemsOnly = false, idParam) => {
        let idVM = null
        idParam ? idVM = idParam : idVM = treatmentHistoryId ? treatmentHistoryId : id

        try {
            if (methodType === 'add' && idVM === null) { setIsEdit(true); return; }
            if (typeof idVM === 'undefined' || idVM === null) { setDialogMessage('ID not found'); return; } 
            setIsLoading(true)
            const result = await viewModel.getTreatmentHistoryById(idVM)
            updateImageItemsOnly ? _updateImageItemsOnly(result) : _prepareData(result)
        } catch (error) {
            setDialogMessage(error)
        }
        setIsLoading(false)
    }

    useMemo(() => {
        _getTreatmentHistoryById()
    }, [])

    const _prepareData = (data) => {  
        const { id, date, time, image, imageId, imageCount, description, department, fileCategory, fileCategoryOther, departmentName, fileCategoryName } = data

        setId(id)
        setDate(dayjs(date, 'YYYY/MM/DD').format('DD/MM/YYYY'))
        setTime(moment(time, 'HH:mm:ss').format('HH:mm'))
        setImage(image)
        setImageId(imageId)
        setImageCount(imageCount)
        setOldImageId(imageId)
        setDescription(description)
        setDepartment(department)
        setFileCategory(fileCategory)
        setFileCategoryOther(fileCategoryOther)
        setDepartmentName(departmentName)
        setFileCategoryName(fileCategoryName)
    }

    const _updateImageItemsOnly = (data) => {  
        const { imageCount } = data
        setImageCount(imageCount)
    }

    const _onSubmit = () => {
        if (!_validate()) return
        
        let data = {
            id: id,
            date: moment(date, 'DD/MM/YYYY').format(),
            time: time,
            imageId: imageId,
            oldImageId: isImageChange ? oldImageId : null,
            description: description,
            department: department,
            fileCategory: fileCategory,
            fileCategoryOther: fileCategoryOther,
            creatureId: creatureId
        }

        if (id === null) {
            _addTreatmentHistory(data)
        } else {
            _editTreatmentHistory(data)
        }
    }

    const _addTreatmentHistory = async (data) => {
        setIsLoading(true)

        try {
            const result = await viewModel.addTreatmentHistory(data)
            setId(result.id)
            
            if (imageList.length) {
                const data = {
                    refId: result.id,
                    ids: imageList
                }
                
                await viewModel.setFilesRefId(data)
            }
            // await _getTreatmentHistoryById(false, result.id)
    
            setIsLoading(false)
            onComplete()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    } 

    const  _editTreatmentHistory = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.editTreatmentHistory(data)
            // await _getTreatmentHistoryById()
            setIsLoading(false)
            onComplete()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _validate = () => {
        if (imageId === null) { setErrorMessage('Image is required'); return false; }
        // if (date === null) return false
        // if (time === null) return false
        // if (description === null || description.trim().length === 0) return false
        if (department === null) { setErrorMessage('Department is required'); return false; }
        if (fileCategory === null) { setErrorMessage('File category is required'); return false; }
        if (fileCategory === 5 && (fileCategoryOther === null || fileCategoryOther.length === 0)) { setErrorMessage('Other is required'); return false; }

        return true
    }

    const _onRefresh = () => {
        setDialogMessage(null)
        setTimeout(() => {
            _getTreatmentHistoryById()
        }, 300)
    }

    const _onCancel = () => {
        setDialogMessage(null)
        Navigator.back()
    }

    const _setImage = async (image) => {
        setIsLoading(true)

        try {
            const data = {
                name: 'treatment-history.js',
                tag: 'treatment-history-image',
                type: image.mime,
                uri: image.path
            }
            
            const file = await viewModel.addFile(data)
            
            setImageId(file.id)
            setImage(file.src)
            setIsImageChange(true)

            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    _goToScreen = (screen) => {
        Navigator.navigate(screen, { 
            treatmentHistoryId: id,
            setImageCount: (value) => setImageCount(value),
            methodType: 'edit',
            onComplete: async () => {
                Navigator.back()
            }
        })
    }

    const _onAddImage = async (image) => {
        setIsLoading(true)

        try {
            const data = {
                refId: id,
                name: 'treatment-history.js',
                tag: 'treatment-history-image-list',
                type: image.mime,
                uri: image.path
            }
            
            const result = await viewModel.addFile(data)

            if (id === null) { setImageList(oldArray => [ ...oldArray, result.id ]) }
            
            _getTreatmentHistoryById(true)
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }
    
    const _setFileCategory = (value) => {
        setFileCategory(value)
        setFileCategoryOther(null)
    }

    return (
        <TreatmentHistoryInfoView 
            viewOnly={viewOnly}
            methodType={methodType}
            id={id}
            date={date}
            time={time}
            image={image}
            imageCount={imageCount === null || imageCount <= 0 ? 'None' : imageCount + (imageCount > 1 ? ' items' : ' item') }
            description={description}
            department={department}
            fileCategory={fileCategory}
            fileCategoryOther={fileCategoryOther}
            fileCategoryName={fileCategoryName}
            departmentName={departmentName}
            isLoading={isLoading}
            isEdit={isEdit}
            errorMessage={errorMessage}
            dialogMessage={dialogMessage}
            setDate={setDate}
            setTime={setTime}
            setImage={_setImage}
            setDescription={setDescription}
            setDepartment={setDepartment}
            setFileCategory={_setFileCategory}
            setFileCategoryOther={setFileCategoryOther}
            setErrorMessage={setErrorMessage}
            setIsEdit={setIsEdit}
            goToScreen={_goToScreen}
            onSubmit={_onSubmit}
            onConfirm={_onRefresh}
            onCancel={_onCancel}
            onAddImage={_onAddImage} />
    )
} 
