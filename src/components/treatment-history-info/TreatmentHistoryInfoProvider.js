import React from 'react'
import TreatmentHistoryInfoController from './TreatmentHistoryInfoController'
import TreatmentHistoryInfoViewModel from './TreatmentHistoryInfoViewModel'
import TreatmentHistoryInfoAction from './TreatmentHistoryInfoAction'

export default () => {
    const action = new TreatmentHistoryInfoAction()
    const viewModel = new TreatmentHistoryInfoViewModel(action)

    return (
        <TreatmentHistoryInfoController viewModel={viewModel} />
    )
} 
