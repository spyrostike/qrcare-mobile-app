import React from 'react'
import TreatmentInfoForm from '../UI/treatment-history/TreatmentInfoForm'
import TreatmentInfoFormViewOnly from '../UI/treatment-history/TreatmentInfoFormViewOnly'

export default (props) => {
    const { 
        viewOnly,
        methodType,
        id,
        date, 
        time, 
        image, 
        imageCount,
        description, 
        department, 
        fileCategory, 
        fileCategoryOther,
        fileCategoryName,
        departmentName,
        isLoading,
        isEdit,
        dialogMessage,
        errorMessage,
        setDate, 
        setTime, 
        setImage, 
        setDescription, 
        setDepartment, 
        setFileCategory, 
        setFileCategoryOther,
        setErrorMessage, 
        setIsEdit,
        goToScreen,
        onSubmit, 
        onCancel,
        onConfirm,
        onAddImage } = props

    return (
        <React.Fragment >
            {
                viewOnly ? 
                    <TreatmentInfoFormViewOnly 
                        date={date}
                        time={time}
                        image={image} 
                        imageCount={imageCount}
                        description={description}
                        departmentName={departmentName}
                        fileCategory={fileCategory}
                        fileCategoryName={fileCategoryName}
                        fileCategoryOther={fileCategoryOther}
                        isLoading={isLoading}
                        dialogMessage={dialogMessage} 
                        errorMessage={errorMessage}
                        setErrorMessage={setErrorMessage}
                        goToScreen={goToScreen}
                        onConfirm={onConfirm} 
                        onCancel={onCancel} />
                    :
                    <TreatmentInfoForm 
                        methodType={methodType}
                        id={id}
                        date={date}
                        time={time}
                        image={image} 
                        imageCount={imageCount}
                        description={description}
                        department={department}
                        fileCategory={fileCategory}
                        fileCategoryOther={fileCategoryOther}
                        departmentName={departmentName}
                        fileCategoryName={fileCategoryName}
                        isLoading={isLoading}
                        isEdit={isEdit}
                        dialogMessage={dialogMessage} 
                        errorMessage={errorMessage}
                        setDate={setDate}
                        setTime={setTime}
                        setImage={setImage}
                        setDescription={setDescription}
                        setDepartment={setDepartment}
                        setFileCategory={setFileCategory}
                        setFileCategoryOther={setFileCategoryOther}
                        setErrorMessage={setErrorMessage}
                        setIsEdit={setIsEdit}
                        goToScreen={goToScreen}
                        onConfirm={onConfirm} 
                        onCancel={onCancel}
                        onSubmit={onSubmit}
                        onAddImage={onAddImage} />
            }
        </React.Fragment>
    )
} 