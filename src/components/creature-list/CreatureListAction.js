import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    
    creatureList = useStoreState(state => state.creature.list)
    setCreatureListAction = useStoreActions(actions => actions.creature.setList)
    getQRCodeItemByIdAction = useStoreActions(actions => actions.qrcode.getQRCodeItemById)
    getCreatureListByMemberIdAction = useStoreActions(actions => actions.creature.getCreatureListByMemberId)
    getCreatureByQRCodeIdAction = useStoreActions(actions => actions.creature.getCreatureByQRCodeId)
    removeCreatureAction = useStoreActions(actions => actions.creature.removeCreature)
    removeCreaturesAction = useStoreActions(actions => actions.creature.removeCreatures)

    getCreatureList = () => this.creatureList

    setCreatureList = payload => this.setCreatureListAction(payload)

    getQRCodeItemById = value => this.getQRCodeItemByIdAction(value)

    getCreatureListByMemberId = () => this.getCreatureListByMemberIdAction()

    getCreatureByQRCodeId = (payload) => this.getCreatureByQRCodeIdAction(payload)

    removeCreature = (payload) => this.removeCreatureAction(payload)

    removeCreatures = payload => this.removeCreaturesAction(payload)

}