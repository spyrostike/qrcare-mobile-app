import React from 'react'
import CreatureListController from './CreatureListController'
import CreatureListViewModel from './CreatureListViewModel'
import CreatureListAction from './CreatureListAction'

export default () => {
    const action = new CreatureListAction()
    const viewModel = new CreatureListViewModel(action)

    return (
        <CreatureListController viewModel={viewModel}  />
    )
} 
