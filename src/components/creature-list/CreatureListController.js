import React, { useMemo, useState } from 'react'
import CreatureListView from './CreatureListView'
import Navigator from '../../services/Navigator'
import { DOMAIN_URL } from '../../../appConfig'

export default (props) => {
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { viewModel } = props

    let _scanBarcodeEnabled = true

    const _getCreatureListByMemberId = async () => {
        try {
            setIsLoading(true)
            await viewModel.getCreatureListByMemberId()
        } catch (error) {
            setTimeout(() => {
                setErrorMessage(error)
            }, 500)
        }
        setTimeout(() => { setIsLoading(false) }, 500)
       
    }

    useMemo(() => {
        viewModel.setCreatureList([])
        _getCreatureListByMemberId()
    }, [])

    const _onItemPress = (value) => {
        if (value) {
            Navigator.navigate('MemberCreaturePreview', { 
                fromScreen: 'MemberCreatureList', 
                creatureId: value.id,
                onComplete: async () => {
                    Navigator.back()
                    await _getCreatureListByMemberId()
                }
            })
        } else {
            _scanBarcodeEnabled = true
            Navigator.navigate('MemberScan', { 
                methodType: 'create-creature',
                onBarCodeRead: (value) => {
                    if (_scanBarcodeEnabled) {
                        _scanBarcodeEnabled = false
                        setTimeout( () => {
                            _getQRCodeItemById(value)
                        }, 100)
    
                        Navigator.back()
                    }
                } 
            })
        }
        
    }
    const _gotoScanScreen = () => {
        _scanBarcodeEnabled = true
        Navigator.navigate('MemberScan', { 
            methodType: 'create-creature',
            onBarCodeRead: (value) => {
                if (_scanBarcodeEnabled) {
                    _scanBarcodeEnabled = false
                    setTimeout( () => {
                        _getQRCodeItemByIdForViewOnly(value)
                    }, 100)

                    Navigator.back()
                }
            } 
        })
    } 

    const _getQRCodeItemByIdForViewOnly = async (value) => {
        const { data, type } = value
        if (type !== 'QR_CODE' && type !== 'org.iso.QRCode') return

        const qrcodeGUID = data.replace(DOMAIN_URL + '/creature/', '')

        try {
            
            if (!isLoading && !errorMessage) {
                setIsLoading(true)
                const result = await viewModel.getQRCodeItemById(qrcodeGUID)
                const { usage_status } = result

                setIsLoading(false)

                if (usage_status === 1 || usage_status === 2) { 
                    setTimeout(() => {
                        setErrorMessage('Data not found.')
                    }, 500)
                } else if (usage_status === 3) {
                    const creature = await viewModel.getCreatureByQRCodeId(qrcodeGUID)
                    const { id } = creature
                    setIsLoading(false)
                    Navigator.navigate('ViewOnlyCreaturePreview', { fromScreen: 'MemberCreatureList', creatureId: id })
                }

                return
            }
        } catch (error) {
            setErrorMessage(error)
            setIsLoading(false)
        }
    }

    const _getQRCodeItemById = async (value) => {

        const { data, type } = value
        
        if (type !== 'QR_CODE' && type !== 'org.iso.QRCode') return

        const qrcodeGUID = data.replace(DOMAIN_URL + '/creature/', '')

        try {
            
            if (!isLoading && !errorMessage) {
                setIsLoading(true)
                const result = await viewModel.getQRCodeItemById(qrcodeGUID)
                const { usage_status } = result
                
                setIsLoading(false)

                if (usage_status === 1 || usage_status === 2) { 
                    Navigator.navigate('MemberCreatureInfo', {
                        onComplete: async (creatureId) => {
                            Navigator.back()
                            await _getCreatureListByMemberId()
                            // Navigator.navigate('MemberCreaturePreview', { creatureId: creatureId })
                        }
                    })
                } else if (usage_status === 3) {
                    setTimeout(() => {
                        setErrorMessage('QRCode have been already used.')
                    }, 400);
                }

                return
            }
        } catch (error) {
            setErrorMessage(error)
            setIsLoading(false)
        }
    }

    const _removeItem = async (value) => {
        setIsLoading(true)

        try {
            await viewModel.removeCreature(value.id)
            await _getCreatureListByMemberId()
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _convertListForDisplay = () => {
        const list = viewModel.getCreatureList()
        return list.map(item => ({ 
            ...item,
            text1: item.first_name + ' ' + item.last_name
        }))
    }

    return (
        <CreatureListView
            list={_convertListForDisplay()}
            onItemPress={_onItemPress}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setErrorMessage={setErrorMessage}
            removeItem={_removeItem}
            gotoScanScreen={_gotoScanScreen} />
    )
} 
