export default class {
    
    constructor(store) {
        this.store = store
    }

    getCreatureList = () => this.store.getCreatureList()


    setCreatureList = payload => this.store.setCreatureList(payload)

    getQRCodeItemById = id => {
        return new Promise((resolve, reject) => {
            this.store.getQRCodeItemById(id).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getCreatureListByMemberId = () => {
        return new Promise((resolve, reject) => {
            this.store.getCreatureListByMemberId().then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getCreatureByQRCodeId = payload => {
        return new Promise((resolve, reject) => {
            this.store.getCreatureByQRCodeId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeCreature = payload => {
        return new Promise((resolve, reject) => {
            this.store.removeCreature(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeCreatures = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeCreatures(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
