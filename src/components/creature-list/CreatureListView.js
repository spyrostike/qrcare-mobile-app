import React from 'react'
import CreatureList from '../UI/creature/CreatureList'

export default (props) => {
    const {
        list,
        onItemPress,
        isLoading,
        errorMessage,
        setErrorMessage,
        removeItem,
        gotoScanScreen } = props
    
    return (
        <React.Fragment >
            <CreatureList 
                list={list} 
                onItemPress={onItemPress} 
                isLoading={isLoading}
                errorMessage={errorMessage}
                setErrorMessage={setErrorMessage}
                onItemLongPress={removeItem} 
                gotoScanScreen={gotoScanScreen} />
        </React.Fragment>
    )
} 