import React from 'react'
import FoodAllergyInfoForm from '../UI/food-allergy/FoodAllergyInfoForm'
import FoodAllergyInfoFormViewOnly from '../UI/food-allergy/FoodAllergyInfoFormViewOnly'

export default (props) => {
    const { 
        viewOnly,
        methodType,
        image, 
        name, 
        description, 
        isLoading,
        isEdit,
        dialogMessage,
        errorMessage, 
        setImage, 
        setName, 
        setDescription, 
        setErrorMessage, 
        setIsEdit,
        onSubmit, 
        onCancel,
        onConfirm } = props

    return (
        <React.Fragment >
            {
                viewOnly ? 
                    <FoodAllergyInfoFormViewOnly 
                        image={image} 
                        name={name}
                        description={description}
                        isLoading={isLoading}
                        dialogMessage={dialogMessage}
                        errorMessage={errorMessage}
                        setErrorMessage={setErrorMessage}
                        onConfirm={onConfirm}
                        onCancel={onCancel} />
                    :
                    <FoodAllergyInfoForm 
                        methodType={methodType}
                        image={image} 
                        name={name}
                        description={description}
                        isLoading={isLoading}
                        isEdit={isEdit}
                        dialogMessage={dialogMessage}
                        errorMessage={errorMessage}
                        setImage={setImage}
                        setName={setName}
                        setDescription={setDescription}
                        setErrorMessage={setErrorMessage}
                        setIsEdit={setIsEdit}
                        onSubmit={onSubmit}
                        onConfirm={onConfirm}
                        onCancel={onCancel} />
            }
        </React.Fragment>
    )
} 