import { genenrateFileObjToServer } from '../../utils/ObjectUtil'

export default class {
    
    constructor(store) {
        this.store = store
    }

    addFoodAllergy = payload => {
        return new Promise((resolve, reject) => {
            this.store.addFoodAllergy(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editFoodAllergy = payload => {
        return new Promise((resolve, reject) => {
            this.store.editFoodAllergy(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getFoodAllergyById = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getFoodAllergyById(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    addFile = (payload) => {
        const formData = genenrateFileObjToServer(payload)
        return new Promise((resolve, reject) => {
            this.store.addFile(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
