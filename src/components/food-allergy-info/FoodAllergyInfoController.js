import React, { useMemo, useState } from 'react'
import FoodAllergyInfoView from './FoodAllergyInfoView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ id, setId ] = useState(null)
    const [ image, setImage ] = useState(null)
    const [ imageId, setImageId ] = useState(null)
    const [ oldImageId, setOldImageId ] = useState(null)
    const [ isImageChange, setIsImageChange ] = useState(false)
    const [ name, setName ] = useState(null)
    const [ description, setDescription ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ dialogMessage, setDialogMessage ] = useState(null)
    const [ isEdit, setIsEdit ] = useState(null)

    const { routes, index } = Navigator.getCurrentRoute()
    const { methodType, creatureId, foodAllergyId, onComplete, viewOnly } = routes[index].params
    
    const { viewModel } = props

    const _getFoodAllergyById = async () => {
        try {
            if (methodType === 'add') { setIsEdit(true); return; }
            if (typeof foodAllergyId === 'undefined' || foodAllergyId === null) { setDialogMessage('ID not found'); return; } 
            setIsLoading(true)
            const result = await viewModel.getFoodAllergyById(foodAllergyId)
            _prepareData(result)
        } catch (error) {
            setDialogMessage(error)
        }
        setIsLoading(false)
    }

    useMemo(() => {
        _getFoodAllergyById()
    }, [])

    const _prepareData = (data) => {  
        const { id, image, imageId, name, description } = data
        setId(id)
        setImage(image)
        setImageId(imageId)
        setOldImageId(imageId)
        setName(name)
        setDescription(description)
    }

    const _onSubmit = () => {
        if (!_validate()) return

        const data = {
            id: id,
            name: name,
            imageId: imageId,
            oldImageId: isImageChange ? oldImageId : null,
            description: description,
            creatureId: creatureId
        }

        if (id === null) {
            _addFoodAllergy(data)
        } else {
            _editFoodAllergy(data)
        }
    }

    const _addFoodAllergy = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.addFoodAllergy(data)
            setIsLoading(false)
            onComplete()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    } 

    const _editFoodAllergy = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.editFoodAllergy(data)
            setIsLoading(false)
            onComplete()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _validate = () => {
        if (imageId === null) { setErrorMessage('Image is required'); return false; }
        if (name === null || name.trim().length === 0) { setErrorMessage('Name is required'); return false; }
        // if (description === null || description.trim().length === 0) { setErrorMessage('Image is required'); return false; }

        return true
    }

    const _onRefresh = () => {
        setDialogMessage(null)
        setTimeout(() => {
            _getFoodAllergyById()
        }, 300)
        
    }

    const _onCancel = () => {
        setDialogMessage(null)
        Navigator.back()
    }

    const _setImage = async (image) => {
        setIsLoading(true)

        try {
            const data = {
                name: 'food-allergy.js',
                tag: 'food-allergy-image',
                type: image.mime,
                uri: image.path
            }
            
            const file = await viewModel.addFile(data)
            
            setImageId(file.id)
            setImage(file.src)
            setIsImageChange(true)

            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    return (
        <FoodAllergyInfoView 
            viewOnly={viewOnly}
            methodType={methodType}
            image={image}
            name={name}
            description={description}
            isLoading={isLoading}
            isEdit={isEdit}
            errorMessage={errorMessage}
            dialogMessage={dialogMessage}
            setImage={_setImage}
            setName={setName}
            setDescription={setDescription}
            setErrorMessage={setErrorMessage}
            setIsEdit={setIsEdit}
            onSubmit={_onSubmit}
            onConfirm={_onRefresh}
            onCancel={_onCancel} />
    )
} 
