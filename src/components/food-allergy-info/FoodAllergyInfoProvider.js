import React from 'react'
import FoodAllergyInfoController from './FoodAllergyInfoController'
import FoodAllergyInfoViewModel from './FoodAllergyInfoViewModel'
import FoodAllergyInfoAction from './FoodAllergyInfoAction'

export default () => {
    const action = new FoodAllergyInfoAction()
    const viewModel = new FoodAllergyInfoViewModel(action)

    return (
        <FoodAllergyInfoController viewModel={viewModel} />
    )
} 
