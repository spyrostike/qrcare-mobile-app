import { useStoreActions } from 'easy-peasy'

export default class {
    addFoodAllergyAction = useStoreActions(actions => actions.foodAllergy.addFoodAllergy)
    editFoodAllergyAction = useStoreActions(actions => actions.foodAllergy.editFoodAllergy)
    getFoodAllergyByIdAction = useStoreActions(actions => actions.foodAllergy.getFoodAllergyById)
    addFileAction = useStoreActions(actions => actions.file.addFile)

    addFoodAllergy = (value) => this.addFoodAllergyAction(value)

    editFoodAllergy = payload => this.editFoodAllergyAction(payload)

    getFoodAllergyById = (payload) => this.getFoodAllergyByIdAction(payload)

    addFile = payload => this.addFileAction(payload)

}