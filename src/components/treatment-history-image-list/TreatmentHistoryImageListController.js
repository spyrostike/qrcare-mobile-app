import React, { useMemo, useState } from 'react'
import TreatmentHistoryImageListView from './TreatmentHistoryImageListView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ images, setImages ] = useState([])
    const [ checkList, setCheckList ] = useState([])
    const [ offSet, setOffSet ] = useState(0)
    const [ mode, setMode ] = useState(null)
    const [ refreshing, setRefreshing ] = useState(false)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ forceUpdate, setForceUpdate ] = useState(null)

    const { viewModel } = props

    const { routes, index } = Navigator.getCurrentRoute()
    const { treatmentHistoryId, viewOnly, setImageCount } = routes[index].params

    const prefixScreen = viewOnly ? 'ViewOnly' : 'Member'

    const _getFileList = async (offSet) => {
        try {
            setRefreshing(true)
            const data = {
                offset: offSet,
                limit: 15,
                refId: treatmentHistoryId,
                tag: 'treatment-history-image-list'
            }
            const result = await viewModel.getFileList(data) 
    
            if (result.items && result.items.length > 0) {
                let imagesVM = offSet === 0  ? [] : images 
                imagesVM = imagesVM.concat(result.items)
                setImages(imagesVM)

                if (offSet === 0) {
                    _initialCheckList(imagesVM)
                } else {
                    let list = []
                    for (var i = 0;i < imagesVM.length; i++) {
                        list.push(null)
                    }
                    let checkListVM = offSet === 0  ? [] : checkList
                    checkListVM.concat(list)
                    setCheckList(checkListVM)
                }

                setOffSet(offSet + 1)
                setImageCount(result.totalRecords)
            }
            
            
        } catch (error) {
            setErrorMessage(error)
        }

        setRefreshing(false)
    }

    useMemo(() => {
        _getFileList(0)
    }, [])

    const _initialCheckList = (items) => {
        let list = []
        const checkListVM = items ? items : checkList
        for (var i = 0;i < checkListVM.length; i++) {
            list.push(null)
        }
        setCheckList(list)
    } 

    const _onAddImage = async (image) => {
        setIsLoading(true)

        try {
            const data = {
                refId: treatmentHistoryId,
                name: 'treatment-history.js',
                tag: 'treatment-history-image-list',
                type: image.mime,
                uri: image.path
            }
            
           await viewModel.addFile(data)

            setIsLoading(false)
        } catch (error) {

            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _onRefresh = () =>{
        if (!refreshing) {
            setImages([])
            // setTimeout(() => {
            _getFileList(0)
            // }, 1000)
            
        }
    }
    
    const _removeItem = async (value) => {
        setIsLoading(true)

        try {
            await viewModel.removeFile(value.id)

            const result = images.filter(image => image.id !== value.id)
            setImages(result)
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
        setIsLoading(false)
    }

    const _onScroll = (event) => {
        const frameHeight = event.nativeEvent.layoutMeasurement.height
        const contentHeight = event.nativeEvent.contentSize.height
        const maxOffset = 0.95 * parseInt(contentHeight - frameHeight)
        const currentOffset = parseInt(event.nativeEvent.contentOffset.y)
        currentOffset >= maxOffset && !refreshing ? _getFileList(offSet) : null
        
    }

    const _onCheckListPress = (index, item) => {
        const listVM = checkList
        listVM[index] ? listVM[index] = null : listVM[index] = item.id
        setCheckList([])
        setCheckList(listVM)
        setForceUpdate(Math.random())
    }

    const _onSubmit = async () => {
        _initialCheckList(checkList)
        setMode(null)
        const items = checkList.filter((item) => item !== null)
        if (items.length === 0) return
        if (mode === 'delete') await _removeItems(items)
        _onRefresh(0)
    }

    const _removeItems = async (items) => {
        setIsLoading(true)

        try {
            await viewModel.removeFiles({ items: items })
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    return (
        <TreatmentHistoryImageListView
            viewOnly={viewOnly}
            mode={mode}
            checkList={checkList}
            images={images}
            isLoading={isLoading}
            refreshing={refreshing}
            errorMessage={errorMessage}
            setErrorMessage={setErrorMessage}
            setMode={setMode}
            onSubmit={_onSubmit}
            onCheckListPress={_onCheckListPress}
            removeItem={_removeItem}
            onRefresh={_onRefresh}
            onScroll={_onScroll} 
            onAddImage={_onAddImage}
            initialCheckList={_initialCheckList} />
    )
} 
