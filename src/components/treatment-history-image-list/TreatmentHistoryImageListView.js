import React from 'react'
import TreatmentHistoryImageList from '../UI/treatment-history/TreatmentHistoryImageList'

export default (props) => {
    const { 
        viewOnly, 
        mode,
        images,
        checkList,
        onBack,
        removeItem,
        refreshing,
        isLoading,
        errorMessage,
        onRefresh,
        setErrorMessage, 
        setMode,
        onCheckListPress,
        onScroll,
        onAddImage,
        initialCheckList, 
        onSubmit } = props

    return (
        <React.Fragment >
            <TreatmentHistoryImageList 
                viewOnly={viewOnly}
                items={images}
                mode={mode}
                checkItems={checkList}
                errorMessage={errorMessage}
                onLogoPress={onBack}
                onItemLongPress={removeItem} 
                refreshing={refreshing}
                isLoading={isLoading}
                setErrorMessage={setErrorMessage}
                setMode={setMode}
                onScroll={onScroll}
                onRefresh={onRefresh}
                onItemPress={onCheckListPress}
                onAddImage={onAddImage}
                onClearCheckList={initialCheckList}
                onSubmit={onSubmit} />
        </React.Fragment>
    )
} 