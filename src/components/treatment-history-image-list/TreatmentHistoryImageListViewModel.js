export default class {
    
    constructor(store) {
        this.store = store
    }

    getFileList = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getFileList(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeFile = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeFile(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeFiles = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeFiles(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
