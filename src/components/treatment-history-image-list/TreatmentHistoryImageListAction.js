import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    getFileListAction = useStoreActions(actions => actions.file.getFileList)
    removeFileAction = useStoreActions(actions => actions.file.removeFile)
    removeFilesAction = useStoreActions(actions => actions.file.removeFiles)
    
    getFileList = (payload) => this.getFileListAction(payload)

    removeFile = payload => this.removeFileAction(payload)

    removeFiles = (payload) => this.removeFilesAction(payload)
}