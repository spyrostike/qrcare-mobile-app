import React from 'react'
import TreatmentHistoryImageListController from './TreatmentHistoryImageListController'
import TreatmentHistoryImageListViewModel from './TreatmentHistoryImageListViewModel'
import TreatmentHistoryImageListAction from './TreatmentHistoryImageListAction'

export default () => {
    const action = new TreatmentHistoryImageListAction()
    const viewModel = new TreatmentHistoryImageListViewModel(action)

    return (
        <TreatmentHistoryImageListController viewModel={viewModel} />
    )
} 
