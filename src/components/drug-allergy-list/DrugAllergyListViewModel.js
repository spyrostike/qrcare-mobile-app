export default class {

    constructor(store) {
        this.store = store
    }

    getDrugAllergyList = () => this.store.getDrugAllergyList()

    setDrugAllergyList = (payload) => this.store.setDrugAllergyList(payload)

    getDrugAllergyListByCreatureId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getDrugAllergyListByCreatureId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
    
    removeDrugAllergy = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeDrugAllergy(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeDrugAllergies = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeDrugAllergies(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
