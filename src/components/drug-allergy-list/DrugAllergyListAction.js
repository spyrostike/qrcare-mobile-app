import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    drugAllergyList = useStoreState(state => state.drugAllergy.list)
    setDrugAllergyListAction = useStoreActions(actions => actions.drugAllergy.setList)
    getDrugAllergyListByCreatureIdAction = useStoreActions(actions => actions.drugAllergy.getDrugAllergyListByCreatureId)
    removeDrugAllergyAction = useStoreActions(actions => actions.drugAllergy.removeDrugAllergy)
    removeDrugAllergiesAction = useStoreActions(actions => actions.drugAllergy.removeDrugAllergies)

    getDrugAllergyList = () => this.drugAllergyList

    setDrugAllergyList = payload => this.setDrugAllergyListAction(payload)

    getDrugAllergyListByCreatureId = payload => this.getDrugAllergyListByCreatureIdAction(payload)

    removeDrugAllergy = payload => this.removeDrugAllergyAction(payload)

    removeDrugAllergies = payload => this.removeDrugAllergiesAction(payload)

}