import React, { useMemo, useState } from 'react'
import DrugAllergyListView from './DrugAllergyListView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ mode, setMode ] = useState(null)
    const [ checkList, setCheckList ] = useState([])
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ forceUpdate, setForceUpdate ] = useState(null)

    const { viewModel } = props

    const { routes, index } = Navigator.getCurrentRoute()
    const { creatureId, viewOnly } = routes[index].params

    const prefixScreen = viewOnly ? 'ViewOnly' : 'Member'

    const _getDrugAllergyListByCreatureId = async () => {
        try {
            setIsLoading(true)
            const items = await viewModel.getDrugAllergyListByCreatureId(creatureId)
            _initialCheckList(items)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    useMemo(() => {
        viewModel.setDrugAllergyList([])
        _getDrugAllergyListByCreatureId()
    }, [])

    const _onItemPress = (value) => {
        if (mode === 'delete') return
        if (value) {
            Navigator.navigate(prefixScreen + 'DrugAllergyInfo', { 
                methodType: 'edit', 
                drugAllergyId: value.id,
                onComplete: async () => {
                    Navigator.back()
                    await _getDrugAllergyListByCreatureId()
                }
            })
        } else {
            Navigator.navigate(prefixScreen + 'DrugAllergyInfo', { 
                methodType: 'add',
                creatureId: creatureId,
                onComplete: async () => {
                    Navigator.back()
                    await _getDrugAllergyListByCreatureId()
                }
            })
        }
    }

    const _convertListForDisplay = () => {
        const list = viewModel.getDrugAllergyList()
        
        return list.map(item => ({ 
            ...item,
            text1: item.name
        }))
    }

    const _removeItems = async (items) => {
        setIsLoading(true)

        try {
            await viewModel.removeDrugAllergies({ items: items })
            await _getDrugAllergyListByCreatureId()
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _onSubmit = async () => {
        _initialCheckList(checkList)
        setMode(null)

        const items = checkList.filter((item) => item !== null)

        if (items.length === 0) return
        if (mode === 'delete') _removeItems(items)

    }

    const _initialCheckList = (items) => {
        let list = []
        const checkListVM = items ? items : checkList
        for (var i = 0;i < checkListVM.length; i++) {
            list.push(null)
        }
        setCheckList(list)
    } 

    const _onCheckBoxPress = (index, item) => {
        const listVM = checkList
        listVM[index] ? listVM[index] = null : listVM[index] = item.id
        setCheckList([])
        setCheckList(listVM)
        setForceUpdate(Math.random())
    }

    return (
        <DrugAllergyListView
            viewOnly={viewOnly}
            mode={mode}
            list={_convertListForDisplay()}
            checkList={checkList}
            onItemPress={_onItemPress}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setMode={setMode}
            onCheckBoxPress={_onCheckBoxPress}
            setErrorMessage={setErrorMessage}
            onSubmit={_onSubmit}
            initialCheckList={_initialCheckList} />
    )
} 
