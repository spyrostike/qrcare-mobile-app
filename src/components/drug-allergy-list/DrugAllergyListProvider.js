import React from 'react'
import DrugAllergyListController from './DrugAllergyListController'
import DrugAllergyListViewModel from './DrugAllergyListViewModel'
import DrugAllergyListAction from './DrugAllergyListAction'

export default () => {
    const action = new DrugAllergyListAction()
    const viewModel = new DrugAllergyListViewModel(action)

    return (
        <DrugAllergyListController viewModel={viewModel} />
    )
} 
