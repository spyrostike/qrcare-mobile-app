import React, { useMemo } from 'react'
import SplashView from './SplashView'
import Navigator from '../../services/Navigator'

export default (props) => {

    const { viewModel } = props

    useMemo(() => {
        const getMemberProfile = async () => {
            try {
                await viewModel.getMemberProfile()
                Navigator.navigate('MemberCreatureList')
            } catch (error) {
                console.log('error xx ', error)
                Navigator.navigate('Entrance')
            }
        }

        setTimeout(() => {
            getMemberProfile()
        }, 1000)

    }, [])

    return (
        <SplashView />
    )
} 
