import React from 'react'
import SplashController from './SplashController'
import SplashViewModel from './SplashViewModel'
import SplashAction from './SplashAction'

export default () => {
    const action = new SplashAction()
    const viewModel = new SplashViewModel(action)

    return (
        <SplashController viewModel={viewModel}  />
    )
} 
