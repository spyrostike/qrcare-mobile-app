import { useStoreActions } from 'easy-peasy'

export default class {
    getMemberProfileAction = useStoreActions(actions => actions.member.getMemberProfile)

    getMemberProfile = () => {
        return this.getMemberProfileAction()
    }

}