import { setUserToken, getUserToken } from '../../services/Token'

export default class {
    constructor(store) {
        this.store = store
    }

    getMemberProfile = () => {
        return new Promise(async (resolve, reject) => {

            const userToken = await getUserToken()
            if (!userToken) reject()

            this.store.getMemberProfile(userToken).then(async v => {
                setUserToken(userToken)
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}