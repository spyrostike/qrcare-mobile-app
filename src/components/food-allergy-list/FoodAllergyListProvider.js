import React from 'react'
import FoodAllergyListController from './FoodAllergyListController'
import FoodAllergyListViewModel from './FoodAllergyListViewModel'
import FoodAllergyListAction from './FoodAllergyListAction'

export default () => {
    const action = new FoodAllergyListAction()
    const viewModel = new FoodAllergyListViewModel(action)

    return (
        <FoodAllergyListController viewModel={viewModel}  />
    )
} 
