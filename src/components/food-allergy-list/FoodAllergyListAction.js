import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    foodAllergyList = useStoreState(state => state.foodAllergy.list)
    setFoodAllergyListAction = useStoreActions(actions => actions.foodAllergy.setList)
    getFoodAllergyListByCreatureIdAction = useStoreActions(actions => actions.foodAllergy.getFoodAllergyListByCreatureId)
    removeFoodAllergyAction = useStoreActions(actions => actions.foodAllergy.removeFoodAllergy)
    removeFoodAllergiesAction = useStoreActions(actions => actions.foodAllergy.removeFoodAllergies)

    getFoodAllergyList = () => this.foodAllergyList

    setFoodAllergyList = payload => this.setFoodAllergyListAction(payload)

    getFoodAllergyListByCreatureId = payload => this.getFoodAllergyListByCreatureIdAction(payload)

    removeFoodAllergy = payload => this.removeFoodAllergyAction(payload)

    removeFoodAllergies = payload => this.removeFoodAllergiesAction(payload)

}