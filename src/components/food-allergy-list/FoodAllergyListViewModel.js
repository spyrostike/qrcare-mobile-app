export default class {

    constructor(store) {
        this.store = store
    }

    getFoodAllergyList = () => this.store.getFoodAllergyList()

    setFoodAllergyList = payload => this.store.setFoodAllergyList(payload)

    getFoodAllergyListByCreatureId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getFoodAllergyListByCreatureId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
    
    removeFoodAllergy = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeFoodAllergy(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeFoodAllergies = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeFoodAllergies(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
