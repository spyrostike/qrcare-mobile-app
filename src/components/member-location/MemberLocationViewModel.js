import { genenrateRegisterObjToServer, genenrateFileObjToServer } from '../../utils/ObjectUtil'
import { setUserToken } from '../../services/Token'

export default class RegisterLocationViewModel {
    
    constructor(store) {
        this.store = store
    }

    setLocation = (value) => {
        this.store.setLocation(value)
    }

    register = () => {
        const qrcodeItem = this.store.getQRCodeItem()
        const authentication = this.store.getAuthentication()
        const profile = this.store.getProfile()
        const location = this.store.getLocation()

        const payload = genenrateRegisterObjToServer(qrcodeItem, authentication, profile, location)
  
        return new Promise((resolve, reject) => {
            this.store.register(payload).then(async v => {
                await setUserToken(v)
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getMemberById = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getMemberById(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getMemberProfile = () => {
        return new Promise((resolve, reject) => {
            this.store.getMemberProfile().then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editMemberLocation = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.editMemberLocation(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    addFile = (payload) => {
        const formData = genenrateFileObjToServer(payload)
        return new Promise((resolve, reject) => {
            this.store.addFile(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
