import React, { useMemo, useState } from 'react'
import Navigator from '../../services/Navigator'
import MemberLocationView from './MemberLocationView'

export default (props) => {
    const [ id, setId ] = useState(null)
    const [ currentLocation, setCurrentLocation ] = useState(null)
    const [ locationImageId, setLocationImageId ] = useState(null)
    const [ isImageChange, setIsImageChange ] = useState(false)
    const [ oldLocationImageId, setOldLocationImageId ] = useState(null)
    const [ locationImage, setLocationImage ] = useState(null)
    const [ address1, setAddress1 ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { viewModel } = props

    const { routes, index } = Navigator.getCurrentRoute()
    const { methodType, memberId, viewOnly } = routes[index].params

    const _getMemberById = async () => {
        try {
            if (!memberId) { setErrorMessage('ID not found'); return; } 
            setIsLoading(true)
            const result = await viewModel.getMemberById(memberId)
            
            _prepareData(result)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _getMemberProfile = async () => {
        try {
            setIsLoading(true)
            const result = await viewModel.getMemberProfile()
            _prepareData(result)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }   

    useMemo(() => {
        if (viewOnly) _getMemberById()
        if (methodType === 'edit') _getMemberProfile()
    }, [])

    const _prepareData = (data) => {
        setId(data.profile.id)
        setCurrentLocation(data.location.coordinate)
        setLocationImage(data.profile.locationImage)
        setLocationImageId(data.profile.locationImageId)
        setOldLocationImageId(data.profile.locationImageId)
        setAddress1(data.profile.address1)
    }

    const _onSubmit = async () => {
        if (!_validate()) return

        if (methodType === 'register') {
            await _register()
        }

        if (methodType === 'edit' && id) {
            await _editMemberLocation()
        }
    }
    
    const _register = async () => {
        try {
            setIsLoading(true)
            await viewModel.register()
            setIsLoading(false)
            Navigator.navigate('MemberCreatureList')
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _editMemberLocation = async () => {
        try {
            setIsLoading(true)

            const data = { 
                locationImageId: locationImageId,
                oldLocationImageId: isImageChange ? oldLocationImageId : null,
                coordinate: currentLocation
            }

            await viewModel.editMemberLocation(data)
            setIsLoading(false)
            Navigator.back()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    } 

    const _setLocationImage = async (image) => {
        try {
            setIsLoading(true)

            const data = {
                id: locationImageId,
                name: 'member-location.js',
                tag: 'member-location-image',
                type: image.mime,
                uri: image.path
            }
            
            const file = await viewModel.addFile(data)
            setLocationImageId(file.id)
            setLocationImage(file.src)
            setIsImageChange(true)

            viewModel.setLocation({ 
                locationImageId: file.id,
                coordinate: currentLocation
            })
    
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _setCurrentLocation = (value) => {
        const data = { 
            locationImageId: locationImageId,
            coordinate: value.coordinate
        }

        viewModel.setLocation(data)
        setCurrentLocation(value.coordinate)
    }

    const _validate = () => {
        if (currentLocation.coordinate === null) { setErrorMessage('Location coordinate is required'); return false }
        if (locationImageId === null) { setErrorMessage('Location image is required'); return false }

        return true
    }

    const _onBack = () => {
        Navigator.back()
    }

    return (
        <MemberLocationView 
            viewOnly={viewOnly}
            currentLocation={currentLocation}
            locationImage={locationImage}
            address1={address1}
            setLocationImage={_setLocationImage}
            setCurrentLocation={_setCurrentLocation}
            errorMessage={errorMessage}
            isLoading={isLoading}
            setErrorMessage={setErrorMessage}
            onBack={_onBack}
            onSubmit={_onSubmit} />
    )
} 
