import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    qrcodeItem = useStoreState(state => state.qrcode.item)
    authentication = useStoreState(state => state.member.authentication)
    profile = useStoreState(state => state.member.profile)
    location = useStoreState(state => state.member.location)
    setLocationAction = useStoreActions(actions => actions.member.setLocation)
    resgisterAction = useStoreActions(actions => actions.member.register)
    getMemberByIdAction = useStoreActions(actions => actions.member.getMemberById)
    getMemberProfileAction = useStoreActions(actions => actions.member.getMemberProfile)
    editMemberLocationAction = useStoreActions(actions => actions.member.editMemberLocation)
    addFileAction = useStoreActions(actions => actions.file.addFile)
    
    getQRCodeItem = () => {
        return this.qrcodeItem
    }

    getAuthentication = () => {
        return this.authentication
    }

    getProfile = () => {
        
        return this.profile
    }

    getLocation = () => {
        return this.location
    }

    setLocation = (payload) => {
        this.setLocationAction(payload)
    }

    register = (payload) => {
        return this.resgisterAction(payload)
    }

    getMemberById = payload => this.getMemberByIdAction(payload)

    getMemberProfile = () => this.getMemberProfileAction()

    editMemberLocation = payload => this.editMemberLocationAction(payload)

    addFile = payload => this.addFileAction(payload)
}