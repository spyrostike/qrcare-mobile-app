import React from 'react'
import LocationForm from '../UI/member/LocationForm'
import LocationFormViewOnly from '../UI/member/LocationFormViewOnly'

export default (props) => {
    const { 
        methodType,
        viewOnly,
        currentLocation,
        locationImage, 
        address1,
        setLocationImage,
        setCurrentLocation,
        isLoading,
        errorMessage, 
        setErrorMessage, 
        onBack,
        onSubmit } = props

    return (
        <React.Fragment >
            {
                viewOnly ? 
                    <LocationFormViewOnly
                        currentLocation={currentLocation}
                        locationImage={locationImage}
                        address1={address1}
                        isLoading={isLoading}
                        errorMessage={errorMessage} 
                        setErrorMessage={setErrorMessage} 
                        onLogoPress={onBack}
                        onSubmit={onSubmit} />
                    :
                    <LocationForm 
                        methodType={methodType}
                        currentLocation={currentLocation}
                        locationImage={locationImage}
                        setCurrentLocation={setCurrentLocation}
                        setLocationImage={setLocationImage}
                        isLoading={isLoading}
                        errorMessage={errorMessage} 
                        setErrorMessage={setErrorMessage} 
                        onLogoPress={onBack}
                        onSubmit={onSubmit} />
            }
        </React.Fragment>
    )
} 