import React from 'react'
import MemberLocationController from './MemberLocationController'
import MemberLocationViewModel from './MemberLocationViewModel'
import MemberLocationAction from './MemberLocationAction'

export default () => {
    const action = new MemberLocationAction()
    const viewModel = new MemberLocationViewModel(action)

    return (
        <MemberLocationController viewModel={viewModel} />
    )
} 
