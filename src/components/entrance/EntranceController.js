import React, { useState } from 'react'
import EntranceView from './EntranceView'
import Navigator from '../../services/Navigator'
import { DOMAIN_URL } from '../../../appConfig'

export default (props) => {
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { viewModel } = props

    let _scanBarcodeEnabled = true

    const _getQRCodeItemById = async (value) => {

        const { data, type } = value

        if (type !== 'QR_CODE' && type !== 'org.iso.QRCode') return
        
        const qrcodeGUID = data.replace(DOMAIN_URL + '/creature/', '')

        try {
            
            if (!isLoading && !errorMessage) {
                setIsLoading(true)
                const qrcode = await viewModel.getQRCodeItemById(qrcodeGUID)
                const { usage_status } = qrcode

                if (usage_status === 1) { 
                    setIsLoading(false)
                    Navigator.navigate('VisitorMemberAuthentication', { methodType: 'register' }) 
                } else if (usage_status === 2) { 
                    setIsLoading(false)
                    setTimeout(() => {
                        setErrorMessage('QRCode have been already registered.') 
                    }, 100)
                } else if (usage_status === 3) {
                    const creature = await viewModel.getCreatureByQRCodeId(qrcodeGUID)
                    const { id } = creature
                    setIsLoading(false)
                    Navigator.navigate('ViewOnlyCreaturePreview', { fromScreen: 'Entrance', creatureId: id })
                }
                
            }
        } catch (error) {
            setErrorMessage(error)
            setIsLoading(false)
        }
    }

    const _gotoAuthenScreen = () => {
        Navigator.navigate('VisitorMemberAuthentication', { 
            methodType: 'authen'
        })
    }
    
    const _gotoScanScreen = () => {
        Navigator.navigate('EntranceScan', { 
            methodType: 'entrance',
            onBarCodeRead: (value) => {
                if (_scanBarcodeEnabled) {
                    _scanBarcodeEnabled = false
                    setTimeout( () => {
                        _getQRCodeItemById(value)
                    }, 100)

                    Navigator.back()
                }
            } 
        })
    }

    return (
        <EntranceView 
            isLoading={isLoading}
            errorMessage={errorMessage}
            setErrorMessage={setErrorMessage}
            gotoAuthenScreen={_gotoAuthenScreen}
            gotoScanScreen={_gotoScanScreen} />
    )
} 
