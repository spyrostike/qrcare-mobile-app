import { API_STATUS_SUCCESS } from '../../../appConfig'
import QRCodeProvider from '../../resources/qrcode-provider'

const qrcodeService = new QRCodeProvider()

export default class {
    
    constructor(store) {
        this.store = store
    }

    setQRCodeItemInfo = (value) => {
        this.store.setQRCodeItemInfo(value)
    }
    
    getQRCodeItemById = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getQRCodeItemById(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getCreatureByQRCodeId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getCreatureByQRCodeId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
