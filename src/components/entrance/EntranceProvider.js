import React from 'react'
import EntranceController from './EntranceController'
import EntranceViewModel from './EntranceViewModel'
import EntranceAction from './EntranceAction'

export default () => {
    const action = new EntranceAction()
    const viewModel = new EntranceViewModel(action)

    return (
        <EntranceController viewModel={viewModel} />
    )
} 
