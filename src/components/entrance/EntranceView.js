import React from 'react'
import EntranceForm from '../UI/entrance/EntranceForm'

export default (props) => {
    const { 
        isLoading, 
        errorMessage, 
        setErrorMessage, 
        gotoAuthenScreen, 
        gotoScanScreen } = props
    
    return (
        <React.Fragment >
            <EntranceForm  
                isLoading={isLoading}
                errorMessage={errorMessage}
                setErrorMessage={setErrorMessage}
                gotoAuthenScreen={gotoAuthenScreen}
                gotoScanScreen={gotoScanScreen} />
        </React.Fragment>
    )
} 