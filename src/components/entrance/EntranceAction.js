
import { useStoreActions } from 'easy-peasy'

export default class {
    getQRCodeItemByIdAction = useStoreActions(actions => actions.qrcode.getQRCodeItemById)
    getCreatureByQRCodeIdAction = useStoreActions(actions => actions.creature.getCreatureByQRCodeId)

    getQRCodeItemById = (payload) => {
        return this.getQRCodeItemByIdAction(payload)
    }

    getCreatureByQRCodeId = (payload) => {
        return this.getCreatureByQRCodeIdAction(payload)
    }
}