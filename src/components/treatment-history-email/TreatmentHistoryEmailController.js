import React, { useMemo, useState } from 'react'
import TreatmentHistoryEmailView from './TreatmentHistoryEmailView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ emailFrom, setEmailForm ] = useState(null)
    const [ emailTo, setEmailTo ] = useState(null)
    const [ subject, setSubject ] = useState(null)
    const [ description, setDescription ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { viewModel } = props

    useMemo(() => {
        setEmailForm(viewModel.getMemberProfile().email)
    }, [])

    const _onSubmit = async () => {
        if (!_validate()) return

        try {
            setIsLoading(true)
            const { routes, index } = Navigator.getCurrentRoute()
            const { items, onComplete } = routes[index].params

            await viewModel.sendTreatmentHistoryMail({
                items: items,
                from: emailFrom,
                to: emailTo,
                tag: 'treatment-history',
                subject: subject,
                description: description
            })
            
            setIsLoading(false)
            onComplete && onComplete()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _validate = () => {
        if (emailFrom === null || emailFrom.trim().length === 0) { setErrorMessage('Sender is required'); return false }
        if (emailTo === null || emailTo.trim().length === 0) { setErrorMessage('Email is required'); return false }
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(emailTo)) { setErrorMessage('Reciever email To is invalid'); return false }
        if (subject === null || subject.trim().length === 0) { setErrorMessage('subject is required'); return false }

        return true
    }

    return (
        <TreatmentHistoryEmailView
            emailFrom={emailFrom}
            emailTo={emailTo}
            subject={subject}
            description={description}
            setEmailForm={setEmailForm}
            setEmailTo={setEmailTo}
            setSubject={setSubject}
            setDescription={setDescription}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setErrorMessage={setErrorMessage}
            onSubmit={_onSubmit} />
    )
}