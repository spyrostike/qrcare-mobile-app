export default class {
    
    constructor(store) {
        this.store = store
    }

    getMemberProfile = () => this.store.getMemberProfile()

    sendTreatmentHistoryMail = payload => {
        return new Promise((resolve, reject) => {
            this.store.sendTreatmentHistoryMail(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
