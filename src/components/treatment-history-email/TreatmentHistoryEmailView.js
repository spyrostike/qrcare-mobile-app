import React from 'react'
import TreatmentHistoryEmailForm from '../UI/treatment-history/TreatmentHistoryEmailForm'

export default (props) => {
    const { 
        emailFrom,
        emailTo,
        subject,
        description,
        setEmailForm,
        setEmailTo,
        setSubject,
        setDescription,
        isLoading,
        errorMessage,
        setErrorMessage,
        onSubmit } = props
    
    return (
        <React.Fragment >
            <TreatmentHistoryEmailForm
                emailFrom={emailFrom}
                emailTo={emailTo}
                subject={subject}
                description={description}
                setEmailForm={setEmailForm}
                setEmailTo={setEmailTo}
                setSubject={setSubject}
                setDescription={setDescription}
                isLoading={isLoading}
                errorMessage={errorMessage}
                setErrorMessage={setErrorMessage}
                onSubmit={onSubmit} />
        </React.Fragment>
    )
} 