import React from 'react'
import TreatmentHistoryEmailController from './TreatmentHistoryEmailController'
import TreatmentHistoryEmailViewModel from './TreatmentHistoryEmailViewModel'
import TreatmentHistoryEmailAction from './TreatmentHistoryEmailAction'

export default () => {
    const action = new TreatmentHistoryEmailAction()
    const viewModel = new TreatmentHistoryEmailViewModel(action)

    return (
        <TreatmentHistoryEmailController viewModel={viewModel} />
    )
} 
