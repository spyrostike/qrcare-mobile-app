import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    memberProfile = useStoreState(state => state.member.profile)
    sendTreatmentHistoryMailAction = useStoreActions(actions => actions.treatmentHistory.sendMail)

    getMemberProfile = () => this.memberProfile

    sendTreatmentHistoryMail = payload => this.sendTreatmentHistoryMailAction(payload)
}