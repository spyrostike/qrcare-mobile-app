import React, { useMemo, useState } from 'react'
import MemberAuthenticationView from './MemberAuthenticationView'
import Navigator from '../../services/Navigator'

export default (props) => {
    // const [ username, setUsername ] = useState('test001')
    // const [ password, setPassword ] = useState('Tanat@175')
    const [ username, setUsername ] = useState(null)
    const [ password, setPassword ] = useState(null)
    const [ accept, setAccept ] = useState(false)
    const [ modal, setModal ] = useState(false)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { routes } = Navigator.getCurrentRoute()
    const { methodType } = routes[0].params

    const { viewModel } = props

    useMemo(() => { }, [])

    const _onSubmit = () => {
        if (methodType === 'register') _register()
        else if (methodType === 'authen') _authen()
    }

    const _authen= async () => {
        if (!_validateForAuthen()) return
        setIsLoading(true)
        
        const data = { 
            username: username,
            password: password
        }

        try {
            await viewModel.authen(data)
            
            setIsLoading(false)
            
            Navigator.navigate('MemberCreatureList')
        } catch (error) {
            setTimeout(() => {
                setIsLoading(false)
            }, 500)
            setTimeout(() => {
            setErrorMessage(error)
        }, 500)
        }
    }

    const _register = async () => {
        if (!_validateForRegister()) return
        
        const data = { 
            username: username,
            password: password
        }

        try {
            await viewModel.checkUserExist(data)
            setTimeout(() => {
                setModal(true)
            }, 200)
        } catch (error) {
            setErrorMessage(error)
        }
    }

    const _onAccept = () => {
        setModal(false)
        setAccept(false)
        Navigator.navigate('VisitorMemberProfile', { methodType: 'register' })
    }

    const _validateForAuthen = () => {
        if (username === null || username.trim().length === 0) { setErrorMessage('Username is required'); return false; }
        if (password === null || password.trim().length === 0) { setErrorMessage('Password is required'); return false; }

        return true
    }

    const _validateForRegister = () => {
        if (username === null || username.trim().length === 0) { setErrorMessage('Username is required'); return false; }
        if (username.trim().length < 6 || username.trim().length > 30) { setErrorMessage('Username should be between 6-30'); return false; }
        // if (!/^[a-z][a-z0-9]*$/.test(username)) { setErrorMessage('Username should be between a-z 0-9 '); return false; }
        if (!/^[a-zA-Z0-9_]+$/.test(username)) { setErrorMessage('Username should be between a-z 0-9 '); return false; }
        if (password === null || password.trim().length === 0) { setErrorMessage('Password is required'); return false; }
        if (password.trim().length < 6 || password.trim().length > 30) { setErrorMessage('Password should be between 6-30'); return false; }
        if (!/^[a-zA-Z0-9!@#$%\^&*+=._-]*$/.test(password)) { setErrorMessage('Password should be between a-z 0-9, symbol(!@#$%^&*+=._-)'); return false; }
        // if (!new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})').test(password)) { 
        //     setErrorMessage('Password must have at least one capital, symbol and number')
        //     return false
        // }

        return true
    }

    return (
        <MemberAuthenticationView 
            setModal={setModal}
            username={username} 
            password={password}
            accept={accept}
            modal={modal}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setUsername={setUsername} 
            setPassword={setPassword} 
            setAccept={setAccept}
            setErrorMessage={setErrorMessage}
            onAccept={_onAccept}
            onSubmit={_onSubmit} />
    )
} 
