import React from 'react'
import MemberAuthenticationController from './MemberAuthenticationController'
import MemberAuthenticationViewModel from './MemberAuthenticationViewModel'
import MemberAuthenticationAction from './MemberAuthenticationAction'

export default () => {
    const action = new MemberAuthenticationAction()
    const viewModel = new MemberAuthenticationViewModel(action)

    return (
        <MemberAuthenticationController viewModel={viewModel} />
    )
} 
