import { setUserToken } from '../../services/Token'

export default class {
    
    constructor(store) {
        this.store = store
    }

    checkUserExist = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.checkUserExist(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    authen = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.authen(payload).then(async v => {
                await setUserToken(v)
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
