import React from 'react'
import AuthenticationForm from '../UI/member/AuthenticationForm'

export default (props) => {
    const { 
        setUsername, 
        setPassword,
        setAccept, 
        setModal,
        username, 
        password,
        accept,
        modal,
        isLoading, 
        errorMessage, 
        setErrorMessage, 
        onSubmit,
        onAccept } = props
    
    return (
        <AuthenticationForm 
            setUsername={setUsername} 
            setPassword={setPassword} 
            setAccept={setAccept}
            setModal={setModal}
            username={username} 
            password={password}
            accept={accept}
            modal={modal}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setErrorMessage={setErrorMessage}
            onSubmit={onSubmit}
            onAccept={onAccept} />
    )
} 