
import { useStoreActions } from 'easy-peasy'

export default class {
    checkUserExistAction = useStoreActions(actions => actions.member.checkUserExist)
    authenAction = useStoreActions(actions => actions.member.authen)

    checkUserExist = paylod => {
        return this.checkUserExistAction(paylod)
    }

    authen = paylod => {
        return this.authenAction(paylod)
    }
}