import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    drugList = useStoreState(state => state.drug.list)
    setDrugListAction = useStoreActions(actions => actions.drug.setList)
    getDrugListByCreatureIdAction = useStoreActions(actions => actions.drug.getDrugListByCreatureId)
    removeDrugAction = useStoreActions(actions => actions.drug.removeDrug)
    removeDrugsAction = useStoreActions(actions => actions.drug.removeDrugs)

    getDrugList = () => this.drugList

    setDrugList = payload => this.setDrugListAction(payload)
    
    getDrugListByCreatureId = payload => this.getDrugListByCreatureIdAction(payload)

    removeDrug = payload => this.removeDrugAction(payload)

    removeDrugs = payload => this.removeDrugsAction(payload)

}