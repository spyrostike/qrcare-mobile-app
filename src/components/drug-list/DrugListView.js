import React from 'react'
import DrugList from '../UI/drug/DrugList'

export default (props) => {
    const { 
        viewOnly,
        mode,
        list, 
        checkList,
        onItemPress, 
        isLoading, 
        errorMessage, 
        setErrorMessage,
        setMode,
        onSubmit,
        onCheckBoxPress,
        initialCheckList } = props

    return (
        <React.Fragment >
            <DrugList 
                viewOnly={viewOnly}
                mode={mode}
                items={list} 
                checkItems={checkList} 
                onItemPress={onItemPress} 
                isLoading={isLoading}
                errorMessage={errorMessage}
                setErrorMessage={setErrorMessage}
                onItemLongPress={setMode}
                onCheckBoxPress={onCheckBoxPress}
                onSubmit={onSubmit}
                onClearCheckList={initialCheckList} />
        </React.Fragment>
    )
} 