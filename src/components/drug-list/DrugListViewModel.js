export default class {

    constructor(store) {
        this.store = store
    }

    getDrugList = () => this.store.getDrugList()

    setDrugList = payload => this.store.setDrugList(payload)

    getDrugListByCreatureId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getDrugListByCreatureId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
    
    removeDrug = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeDrug(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeDrugs = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeDrugs(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
