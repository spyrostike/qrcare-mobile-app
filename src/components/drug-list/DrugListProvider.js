import React from 'react'
import DrugListController from './DrugListController'
import DrugListViewModel from './DrugListViewModel'
import DrugListAction from './DrugListAction'

export default () => {
    const action = new DrugListAction()
    const viewModel = new DrugListViewModel(action)

    return (
        <DrugListController viewModel={viewModel} />
    )
} 
