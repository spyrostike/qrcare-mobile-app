import React, { useMemo, useState } from 'react'
import DrugListView from './DrugListView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ mode, setMode ] = useState(null)
    const [ checkList, setCheckList ] = useState([])
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ forceUpdate, setForceUpdate ] = useState(null)

    const { viewModel } = props

    const { routes, index } = Navigator.getCurrentRoute()
    const { creatureId, viewOnly } = routes[index].params
    
    const prefixScreen = viewOnly ? 'ViewOnly' : 'Member'

    const _getDrugListByCreatureId = async () => {
        try {
            setIsLoading(true)
            const items = await viewModel.getDrugListByCreatureId(creatureId)
            _initialCheckList(items)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    useMemo(() => {
        viewModel.setDrugList([])
        _getDrugListByCreatureId()
    }, [])

    const _onItemPress = (value) => {
        if (mode === 'delete') return
        if (value) {
            Navigator.navigate(prefixScreen + 'DrugInfo', { 
                methodType: 'edit', 
                drugId: value.id,
                onComplete: async () => {
                    Navigator.back()
                    await _getDrugListByCreatureId()
                }
            })
        } else {
            Navigator.navigate( prefixScreen + 'DrugInfo', { 
                methodType: 'add',
                creatureId: creatureId,
                onComplete: async () => {
                    Navigator.back()
                    await _getDrugListByCreatureId()
                }
            })
        }
    }

    const _convertListForDisplay = () => {
        const list = viewModel.getDrugList()

        return list.map(item => ({ 
            ...item,
            text1: item.name
        }))
    }

    const _removeItems = async (items) => {
        setIsLoading(true)

        try {
            await viewModel.removeDrugs({ items: items })
            await _getDrugListByCreatureId()
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _onSubmit = async () => {
        _initialCheckList(checkList)
        setMode(null)

        const items = checkList.filter((item) => item !== null)

        if (items.length === 0) return
        if (mode === 'delete') _removeItems(items)

    }

    const _initialCheckList = (items) => {
        let list = []
        const checkListVM = items ? items : checkList
        for (var i = 0;i < checkListVM.length; i++) {
            list.push(null)
        }
        setCheckList(list)
    } 

    const _onCheckBoxPress = (index, item) => {
        const listVM = checkList
        listVM[index] ? listVM[index] = null : listVM[index] = item.id
        setCheckList([])
        setCheckList(listVM)
        setForceUpdate(Math.random())
    }

    return (
        <DrugListView
            viewOnly={viewOnly}
            mode={mode}
            list={_convertListForDisplay()}
            checkList={checkList}
            onItemPress={_onItemPress}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setMode={setMode}
            onCheckBoxPress={_onCheckBoxPress}
            setErrorMessage={setErrorMessage}
            onSubmit={_onSubmit}
            initialCheckList={_initialCheckList} />
    )
} 
