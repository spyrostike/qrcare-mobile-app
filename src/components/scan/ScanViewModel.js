export default class {
    
    constructor(store) {
        this.store = store
    }

    getQRCodeItemById = (id) => {
        return new Promise((resolve, reject) => {
            this.store.getQRCodeItemById(id).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
