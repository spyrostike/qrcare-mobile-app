import React, { useState } from 'react'
import ScanView from './ScanView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { viewModel } = props
    const { routes, index } = Navigator.getCurrentRoute()
    const { onBarCodeRead } = routes[index].params

    const _onBarCodeRead = async (value) => {
        onBarCodeRead ? onBarCodeRead(value) : null
    }

    return (
        <ScanView 
            isLoading={isLoading}
            errorMessage={errorMessage}
            setErrorMessage={setErrorMessage}
            onBarCodeRead={_onBarCodeRead} />
    )
} 
