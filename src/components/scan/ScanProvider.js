import React from 'react'
import ScanController from './ScanController'
import ScanViewModel from './ScanViewModel'
import ScanAction from './ScanAction'

export default () => {
    const action = new ScanAction()
    const viewModel = new ScanViewModel(action)

    return (
        <ScanController viewModel={viewModel}  />
    )
} 
