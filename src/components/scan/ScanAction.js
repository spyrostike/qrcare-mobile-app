
import { useStoreActions } from 'easy-peasy'

export default class {
    getQRCodeItemByIdAction = useStoreActions(actions => actions.qrcode.getQRCodeItemById)

    getQRCodeItemById = (value) => {
        return this.getQRCodeItemByIdAction(value)
    }
}