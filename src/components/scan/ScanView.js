import React from 'react'
import ScanForm from '../UI/scan/ScanForm'

const ScanView = (props) => {

    const { isLoading, errorMessage, setErrorMessage, onBarCodeRead } = props

    return (
        <ScanForm 
            isLoading={isLoading}
            errorMessage={errorMessage}
            setErrorMessage={setErrorMessage}
            onBarCodeRead={onBarCodeRead} />
    )
} 

export default ScanView