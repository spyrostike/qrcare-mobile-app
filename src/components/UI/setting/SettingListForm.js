import React from 'react'
import { View, Text } from 'react-native'
import { ListItem } from 'react-native-elements'
import IList from '../list/IList'

export default(props) => {
    const { items, onPress } = props

    const _header = () => {
        return (
            <ListItem
                title={
                    <View style={{flexDirection: 'column'}}>
                        <Text style={{ color: '#FFFFFF' }} >การตั้งค่า</Text>
                    </View>
                }
                containerStyle={{ backgroundColor: '#000000' }}
                leftIcon={{name: 'gear', type: 'evilicon', iconStyle: { color: '#FFFFFF', width: 25, height: 25 } }}
                bottomDivider >
            </ListItem>
        )
    }

    const _renderItem = ({ item }) => {
        return (
            <ListItem
                title={
                    <View style={{ flexDirection: 'column'}}>
                        <Text style={{ color: '#FFFFFF' }} >{ item.title }</Text>
                    </View>
                }
                leftIcon={
                    {
                        name: item.iconName, 
                        type: item.iconType, 
                        iconStyle: item.iconStyle, 
                        size: item.iconSize
                    }
                }
                onPress={() => onPress(item)}
                containerStyle={{ backgroundColor: '#000000' }}
                bottomDivider />
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#000000' }}>
            <IList
                header={_header} 
                data={items} 
                renderItem={_renderItem}
                stickyHeaderIndices={[0]} />
            
        </View>
    )
}
