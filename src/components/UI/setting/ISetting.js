import React from 'react'
import { StyleSheet, TouchableHighlight } from 'react-native'
import { Icon } from 'native-base'
import Navigator from '../../../services/Navigator'

export default (props) => {
    const { onPress } = props

    const _onPress = () => {
        Navigator.navigate('MemberSettingList')
    } 

    return (
        <TouchableHighlight style={styles.container} onPress={_onPress} >
            <Icon type='EvilIcons' name='gear' style={styles.icon} />
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 0.7,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    icon: { 
        fontSize: 40,
        color: '#FFFFFF'
    }
})