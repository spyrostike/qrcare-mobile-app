import React, { useEffect, useState } from 'react'
import { PermissionsAndroid, Platform, ToastAndroid, View, Text, StyleSheet } from 'react-native'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import Geolocation from 'react-native-geolocation-service'
import _ from 'lodash'
import Geocoder from 'react-native-geocoding'

import IButton from '../button/IButton'

export default (props) => {
    const [ currentLocation, setCurrentLocation ] = useState(null)
    const [ region, setRegion ] = useState(null)
    const [ watchId, setWatchId ] = useState(null)
    const [ mapRef, setMapRef ] = useState(null)
    const [ firstSetLocation, setFirstSetLocation] = useState(false)

    const { draggable, onLocationChange, disabledAutoGetLocation, location } = props

    useEffect(() => {
        if (!disabledAutoGetLocation) _getLocation(_onSetLocationSuccess)
    }, [])

    const _onSetLocationSuccess = (position) => {
        _setRegion(position)
        _setCurrentLocation(position)
        _onLocationChange(position)
    }

    const _onSetLocationSuccessAndGotoRegion = (position) => {
        _onSetLocationSuccess(position)
        const data = {
            latitude: position.latitude,
            longitude: position.longitude,
            latitudeDelta: region.latitudeDelta ? region.latitudeDelta : 0.0922,
            longitudeDelta: region.longitudeDelta ? region.longitudeDelta  : 0.0421
        }

        mapRef && mapRef.animateToRegion(data, 350)
    }

    const _onSetLocationSuccessAndGotoRegionAndReNewDelta = (position) => {
        _onSetLocationSuccess(position)
        const data = {
            latitude: position.latitude,
            longitude: position.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
        }

        mapRef && mapRef.animateToRegion(data, 350)
    }

    const _setRegion = (position) => {
        setRegion({
            latitude: position.latitude * 1,
            longitude: position.longitude * 1,
            latitudeDelta: position.latitudeDelta ? position.latitudeDelta : 0.0922,
            longitudeDelta: position.latitudeDelta ? position.latitudeDelta : 0.0421
        })
    }

    const _setCurrentLocation = (position) => {
        const data = {
            title: 'ตำแหน่งของคุณ',
            coordinate: {
                latitude: position.latitude * 1,
                longitude: position.longitude * 1
            }
        }

        setCurrentLocation(data)
        _getLocationInfo(data.coordinate)
    }

    const _hasLocationPermission = async () => {
        if (Platform.OS === 'ios' ||
            (Platform.OS === 'android' && Platform.Version < 23)) {
          return true
        }
    
        const hasPermission = await PermissionsAndroid.check(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        )
    
        if (hasPermission) return true;
    
        const status = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        )
    
        if (status === PermissionsAndroid.RESULTS.GRANTED) return true
    
        if (status === PermissionsAndroid.RESULTS.DENIED) {
            ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG)
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG)
        }
    
        return false
    }

    const _getLocation = async (success) => {
        const hasLocationPermission = await _hasLocationPermission()
    
        if (!hasLocationPermission) return
            Geolocation.getCurrentPosition(
            (position) => { success(position.coords) },
            (error) => {
                console.log('error', error)
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
        )
    }

    const _getLocationUpdates = async (success) => {
        const hasLocationPermission = await _hasLocationPermission()
    
        if (!hasLocationPermission) return
        const watchId = Geolocation.watchPosition(
            (position) => { success(position) },
            (error) => {
                console.log(error)
            },
            { enableHighAccuracy: true, distanceFilter: 0, interval: 5000, fastestInterval: 2000 }
        )

        setWatchId(watchId)
      }
    
    const _removeLocationUpdates = () => {
        if (watchId !== null) {
            Geolocation.clearWatch(this.watchId)
            // this.setState({ updatesEnabled: false })
        }
    }

    const _onRegionChangeComplete = (value) => {
        setRegion && setRegion(value)
    }
    
    const _onRefresh = () => {
        setRegion(null)
        _getLocation(_onSetLocationSuccessAndGotoRegionAndReNewDelta)
    }

    const _onDragEnd = (position) => {
        _onSetLocationSuccessAndGotoRegion(position)
    }

    const _onPress = (position) => {
        _onSetLocationSuccess(position)
    }

    const _onLocationChange = (position) => {
        const data = {
            coordinate: {
                latitude: position.latitude,
                longitude: position.longitude
            }
        }
        
        onLocationChange && onLocationChange(data)
    }
    
    const _getLocationInfo = (coordinate) => {
        // Geocoder.init("AIzaSyAiEkOrs7ixOydoKkfPmKILnzeXBMcjItI")
        
        // Geocoder.from(coordinate.latitude, coordinate.longitude)
		// .then(json => {
        //     var addressComponent = json.results[0].formatted_address
		// 	console.log(addressComponent);
		// })
		// .catch(error => console.warn(error))
    }


    if (disabledAutoGetLocation && location && !firstSetLocation) {
        setFirstSetLocation(true)
        _setRegion(location)
        _setCurrentLocation(location)
    }
    
    const _renderCurrentMarker = (marker) => {
        return (
            <MapView.Marker 
                coordinate={marker.coordinate} 
                draggable={draggable} 
                onDragEnd={(e) =>  _onDragEnd(e.nativeEvent.coordinate)} >
                 <MapView.Callout style={{width: 100}} >
                    <Text>{marker.title}</Text>
                    <Text>{marker.description}</Text>
                </MapView.Callout>
            </MapView.Marker>
        )
    }

    return (
        <View style={[ styles.container ]}>
            <View style={{ flex: 0.8 }}>
                <MapView
                    ref={ref => { setMapRef(ref) } }
                    onMapReady={() => { _onReady = true }}
                    provider={PROVIDER_GOOGLE}
                    initialRegion={region}
                    onRegionChangeComplete={_onRegionChangeComplete}
                    style={styles.map}
                    onPress={(e) => _onPress( e.nativeEvent.coordinate) } >
                    
                    { currentLocation  && _renderCurrentMarker(currentLocation) }

                </MapView>
            </View>
            <View style={{ flex: 0.2, flexDirection: 'row', justifyContent: 'space-around' }}>
                <IButton text='TRY AGAIN' onPress={_onRefresh} width={'50%'} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    }
})
