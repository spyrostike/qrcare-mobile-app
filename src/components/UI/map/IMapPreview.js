import React, { useEffect } from 'react'
import { Linking, PermissionsAndroid, Platform, ToastAndroid, View, StyleSheet, Text } from 'react-native'
import { Icon } from 'native-base'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import _ from 'lodash'

export default (props) => {
    
    const { location } = props

    useEffect(() => {
        _hasLocationPermission()
    }, [])

    const _hasLocationPermission = async () => {
        if (Platform.OS === 'ios' ||
            (Platform.OS === 'android' && Platform.Version < 23)) {
          return true
        }
    
        const hasPermission = await PermissionsAndroid.check(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        )
    
        if (hasPermission) return true;
    
        const status = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        )
    
        if (status === PermissionsAndroid.RESULTS.GRANTED) return true
    
        if (status === PermissionsAndroid.RESULTS.DENIED) {
            ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG)
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG)
        }
    
        return false
    }

    const _renderCurrentMarker = (marker) => {
        return (
            <MapView.Marker coordinate={marker.coordinate} >
                 <MapView.Callout style={{width: 100}} >
                    <Text>{marker.title}</Text>
                    <Text>{marker.description}</Text>
                </MapView.Callout>
            </MapView.Marker>
        )
    }

    const _gotoGoogleApp = () => {
        if (!location) return
        const url = `http://maps.google.com/maps?daddr=${location.latitude * 1},${location.longitude * 1}`
        Linking.openURL(url)
    }
    return (
        <View style={[ styles.container ]}>
            <View style={{ flex: 1 }}>
                <MapView
                    provider={PROVIDER_GOOGLE}
                    region={{
                        latitude: location && location.latitude ? location.latitude * 1 : 0,
                        longitude: location && location.longitude ? location.longitude * 1 : 0,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421
                    }}
                    style={styles.map}
                    scrollEnabled={false}
                    liteMode >
                    
                    { 
                        location  && _renderCurrentMarker({
                            title: 'ตำแหน่งของเด็ก',
                            coordinate: {
                                latitude: location.latitude * 1,
                                longitude: location.longitude * 1
                            }
                        }) 
                    }

                </MapView>

                <Icon 
                    name='ios-navigate'
                    type='Ionicons' 
                    onPress={() => _gotoGoogleApp()}
                    style={{ fontSize: 35, position: 'absolute', color: '#3BB9FF', bottom: 10, right: 10 }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    }
})
