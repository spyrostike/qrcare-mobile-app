import React from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import ILogo from '../logo/ILogo'

const ScanForm = () => {

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.logoContainer}>
                <ILogo />
            </View>
        </SafeAreaView>
    )
}
export default ScanForm

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000', justifyContent: 'center' },
    logoContainer: { 
        flex: 0.5, 
        padding: 5, 
        flexDirection: 'row'
    }
})