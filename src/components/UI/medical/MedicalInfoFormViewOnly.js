import React from 'react'
import { Keyboard, Linking, SafeAreaView, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import ITextWithLabel from '../text/ITextWithLabel'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IConfirmDialog from '../dialog/IConfirmDialog'
import IImageChooserIconPreview from '../photo/IImageChooserIconPreview'

export default (props) => {

    const { 
        hospitalName,
        hospitalTel,
        patientId,
        bloodTypeName,
        doctorName,
        doctorContactNo,
        congenitalDisorder,
        birthCertificate, 
        drug,
        drugAllergy,
        foodAllergy,
        isLoading,
        dialogMessage, 
        errorMessage,
        setErrorMessage, 
        goToScreen,
        onConfirm, 
        onCancel } = props

    return (
        <SafeAreaView style={styles.container}>
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    <View style={styles.title}>
                        <Text style={styles.titleText} >MEDICAL INFO</Text>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <ITextWithLabel label='Hospital : ' value={hospitalName} />
                        <ITextWithLabel 
                            label='Hospital Tel : ' 
                            value={hospitalTel} 
                            textStyle={{ color: '#3BB9FF' }}
                            onPress={() => Linking.openURL(`tel:${hospitalTel}`)} />
                        <ITextWithLabel label='Patient ID : ' value={patientId} />
                        <ITextWithLabel label='Blood type : ' value={bloodTypeName} />
                        <ITextWithLabel label='Name of doctor : ' value={doctorName} />
                        <ITextWithLabel 
                            label='Contact No of doctor : ' 
                            value={doctorContactNo} 
                            textStyle={{ color: '#3BB9FF' }}
                            onPress={() => Linking.openURL(`tel:${doctorContactNo}`)} />
                        <ITextWithLabel label='Congenital disorder : ' value={congenitalDisorder} />
                        <ITextWithLabel 
                            label='Drug : ' 
                            value={drug} 
                            onPress={() => goToScreen('ViewOnlyDrugList')}
                            textStyle={{ color: drug === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                        <ITextWithLabel 
                            label='Drug allergy : ' 
                            value={drugAllergy} 
                            onPress={() => goToScreen('ViewOnlyDrugAllergyList')}
                            textStyle={{ color: drugAllergy === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                        <ITextWithLabel 
                            label='Food allergy : ' 
                            value={foodAllergy} 
                            onPress={() => goToScreen('ViewOnlyFoodAllergyList')}
                            textStyle={{ color: foodAllergy === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                        <IImageChooserIconPreview 
                            label='Birth certificate : ' 
                            items={birthCertificate ? [{ url: birthCertificate }] : [] } />
                    </ScrollView>
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IConfirmDialog message={dialogMessage} onConfirm={onConfirm} onCancel={onCancel} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    title: { flex: 0.2, padding: 5, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
    titleText: { 
        textAlign: 'center',
        fontSize: 40, 
        color: '#FFFFFF'
    },
    scroll: { 
        flex: 0.8, 
        paddingHorizontal: 5 
    }
})