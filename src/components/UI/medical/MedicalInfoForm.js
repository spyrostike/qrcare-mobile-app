import React, { useEffect, useState } from 'react'
import { Keyboard, Linking, SafeAreaView, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import { Switch } from 'native-base'
import IButton from '../button/IButton'
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import ITextWithLabel from '../text/ITextWithLabel'
import IPickerWithLabel from '../input/IPickerWithLabel'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IConfirmDialog from '../dialog/IConfirmDialog'
import IImageChooserIconPreview from '../photo/IImageChooserIconPreview'

export default (props) => {
    const [ keyboardShow, setKeyboardShow ] = useState(false)

    const { 
        id,
        hospitalName,
        hospitalTel,
        patientId,
        bloodType,
        bloodTypeName,
        doctorName,
        doctorContactNo,
        congenitalDisorder,
        birthCertificate,
        drug,
        drugAllergy,
        foodAllergy,
        drugViewOnly,
        drugAllergyViewOnly,
        foodAllergyViewOnly,
        isLoading,
        isEdit,
        dialogMessage, 
        errorMessage,
        setHospitalName,
        setHospitalTel,
        setPatientId,
        setBloodType,
        setDoctorName,
        setDoctorContactNo,
        setCongenitalDisorder,
        setErrorMessage, 
        setIsEdit, 
        goToScreen,
        onConfirm, 
        onCancel,
        onSubmit } = props

    const bloodTypeItems = [
        { label: 'AB', value: 1 },
        { label: 'A', value: 2 },
        { label: 'B', value: 3 },
        { label: 'O', value: 4 }
    ]

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            _keyboardDidShow,
        )

        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            _keyboardDidHide,
        )

        return () => {
            keyboardDidShowListener.remove()
            keyboardDidHideListener.remove();
        }

    }, [])

    const _keyboardDidShow = () => {
        setKeyboardShow(true)
    }
    
    const _keyboardDidHide = () => {
        setKeyboardShow(false)
    }

    return (
        <SafeAreaView style={styles.container}>
            {
                !keyboardShow ? 
                    
                        <View style={{ flex: 0.1, paddingHorizontal: 10, justifyContent: 'center' }} >
                            {
                                id ? 
                                    <Switch 
                                            value={isEdit} 
                                            onValueChange={setIsEdit}
                                            trackColor={{ true: '#3BB9FF', false: '#FFFFFF' }}  
                                            thumbColor='#FFFFFF' />
                                    
                                    :
                                    null
                            }
                        </View>
                    : null
            }
            {
                !keyboardShow ? 
                    <View style={styles.title}>
                        <Text style={styles.titleText} >MEDICAL INFO</Text>
                    </View>
                    : 
                    null
            }
            
            {
                isEdit ?
                    <ScrollView style={styles.scroll}>
                        <ITextInputWithLabel label='Hospital : ' value={hospitalName} onChangeText={setHospitalName} />
                        <ITextInputWithLabel label='Hospital Tel : ' value={hospitalTel} onChangeText={setHospitalTel} />
                        <ITextInputWithLabel label='Patient ID : ' value={patientId} onChangeText={setPatientId} />
                        <IPickerWithLabel label='Blood type : ' selectedValue={bloodType} onValueChange={setBloodType} items={bloodTypeItems} />
                        <ITextInputWithLabel label='Name of doctor : ' value={doctorName} onChangeText={setDoctorName} />
                        <ITextInputWithLabel label='Contact No of doctor : ' value={doctorContactNo} onChangeText={setDoctorContactNo} />
                        <ITextInputWithLabel label='Congenital disorder : ' value={congenitalDisorder} onChangeText={setCongenitalDisorder} />
                        <ITextInputWithLabel 
                            label='Drug : ' 
                            disabled 
                            showLighter
                            lighter={drug}
                            image={require('../../../assets/images/plus.png')}
                            onPress={() => goToScreen('MemberDrugList')} />
                        <ITextInputWithLabel 
                            label='Drug allergy : '
                            disabled 
                            showLighter
                            lighter={drugAllergy}
                            image={require('../../../assets/images/plus.png')}
                            showLighter
                            onPress={() => goToScreen('MemberDrugAllergyList')} />
                        <ITextInputWithLabel 
                            label='Food allergy : ' 
                            disabled 
                            showLighter
                            lighter={foodAllergy}
                            image={require('../../../assets/images/plus.png')}
                            onPress={() => goToScreen('MemberFoodAllergyList')} />
                    </ScrollView>
                    :
                    <ScrollView style={styles.scroll}>
                        <ITextWithLabel label='Hospital : ' value={hospitalName} />
                        <ITextWithLabel 
                            label='Hospital Tel : ' 
                            value={hospitalTel} 
                            textStyle={{ color: '#3BB9FF' }}
                            onPress={() => Linking.openURL(`tel:${hospitalTel}`)} />
                        <ITextWithLabel label='Patient ID : ' value={patientId} />
                        <ITextWithLabel label='Blood type : ' value={bloodTypeName} />
                        <ITextWithLabel label='Name of doctor : ' value={doctorName} />
                        <ITextWithLabel 
                            label='Contact No of doctor : ' 
                            value={doctorContactNo}
                            textStyle={{ color: '#3BB9FF' }}
                            onPress={() => Linking.openURL(`tel:${doctorContactNo}`)} />
                        <ITextWithLabel label='Congenital disorder : ' value={congenitalDisorder} />
                        <ITextWithLabel 
                            label='Drug : ' 
                            value={drugViewOnly} 
                            onPress={() => goToScreen('MemberDrugList')}
                            textStyle={{ color: drugViewOnly === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                        <ITextWithLabel 
                            label='Drug allergy : ' 
                            value={drugAllergyViewOnly} 
                            onPress={() => goToScreen('MemberDrugAllergyList')} 
                            textStyle={{ color: drugAllergyViewOnly === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                        <ITextWithLabel 
                            label='Food allergy : ' 
                            value={foodAllergyViewOnly} 
                            onPress={() => goToScreen('MemberFoodAllergyList')} 
                            textStyle={{ color: foodAllergyViewOnly === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                        <IImageChooserIconPreview 
                            label='Birth certificate : ' 
                            items={birthCertificate ? [{ url: birthCertificate }] : [] } />
                    </ScrollView>
            }
            
            {
                isEdit && !keyboardShow ? 
                    <View style={styles.footer}>
                        <IButton text='Sumbit' onPress={onSubmit} width={'50%'} />
                    </View>
                    :
                    null
            }
            <ILoadingDialog isLoading={isLoading} />
            <IConfirmDialog message={dialogMessage} onConfirm={onConfirm} onCancel={onCancel} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    title: { flex: 0.2, padding: 5, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
    titleText: { textAlign: 'center', fontSize: 40, color: '#FFFFFF' },
    scroll: { flex: 0.55, paddingHorizontal: 5 },
    footer: { flex: 0.15, flexDirection: 'row', justifyContent: 'space-around', paddingVertical: 5 },
})