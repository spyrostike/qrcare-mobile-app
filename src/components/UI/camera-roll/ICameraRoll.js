import React from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import CameraRollPicker from 'react-native-camera-roll-picker'

export default (props) => {
    
    const { selected, selectSingleItem, onSelect } = props

    const callback = (images, current) => {

        onSelect ? onSelect(images, current) : null
    }
    
    return (
        <SafeAreaView style={styles.container}>
            <CameraRollPicker 
                groupTypes='All'
                maximum={3}
                selectSingleItem={selectSingleItem}
                assetType='Photos'
                imagesPerRow={3}
                imageMargin={5}
                selected={selected ? selected : []}
                callback={callback} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { 
        flex: 1,
        marginTop: 10
    }
})