import React, { useEffect, useState } from 'react'
import { Keyboard, SafeAreaView, StyleSheet, TouchableWithoutFeedback, View, Linking } from 'react-native'
import { Label, Text } from 'native-base'
import ViewMoreText from 'react-native-view-more-text'
import ParsedText from 'react-native-parsed-text'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextWithLabel from '../text/ITextWithLabel'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IConfirmDialog from '../dialog/IConfirmDialog'
import Navigator from '../../../services/Navigator'
import ITimePickWithText from '../text/ITimePickWithText'
import IDatePickWithText from '../text/IDatePickWithText'

export default (props) => {
    const [ imageHeight, setImageHeight ] = useState(0)

    const { 
        date,
        time,  
        image, 
        imageCount,
        description, 
        departmentName,
        fileCategory,
        fileCategoryName,
        fileCategoryOther,
        isLoading,
        dialogMessage, 
        errorMessage,
        setErrorMessage, 
        goToScreen,
        onConfirm, 
        onCancel } = props

    const _handleUrlPress = (url, matchIndex) => Linking.openURL(url)

    const _onImageLayout = (event) => {
        const { x, y, height, width } = event.nativeEvent.layout
        setImageHeight(height)
    }

    return (
        <SafeAreaView  style={styles.container} >
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    <View style={styles.header} >
                        <View style={{ flex: 0.5 }} />
                        <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 10 }}>
                            <IDatePickWithText 
                                date={date} 
                                disabled={true}
                                containerStyle={{ flex: 0.7 }} 
                                alignItems='flex-end'  />
                            <ITimePickWithText 
                                containerStyle={{ flex: 0.3, marginRight: 12 }} 
                                time={time} 
                                disabled={true}
                                width={'100%'} 
                                alignItems='flex-end'  />
                        </View>
                    </View>
                    <View style={{flex: 0.9 }} >
                        <View style={{ flex: 1 }} >
                            <View style={styles.image} onLayout={(event) => _onImageLayout(event)} >
                                <IPhotoChooser 
                                    photo={image} 
                                    iconType='Entypo' 
                                    iconName='folder-images' 
                                    disabled={true}
                                    imageStyle={{ 
                                        width: imageHeight, 
                                        height: imageHeight
                                    }} />
                            </View>
                            <View style={{ flex: 0.7 }}>
                                <View style={[ styles.body ]} >
                                    <ITextWithLabel label='Department : ' value={departmentName} />
                                    <ITextWithLabel label='File category : ' value={fileCategoryName} />
                                    <View style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                        {
                                            fileCategory === 5 ?
                                                <ITextWithLabel label='Other : ' value={fileCategoryOther} />   
                                                :
                                                null
                                        }
                                    </View>
                                    <ITextWithLabel 
                                        label='Treatment history files : '
                                        value={imageCount} onPress={() => goToScreen('ViewOnlyTreatmentHistoryImageList')} 
                                        textStyle={{ color: imageCount === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                                </View>
                                <View style={[ styles.scroll, { overflow: 'hidden' } ]}>
                                    <Label style={[ styles.label, { alignSelf: 'center' } ]}>Description</Label>
                                    {
                                        description? 
                                            <ViewMoreText
                                                numberOfLines={8}
                                                renderViewMore={
                                                    () => <Text 
                                                                onPress={() => Navigator.navigate('ViewOnlyDescriptionMore', { description: description })} 
                                                                style={{ color: 'blue', fontSize: 14 }}>
                                                                    View more
                                                        </Text>
                                                } >
                                                <ParsedText
                                                    style={{ color: '#FFFFFF', fontSize: 14 }}
                                                    parse={ [ {type: 'url', style: styles.url, onPress: _handleUrlPress} ]}
                                                    childrenProps={{allowFontScaling: false}}>
                                                        { description }
                                                </ParsedText>
                                                {/* <Text style={{ color: '#FFFFFF', fontSize: 14 }}>{ description }</Text> */}
                                            </ViewMoreText>
                                        :
                                        null
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IConfirmDialog message={dialogMessage} onConfirm={onConfirm} onCancel={onCancel} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    image: { flex: 0.3 },
    header: { 
        flex: 0.1, 
        padding: 5, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'flex-end' 
    },
    body: { 
        flex: 0.3, 
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    scroll: { 
        flex: 0.7, 
        // alignItems: 'center', 
        paddingHorizontal: 20, 
        paddingVertical: 10
    },
    label: { color: '#FFFFFF', marginVertical: 10, textAlign: 'center' },
    url: {
        color: '#3BB9FF',
        textDecorationLine: 'underline',
    }

})