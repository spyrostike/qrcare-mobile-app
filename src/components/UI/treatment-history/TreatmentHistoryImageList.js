import React, { useState } from 'react'
import { Dimensions, Modal, SafeAreaView, StyleSheet, View } from 'react-native'
import ImageViewer from 'react-native-image-zoom-viewer'
import IList from '../list/IList'
import { API_IMAGE_PATH } from '../../../../appConfig'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IImageChooserImage from '../photo/IImageChooserImage'
import IImage from '../image/IImage'
import IButton from '../button/IButton'
import IHeaderImageViewer from '../photo/IHeaderImageViewer'

const dimensions = Dimensions.get('window')
const imageWidth = (dimensions.width - 6) / 3

export default(props) => {
    const [ visible, setVisible ] = useState(false)
    const [ index, setIndex ] = useState(0)

    const { 
        viewOnly, 
        items, 
        checkItems,
        mode,
        refreshing, 
        onRefresh, 
        onScroll, 
        onItemPress,
        isLoading,
        errorMessage,
        setErrorMessage, 
        setMode,
        onAddImage,
        onSubmit,
        onClearCheckList } = props

    const _prepareItems = (items) => {
        let result = []
        result = result.concat(items)
        const block = 3 - (result.length % 3)

        if (block < 3) {
            for (var i = 0; i < block; i++) {
                result = result.concat({ type: 'block' })
            }
        }
        
        return result
    }

    const _data = _prepareItems(items)

    const _renderItem = ({ item, index }) => {
        if (item.type === 'add') return _renderAddImage() 
        else if (item.type === 'block') return _renderBlock()
        else return _renderImage(item, index)
    }
    
    const _renderImage = (item, index) => {
        return (
            <IImage 
                uri={API_IMAGE_PATH + '/' + item.path + '/' + item.name} 
                containerStyle={{ margin: 1, width: imageWidth, height: imageWidth }}
                imageStyle={{ width: imageWidth, height: imageWidth }}
                checked={checkItems[index] === item.id}
                onPress={(value) => {
                    console.log('mode', mode)
                    if (mode === 'delete') {
                        onItemPress && onItemPress(index, item)
                    } else if (mode === null) {
                        setVisible(true)
                        setIndex(index)
                    }
                }}
                onLongPress={() => {
                    // onItemLongPress && _alertRemoveDialog(item)
                    !viewOnly && setMode('delete')
                }} />
        )
    }

    const _renderAddImage = () => {
        return (
            <IImageChooserImage onPhotoChange={onAddImage} imageStyle={{ width: imageWidth, height: imageWidth }}/>
        )
    }

    const _renderBlock = () => {
        return (
            <View style={{ width: imageWidth, height: imageWidth }} />
        )
    }

    const _renderHeader = () => {
        return (
            <IHeaderImageViewer onPress={() => setVisible(false) } />
        )
    }

    const images = items && items.length > 0 ? 
        items.map(item => ({
            url: API_IMAGE_PATH + '/' + item.path + '/' + item.name,
            props: { }
        }))
        : []

    return (
        <SafeAreaView style={styles.container}>
            <View style={[ styles.body, mode === 'delete' ? { flex: 0.9 } : null]}>
                <IList
                    numColumns={3}
                    data={_data} 
                    renderItem={_renderItem}
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                    onScroll={onScroll} />
            </View>
            {
                mode === 'delete' ? 
                    <View style={styles.footer} >
                        <IButton text='Delete' onPress={onSubmit} width={'30%'} />
                        <IButton text='Cancel' onPress={onClearCheckList} width={'30%'} />
                    </View>
                    :
                    null
            }   
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
            <Modal visible={visible} transparent={true}>
                <ImageViewer 
                    index={index}
                    renderHeader={() => _renderHeader()}
                    saveToLocalByLongPress={false}
                    enableSwipeDown
                    onSwipeDown={() => setVisible(false)}
                    imageUrls={images}/>
            </Modal>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, flexDirection: 'column', backgroundColor: '#000000' },
    body: { flex: 0.9 },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' }
})