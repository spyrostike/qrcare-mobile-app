import React from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import ModalSelector from 'react-native-modal-selector'
import IGridData from '../list/IGridData'
import IButton from '../button/IButton'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

export default (props) => {
    const { 
        viewOnly, 
        items, 
        checkItems,
        mode,
        onItemPress, 
        errorMessage, 
        setErrorMessage, 
        setMode,
        isLoading,
        onCheckBoxPress,
        onSubmit,
        onClearCheckList } = props
    
    const modalSelectItems = [
        { key: 0, label: 'Send e-mail' },
        { key: 1, label: 'Delete' }
        
    ]

    const _disabledModalSelector = () => {
        if (viewOnly === true) return 
        if (mode === 'delete') return 
        if (mode === 'send-email') return 

        this.selector.open()
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.grid} >
                <IGridData 
                    viewOnly={viewOnly} 
                    items={items} 
                    checkItems={checkItems}
                    onItemPress={onItemPress} 
                    checkBoxMode={mode === 'delete' || mode === 'send-email' ? true : false}
                    checkBoxColor={mode === 'delete' ? 'red' : 'green'}
                    onCheckBoxPress={onCheckBoxPress}
                    onItemLongPressCustom={() => _disabledModalSelector()} />
            </View>
            {
                mode === 'delete' || mode === 'send-email' ? 
                    <View style={styles.footer} >
                        <IButton text={mode === 'delete' ? 'Delete' : 'Send'} onPress={onSubmit} width={'30%'} />
                        <IButton text='Cancel' onPress={onClearCheckList} width={'30%'} />
                    </View>
                    :
                    null
            }   
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
            
            <ModalSelector
                data={modalSelectItems}
                ref={selector => { this.selector = selector }}
                customSelector={<View style={{ flex: 0 }} />}
                onChange={(value)=> value.key === 0 ? setMode('send-email') : setMode('delete') } /> 
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, flexDirection: 'column', backgroundColor: '#000000' },
    grid: { flex: 1 },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' }
})