import React, { useEffect, useState } from 'react'
import { Keyboard, SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View, Linking } from 'react-native'
import { Label, Switch } from 'native-base'
import ViewMoreText from 'react-native-view-more-text'
import ParsedText from 'react-native-parsed-text'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextWithLabel from '../text/ITextWithLabel'
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import ITextArea from '../input/ITextArea' 
import IButton from '../button/IButton'
import IDatePickWithText from '../text/IDatePickWithText'
import ITimePickWithText from '../text/ITimePickWithText'
import IPickerWithLabel from '../input/IPickerWithLabel'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IConfirmDialog from '../dialog/IConfirmDialog'
import IImageChooserButton from '../photo/IImageChooserButton'
import Navigator from '../../../services/Navigator'
import Screen from '../../../services/Screen'

export default (props) => {
    const [ keyboardShow, setKeyboardShow ] = useState(false)
    const [ imageHeight, setImageHeight ] = useState(0)
    
    const { 
        methodType,
        id,
        date,
        time,  
        image, 
        imageCount,
        description, 
        department, 
        fileCategory, 
        fileCategoryOther, 
        departmentName,
        fileCategoryName,
        isLoading, 
        isEdit, 
        dialogMessage, 
        errorMessage,
        setDate, 
        setTime,
        setImage, 
        setDescription, 
        setDepartment, 
        setFileCategory, 
        setFileCategoryOther,
        setErrorMessage, 
        setIsEdit, 
        goToScreen,
        onSubmit, 
        onConfirm, 
        onCancel,
        onAddImage } = props
    
    const fileCategoryItems = [
        { label: '-Check up', value: 1 },
        { label: '-X ray', value: 2 },
        { label: '-MRI / CT scan', value: 3 },
        { label: '-Test  result', value: 4 },
        { label: '-Other', value: 5 }
    ]
    
    const departmentItems = [
        { label: 'Medicine', value: 1 },
        { label: 'Ear nose throat', value: 2 },
        { label: 'Teeth', value: 3 },
        { label: 'Endocrine system', value: 4 },
        { label: 'Heart', value: 5 },
        { label: 'Lung', value: 6 },
        { label: 'Liver', value: 7 },
        { label: 'Kidney', value: 8 },
        { label: 'Bone & Joint', value: 9 }
    ]
    
    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            _keyboardDidShow,
        )

        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            _keyboardDidHide,
        )

        return () => {
            keyboardDidShowListener.remove()
            keyboardDidHideListener.remove();
        }

    }, [])

    const _keyboardDidShow = () => {
        setKeyboardShow(true)
    }
    
    const _keyboardDidHide = () => {
        setKeyboardShow(false)
    }

    const _handleUrlPress = (url, matchIndex) => Linking.openURL(url)

    const _onImageLayout = (event) => {
        const { x, y, height, width } = event.nativeEvent.layout
        setImageHeight(height)
    }

    return (
        <SafeAreaView  style={styles.container} >
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    {
                        !keyboardShow ? 
                            <View style={styles.header} >
                                <View style={{ flex: 0.5 }} />
                                <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 10 }}>
                                    <IDatePickWithText 
                                        date={date} 
                                        onDayPress={setDate} 
                                        disabled={!isEdit}
                                        containerStyle={{ flex: 0.7 }} 
                                        alignItems='flex-end'  />
                                    <ITimePickWithText 
                                        containerStyle={{ flex: 0.3, marginRight: 12 }} 
                                        time={time} 
                                        disabled={!isEdit}
                                        onTimeChange={setTime} 
                                        width={'100%'} 
                                        alignItems='flex-end'  />
                                </View>
                            </View>
                            :
                            null
                    }
                    {
                        !keyboardShow ? 
                            <View style={{ flex: 0.05, paddingHorizontal: 10 }} >
                                {
                                    id ?
                                        <Switch 
                                            value={isEdit} 
                                            onValueChange={setIsEdit}
                                            trackColor={{ true: '#3BB9FF', false: '#FFFFFF' }}  
                                            thumbColor='#FFFFFF' />
                                        : null
                                }
                            </View>
                            :
                            null
                    }
                    {
                        !keyboardShow ? 
                            <View style={styles.image} onLayout={(event) => _onImageLayout(event)} >
                                <IPhotoChooser 
                                    photo={image} 
                                    onPhotoChange={setImage} 
                                    iconType='Entypo' 
                                    iconName='folder-images' 
                                    disabled={!isEdit}
                                    imageStyle={{ 
                                        width: imageHeight, 
                                        height: imageHeight
                                    }} />
                            </View>
                            :
                            null
                    }
                    {
                        isEdit ?
                        <View style={styles.body} >
                            {
                                !keyboardShow ? 
                                    <IPickerWithLabel 
                                        label='Department : ' 
                                        selectedValue={department} 
                                        onValueChange={setDepartment} 
                                        items={departmentItems} />
                                    :
                                    null
                            }
                            {
                                !keyboardShow ? 
                                    <IPickerWithLabel 
                                        label='File category : ' 
                                        selectedValue={fileCategory} 
                                        onValueChange={setFileCategory} 
                                        items={fileCategoryItems} />
                                        :
                                        null
                            }
                            {
                                fileCategory === 5 ? 
                                    <View style={{ width: '100%', height: 50 }}>
                                        {/* {
                                            fileCategory === 5 ?  */}
                                                <ITextInputWithLabel label='Other : ' value={fileCategoryOther} onChangeText={setFileCategoryOther} />
                                                {/* :
                                                null
                                        } */}
                                    </View>
                                    :
                                    null
                            }
                            <Label style={styles.label} >Description</Label>
                            <ITextArea 
                                rowSpan={Screen.isPhone() ? 3 : 7} 
                                value={description} 
                                onChangeText={setDescription} />
                        </View>
                        :
                        <View style={styles.body} >
                            <ITextWithLabel label='Department : ' value={departmentName} />
                            <ITextWithLabel label='File category : ' value={fileCategoryName} />
                            {
                                fileCategory === 5 ?
                                    <View style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                        <ITextWithLabel label='Other : ' value={fileCategoryOther} />   
                                    </View>
                                    :
                                    null
                            }
                            <ITextWithLabel 
                                label='Treatment history files : ' 
                                value={imageCount} onPress={() => goToScreen('MemberTreatmentHistoryImageList')} 
                                textStyle={{ color: imageCount === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                            <View style={[ styles.scroll, { overflow: 'hidden' } ]}>
                                <Label style={[ styles.label, { alignSelf: 'center' } ]}>Description</Label>
                                {
                                    description? 
                                        <ViewMoreText
                                            numberOfLines={7}
                                            renderViewMore={
                                                () => <Text 
                                                            onPress={() => Navigator.navigate('MemberDescriptionMore', { description: description })} 
                                                            style={{ color: 'blue', fontSize: 14 }}>
                                                            View more
                                                    </Text>
                                            } >
                                                <ParsedText
                                                    style={{ color: '#FFFFFF', fontSize: 14 }}
                                                    parse={ [ {type: 'url', style: styles.url, onPress: _handleUrlPress} ]}
                                                    childrenProps={{allowFontScaling: false}}>
                                                        { description }
                                                </ParsedText>
                                                {/* <Text style={{ color: '#FFFFFF', fontSize: 14 }}>{ description }</Text> */}
                                        </ViewMoreText>
                                    :
                                    null
                                }
                            </View>
                        </View>
                    }       
                    {
                        isEdit && !keyboardShow ? 
                            <View style={styles.footer} >
                                <IButton text='Done' onPress={onSubmit} width={'30%'} />
                                <IImageChooserButton text='Add photo' onSuccess={onAddImage} width={'30%'} />
                            </View>
                            :
                            <View style={styles.footer} />
                    }
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IConfirmDialog message={dialogMessage} onConfirm={onConfirm} onCancel={onCancel} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    image: { flex: 0.3 },
    header: { 
        flex: 0.1, 
        padding: 5, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'flex-end' 
    },
    body: { 
        flex: 0.9, 
        // justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 20
    },
    scroll: { 
        
        // alignItems: 'center', 
        paddingHorizontal: 20, 
        paddingBottom: 10
    },
    footer: { flex: 0.15, flexDirection: 'row', justifyContent: 'space-around', paddingVertical: 5 },
    label: { color: '#FFFFFF', marginVertical: 10 },
    url: {
        color: '#3BB9FF',
        textDecorationLine: 'underline',
    }

})