import React, { useEffect, useState } from 'react'
import { Keyboard, SafeAreaView, StyleSheet, TouchableWithoutFeedback, View } from 'react-native'
import { Label } from 'native-base'
import ITextArea from '../input/ITextArea' 
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import IButton from '../button/IButton'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

export default (props) => {
    const [ keyboardShow, setKeyboardShow ] = useState(false)

    const { 
        emailFrom,
        emailTo,
        subject,
        description,
        setEmailForm,
        setEmailTo,
        setSubject,
        setDescription,
        isLoading,
        errorMessage,
        setErrorMessage,
        onSubmit} = props

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            _keyboardDidShow,
        )

        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            _keyboardDidHide,
        )

        return () => {
            keyboardDidShowListener.remove()
            keyboardDidHideListener.remove();
        }

    }, [])

    const _keyboardDidShow = () => {
        setKeyboardShow(true)
    }
    
    const _keyboardDidHide = () => {
        setKeyboardShow(false)
    }

    return (
        <SafeAreaView  style={styles.container} >
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    <View style={styles.body}>
                        <ITextInputWithLabel label='From : ' value={emailFrom} onChangeText={setEmailForm} />
                        <ITextInputWithLabel label='To : ' value={emailTo} onChangeText={setEmailTo} />
                        <ITextInputWithLabel label='Subject : ' value={subject} onChangeText={setSubject} />
                        <Label style={styles.label} >Description</Label>
                        <ITextArea 
                            rowSpan={10} 
                            value={description} 
                            onChangeText={setDescription} />
                    </View>
                    {
                        !keyboardShow ?
                            <View style={styles.footer} >
                                <IButton text='Done' onPress={onSubmit} width={'50%'} />
                            </View>
                            :
                            null
                    }
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    body: { 
        flex: 0.9, 
        alignItems: 'center', 
        paddingHorizontal: 20, 
        alignItems: 'center'
    },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' },
    label: { color: '#FFFFFF', marginVertical: 10 }

})