  
import React from 'react'
import { StyleSheet } from 'react-native'
import { Icon, Item, Label } from 'native-base'
import IDatePicker from './IDatePicker'

export default (props) => {

    const { label, date, onDayPress } = props

    return (
        <Item style={styles.container} >
            <Label style={[ styles.label ]} >{ label }</Label>
            <IDatePicker date={date} onDayPress={onDayPress} />
        </Item>
    )

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        marginVertical: 5,
        paddingHorizontal: 5,
        borderRadius: 5
    },
    label: {
        color: '#000000'
    }
})