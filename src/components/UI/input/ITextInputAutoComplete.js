import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Item, Label } from 'native-base'
import Autocomplete from 'react-native-autocomplete-input'

export default (props) => {

    const { 
        label, 
        value, 
        items,
        onChangeText, 
        containerStyle, 
        labelStyle, 
        inputStyle, 
        disabled,  } = props

    const _findFilm = (query) => {
        if (query === '') {
          return []
        }

        const regex = new RegExp(`${query.trim()}`, 'i');
        return items.filter(item => item.title.search(regex) >= 0)
      }

    const _items = _findFilm(value)
    const _comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim()

    return (
        <Autocomplete
            autoCapitalize='none'
            autoCorrect={false}
            containerStyle={styles.autocompleteContainer}
            data={_items.length === 1 && _comp(value, _items[0].title) ? [] : _items}
            defaultValue={value}
            onChangeText={onChangeText}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
                <TouchableOpacity onPress={() => onChangeText && onChangeText(item.title)} key={index}>
                    <Text key={index} style={styles.itemText}>{ item.title }</Text>
                </TouchableOpacity>
            )} />
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF', 
        // flex: 1,
        paddingTop: 25
    },
    label: {
        color: '#000000'
    },
    input: { 
        // backgroundColor: '#FFFFFF'
    },
    autocompleteContainer: {
        flex: 1,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0,
        zIndex: 1
    },
    itemText: {
        fontSize: 15,
        margin: 2,
        color: '#000000'
    }
})
