  
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Icon, Item, Label, Picker } from 'native-base'

export default (props) => {

    const { label, selectedValue, onValueChange, items } = props
    
    return (
        <Item style={styles.container} >
            <Label style={styles.label} >{ label }</Label>
            <View style={styles.pickerContainer} >
                <Picker
                    mode="dropdown"
                    iosIcon={<Icon name='arrow-down' style={{ paddingTop: -6 }} />}
                    placeholder='select'
                    placeholderStyle={{ color: '#bfc6ea' }}
                    placeholderIconColor='#007aff'
                    style={styles.picker}
                    selectedValue={selectedValue}
                    onValueChange={(value) => onValueChange(value)} >

                        {
                            items.map((item, key) => <Picker.Item key={key} label={item.label} value={item.value} />)
                        }
                    
                </Picker>
            </View>
        </Item>
    )

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        marginVertical: 5,
        paddingHorizontal: 5,
        borderRadius: 5
    },
    label: {
        color: '#000000'
    },
    pickerContainer: { 
        color: '#FFFFFF', 
        flex: 1,
        
    },
    picker: { 
        width: undefined, 
        borderRadius: 0 
    }
})