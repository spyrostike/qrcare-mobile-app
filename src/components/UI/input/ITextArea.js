  
import React from 'react'
import { StyleSheet } from 'react-native'
import { Item, Label, Textarea } from 'native-base'

export default (props) => {

    const { 
        label, 
        value, 
        onChangeText, 
        containerStyle, 
        labelStyle, 
        inputStyle, 
        disabled,
        placeholder,
        rowSpan } = props

    return (
        <Item style={[ styles.container, containerStyle ]} >
            {
                label && <Label style={[ styles.label, labelStyle]} >{ label }</Label>
            }
            <Textarea 
                style={[ styles.input, inputStyle,  { flex: label ? 0.8 : 1 }]} 
                value={value} 
                onChangeText={onChangeText} 
                disabled={disabled}
                rowSpan={rowSpan ? rowSpan : 5 }
                placeholder={placeholder} />
    
        </Item>
    )

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF', 
        marginVertical: 5,
        alignItems: 'flex-start',
        borderRadius: 5
    },
    label: {
        flex:0.2,
        marginTop: 7,
        color: '#FFFFFF'
    },
    input: { 
        flex: 0.8
    }
})