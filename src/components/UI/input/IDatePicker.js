import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import DatePicker from 'react-native-datepicker'

export default (props) => {

    const [ visible, setVisible ] = useState(false)

    const { onDayPress, date } = props 

    const _onDayPress = (value) => {
        onDayPress && onDayPress(value)
        setVisible(false)
    }

    return (
        <View style={{ flex: 1 }} >
           <DatePicker
                style={{ width: '100%' }}
                date={date}
                mode='date'
                placeholder='select date'
                format='DD/MM/YYYY'
                confirmBtnText='Confirm'
                cancelBtnText='Cancel'
                customStyles={{
                    dateText: {
                        
                    },
                    dateInput: {
                        paddingLeft: 0,
                        alignItems: 'flex-start',
                        borderWidth: 0
                    },
                    dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: 4,
                        marginLeft: 0
                    }
                }}
                onDateChange={_onDayPress} />

        </View>
    )
}
