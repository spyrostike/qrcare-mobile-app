  
import React from 'react'
import { Image, StyleSheet, TouchableHighlight } from 'react-native'
import { Icon, Input, Item, Label } from 'native-base'

export default (props) => {

    const { 
        label, 
        value, 
        onChangeText, 
        containerStyle, 
        labelStyle, 
        inputStyle, 
        disabled, 
        keyboardType,
        secureTextEntry,
        icon,
        image,
        showLighter,
        lighter,
        onPress } = props

    return (
        <Item style={[ styles.container, containerStyle ]} >
            {
                label && <Label style={[ styles.label, labelStyle ]} >{ label }</Label>
            }

            <Input 
                style={[ styles.input, inputStyle]} 
                value={value} 
                onChangeText={onChangeText} 
                disabled={disabled} 
                secureTextEntry={secureTextEntry} 
                keyboardType={keyboardType || 'default'} />

            {
                icon && <Icon active name={icon} style={{ color: 'blue' }} onPress={onPress} />
            }

            {
                showLighter && 
                <TouchableHighlight style={[ styles.imageContainer, { right: 40 } ]} onPress={onPress} underlayColor='#FFFFFF' >
                    {
                        lighter ? 
                            <Image
                                resizeMode='contain'
                                source={require('../../../assets/images/green_light.png')}
                                style={[ styles.image, { width: 50, height: 50 } ]} />
                            :
                            <Image
                                resizeMode='contain'
                                source={require('../../../assets/images/none_light.png')}
                                style={[ styles.image, { width: 50, height: 50 } ]} />
                    }
                    
                </TouchableHighlight>
            }
            
            {
                image && 
                <TouchableHighlight style={styles.imageContainer} onPress={onPress} underlayColor='#FFFFFF' >
                    <Image
                        resizeMode='contain'
                        source={image}
                        style={[ styles.image ]} />
                </TouchableHighlight>
            }
        </Item>
    )

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF', 
        marginVertical: 5,
        paddingHorizontal: 5,
        borderRadius: 5
    },
    label: {
        color: '#000000'
    },
    input: { 
        // backgroundColor: '#FFFFFF'
    },
    imageContainer: {
        position: 'absolute',
        right: 5,
        flex: 0.1
    },
    image: { 
        // flex: 1,
        width: 35,
        height: 35
    }
})