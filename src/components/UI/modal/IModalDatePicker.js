import React, { useState, useEffect } from 'react'
import { SafeAreaView, Text, View } from 'react-native'
import { Icon } from 'native-base'
import { Calendar } from 'react-native-toggle-calendar'
import moment from 'moment'
import IModal from './IModal'

export default (props) => {
    const [ current, setCurrent ] = useState(moment().format('YYYY-MM-DD'))

    const { visible, date, onDayPress } = props
    
    useEffect(() => {
        if (date) { setCurrent(date) }
    }, [date])

    const _calendarHeaderComponent = (props) => {
        const { addMonth } = props
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                <Icon type='AntDesign' name='left' style={{ fontSize: 25, color: '#00FFFF' }} onPress={() => addMonth(-1)} />
                <Text style={{ fontSize: 20 }}>{ moment(current, 'YYYY-MM-DD').format('MMMM YYYY') }</Text>
                <Icon type='AntDesign' name='right' style={{ fontSize: 25, color: '#00FFFF' }} onPress={() => addMonth(1)} />
            </View>
        )
    }

    const _onDayPress = (value) => {
        onDayPress && onDayPress(value.dateString)
    }

    return (
        <IModal visible={visible} animationType={'slide'} >
            <SafeAreaView style={{ marginTop: 15 }}>
                <Calendar 
                    current={current}
                    calendarHeaderComponent={_calendarHeaderComponent}
                    onMonthChange={(value) =>  { 
                        setCurrent(value.dateString)
                    }}
                    onDayPress={(value) =>  _onDayPress(value)}
                    markedDates={{
                        [date] : {selected: true, selectedColor: 'blue'}
                    }} />
            </SafeAreaView>
        </IModal>
    )
}