import React from 'react'
import { Modal } from 'react-native'

export default (props) => {
    const { 
        visible, 
        onRequestClose,
        animationType,
        transparent,
        children,
        style } = props

    return (
        <Modal
            animationType={animationType ? animationType : 'none'}
            style={style}
            visible={visible} 
            onRequestClose={() => onRequestClose ? onRequestClose() : null}
            transparent={transparent} >
            {children}

        </Modal>
    )
}
