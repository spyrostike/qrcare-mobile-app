  
import React from 'react'
import { StyleSheet, Text, TouchableHighlight } from 'react-native'

export default (props) => {

    const { text, onPress, disabled, constainerStyle, width, textStyle } = props
    
    return (
        <TouchableHighlight  
            style={[ 
                styles.container, 
                constainerStyle, 
                { width: width }, 
                { backgroundColor: disabled ? '#B6B6B4' : constainerStyle && constainerStyle.backgroundColor ? constainerStyle.backgroundColor : '#3BB9FF' } ]} 
            onPress={() => onPress()}
            disabled={disabled}
            underlayColor={constainerStyle && constainerStyle.backgroundColor ? constainerStyle.backgroundColor :'#3BB9FF'} >
            <Text style={[ styles.text, textStyle ]} >{ text }</Text> 
        </TouchableHighlight>
    )

}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: '#3BB9FF', 
        flexDirection: 'row', 
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        paddingVertical: 10, 
        alignSelf: 'center',
        justifyContent: 'center'
    },
    text: {
        color: '#000000'
    }
})