import React from 'react'
import { Alert, Dimensions, Image, SafeAreaView, StyleSheet, TouchableHighlight, View } from 'react-native'
import { Label } from 'native-base'
import Swiper from 'react-native-swiper'
import _ from 'lodash'

const dimensions = Dimensions.get('window')

export default (props) => {
    const { items, onItemPress, onItemLongPress } = props
    
    // console.log('items', items)

    const _prepareItems = (items) => {
        let result = items ? items : []
        result = result.concat(null) 

        // result = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, null]

        return _.chunk(result, 5) 
    }

    const _data = _prepareItems(items)

    const _onItemPress = (page, index) => {
        if (typeof _data[page][index] !== 'undefined') {
            onItemPress(_data[page][index])
        }
    }

    const _onItemLongPress = (page, index) => {
        if (typeof _data[page][index] !== 'undefined') {
            onItemLongPress && onItemLongPress(_data[page][index])
        }
    }

    const _alertRemoveDialog = (page, index) => Alert.alert(
        'ประกาศ',
        'คุณต้องการลบข้อมูล?',
        [
            {text: 'ยกเลิก', onPress: () => {}, style: 'cancel'},
            {text: 'ยืนยัน', onPress: () => _onItemLongPress(page, index)}
        ],
        { cancelable: false }
    )

    const _generateScale = () => {
        let row = []
        const scale = [
            // 0                                                     9
            [ null, null, 4, 4, 4, 0, 0, 0, null, null ],
            [ null, 4, 4, 4, 4, 0, 0, 0, 0, null ],
            [ 4, 4, 4, 4, 4, null, 0, 0, 0, 0 ],
            [ 4, 3, 4, null, null, null, null, 0, 0, 0 ],
            [ 3, 3, null, null, null, null, null, null, 1, 1 ],
            [ 3, 3, null, null, null, null, null, null, 1, 1 ],
            [ 3, 3, 3, null, null, null, null, 1, 1, 1 ],
            [ null, 3, 3, 2, null, null, 2, 1, 1, null ],
            [ null, 3, 2, 2, 2, 2, 2, 1, 1, null ],
            [ null, null, 2, 2, 2, 2, 2, null, null, null ],
        ]
        for (var i = 0; i < 10; i++) {
            let column = []
            for (var j = 0; j < 10; j++) {
                const setScale = scale[i]
                column.push(setScale[j])
            }
            row.push(column)
        }

        return row
    }

    return (
        <SafeAreaView style={styles.container} >
            <Swiper 
                showsPagination={true} 
                dot={<View style={styles.dot}/>} 
                activeDot={<View style={styles.activeDot}/>} 
                paginationStyle={{
                    position: 'absolute', 
                    bottom: -5 }} >
                        
                {
                    _data.map((item, i) => {      
                        let imageVaccine = null
                        let qty = 0
                        if (item.length === 1 && item[0] === null) {
                            qty = 0
                            imageVaccine = require('../../../assets/images/puzzle_0.png')
                        } else if (item.length === 2 && item[1] === null) {
                            qty = 1
                            imageVaccine = require('../../../assets/images/puzzle_1.png')
                        } else if (item.length === 3 && item[2] === null) {
                            qty = 2
                            imageVaccine = require('../../../assets/images/puzzle_2.png')
                        } else if (item.length === 4 && item[3] === null) {
                            qty = 3
                            imageVaccine = require('../../../assets/images/puzzle_3.png')
                        } else if (item.length === 5 && item[4] === null) {
                            qty = 4
                            imageVaccine = require('../../../assets/images/puzzle_4.png')
                        } else if (item.length === 5 && item[5] !== null) {
                            qty = 5
                            imageVaccine = require('../../../assets/images/puzzle_5.png')
                        }

                        qty = (i * 5) + qty 
                        const totalPerPage = (i * 5) + 5

                        return (
                            <View key={i} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                                <View style={{ width:  dimensions.width * 0.8, height: dimensions.width * 0.8, position: 'absolute' } }>
                                    <Image
                                        resizeMode='contain'
                                        source={imageVaccine}
                                        style={[ styles.image ]} />
                                    
                                    <View 
                                        style={{ 
                                            width: dimensions.width * 0.8,
                                            height: dimensions.width * 0.8,
                                            // borderWidth: 1,
                                            // borderColor: 'red',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            position: 'absolute' }}>
                                        <Label style={styles.label} > { qty } / { totalPerPage }</Label> 
                                    </View>


                                    <View style={{ flex: 1 }}>
                                        {
                                            _generateScale().map((row, index) => 
                                                <View key={index} style={{ flex: 1, flexDirection: 'row' }}>
                                                    {
                                                        row.map((column, index) => 
                                                            <TouchableHighlight 
                                                                key={index} 
                                                                style={{ flex: 1 }}
                                                                underlayColor='transparent'
                                                                onPress={() => column !== null ? _onItemPress(i, column) : null }
                                                                onLongPress={() => column !== null && typeof _data[i][column] !== 'undefined' && _data[i][column] !== null && onItemLongPress ? _alertRemoveDialog(i, column) : null } >
                                                                <View style={{ 
                                                                    flex: 1,
                                                                    // backgroundColor: column == 0 ? 'red' : column == 4 ? 'green' : column === 3 ? 'orange' : column === 2 ? 'brown' : column === 1 ? 'yellow' : null
                                                                }}>

                                                                </View>
                                                            </TouchableHighlight>
                                                        )
                                                    }
                                                </View>
                                            )
                                        }
                                    </View>
                                </View>
                            </View> 
                        )
                    })
                }
            </Swiper>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#000000'
    },
    imageContainer: {
        width:  dimensions.width * 0.8, 
        height: dimensions.width * 0.8,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'red'
    },
    image: { 
        width: dimensions.width * 0.8,
        height: dimensions.width * 0.8,
        // borderWidth: 1,
        // borderColor: 'red',
        position: 'absolute'
    },
    dot: {
        backgroundColor: '#FFFFFF',
        borderWidth: 0.5,
        width: 8, 
        height: 8, 
        borderRadius: 4, 
        marginLeft: 3, 
        marginRight: 3, 
        marginTop: 3, 
        marginBottom: 3,
    },
    activeDot: {
        backgroundColor: 'blue',
        width: 8, 
        height: 8, 
        borderRadius: 4, 
        marginLeft: 3, 
        marginRight: 3, 
        marginTop: 3, 
        marginBottom: 3,
    },
    label: { color: '#FFFFFF', fontSize: 30, position: 'absolute' }
})