import React from 'react'
import { Keyboard, SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import { Label } from 'native-base'
import ViewMoreText from 'react-native-view-more-text'
import Navigator from '../../../services/Navigator'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextWithLabel from '../text/ITextWithLabel'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IConfirmDialog from '../dialog/IConfirmDialog'

export default (props) => {
    const [ imageHeight, setImageHeight ] = useState(0)
    
    const { 
        image, 
        name, 
        description,
        isLoading,
        dialogMessage, 
        errorMessage,
        setImage, 
        setErrorMessage, 
        onConfirm, 
        onCancel, } = props

    const _onImageLayout = (event) => {
        const { x, y, height, width } = event.nativeEvent.layout
        setImageHeight(height)
    }

    return (
        <SafeAreaView style={styles.container} >
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    <View style={styles.image} onLayout={(event) => _onImageLayout(event)} >
                        <IPhotoChooser 
                            photo={image} 
                            onPhotoChange={setImage} 
                            iconType='Entypo' 
                            iconName='folder-images' 
                            disabled={true}
                            imageStyle={{ 
                                width: imageHeight, 
                                height: imageHeight
                            }} />
                    </View>
                    <View style={styles.body} >
                        <Label style={styles.label} >Food name</Label>
                        <Text style={{ color: '#FFFFFF' }}>{name}</Text>
                        <Label style={styles.label} >Description</Label>
                        {
                            description? 
                                <ViewMoreText
                                    numberOfLines={6}
                                    renderViewMore={
                                        () => <Text 
                                                    onPress={() => Navigator.navigate('ViewOnlyDescriptionMore', { description: description })} 
                                                    style={{ color: 'blue', fontSize: 14 }}>
                                                        View more
                                            </Text>
                                    } >
                                        <Text style={{ color: '#FFFFFF', fontSize: 14 }}>{ description }</Text>
                                </ViewMoreText>
                            :
                            null
                        }
                    </View>
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IConfirmDialog message={dialogMessage} onConfirm={onConfirm} onCancel={onCancel} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    image: { flex: 0.3 },
    body: { 
        flex: 0.7, 
        alignItems: 'center', 
        paddingHorizontal: 20
    },
    label: { color: '#FFFFFF', marginVertical: 10 }

})