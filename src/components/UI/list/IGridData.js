import React from 'react'
import { Alert, Image, ImageBackground, SafeAreaView, StyleSheet, Text, TouchableHighlight, View } from 'react-native'
import { CheckBox } from 'native-base'
import Swiper from 'react-native-swiper'
import _ from 'lodash' 

export default (props) => {
    const { 
        viewOnly, 
        loop,
        items, 
        checkItems,
        forceUpdate,
        onItemPress, 
        onItemLongPress, 
        onItemLongPressCustom, 
        checkBoxMode, 
        checkBoxColor, 
        onCheckBoxPress } = props

    const _prepareItems = (items) => {
        let result = items ? items : []
        viewOnly ? null : result = result.concat(null) 

        // result = [{}, {}, null]

        return _.chunk(_.chunk(result, 2), 4) 
    }

    const _prepareCheckItems = (checkItems) => {
        let result = checkItems ? checkItems : []
    

        // result = [{}, {}, null]

        return _.chunk(_.chunk(result, 2), 4) 
    }

    const _data = _prepareItems(items)
    const _checkData = _prepareCheckItems(checkItems)

    const _alertRemoveDialog = (value) => Alert.alert(
        'Announce',
        'Do you want to delele?',
        [
            {text: 'Cancel', onPress: () => {}, style: 'cancel'},
            {text: 'Confirm', onPress: () => onItemLongPress && onItemLongPress(value)}
        ],
        { cancelable: false }
    )

    const _onLongPress = (item) => {
        onItemLongPressCustom ? onItemLongPressCustom(item) : _alertRemoveDialog(item)
    }

    return (
        <SafeAreaView style={styles.container} >
            <Text style={{ display: 'none' }}>{forceUpdate}</Text>
            <Swiper index={_data.length - 1} loop={loop} showsPagination={true} dot={<View style={styles.dot}/>} activeDot={<View style={styles.activeDot}/>} > 
                {
                    _data.map((value, parentIndex) => 
                        <View style={{ flex: 1 }} key={parentIndex} >
                            {
                                value.map((value, rowIndex) =>  
                                    <View style={styles.rowContainer} key={rowIndex} >
                                        {
                                            value.map((value, index) => 
                                                value ?
                                                <View style={styles.columContainer} key={index} >
                                                    <TouchableHighlight 
                                                        onPress={() => onItemPress(value)} 
                                                        onLongPress={() => viewOnly ? null : _onLongPress(value) }
                                                        style={styles.imageContainer}>
                                                        <ImageBackground 
                                                            resizeMode='cover'
                                                            source={require('../../../assets/images/square_button.png')}
                                                            style={styles.image} >
                                                                { value.text1 ? <Text style={{ textAlign: 'center', fontSize: 8 }}>{ value.text1 }</Text> : null }
                                                                { value.text2 ? <Text style={{ textAlign: 'center', fontSize: 8 }}>{ value.text2 }</Text> : null }
                                                                { value.text3 ? <Text style={{ textAlign: 'center', fontSize: 8 }}>{ value.text3 }</Text> : null }
                                                        </ImageBackground>
                                                    </TouchableHighlight>
                                                    {

                                                        checkBoxMode ?
                                                            <View style={{ flexDirection: 'column-reverse', height: 120, marginBottom: 20 }}>
                                                                <CheckBox 
                                                                    onPress={() => onCheckBoxPress && onCheckBoxPress((parentIndex * 8) + (rowIndex * 2) + index, value)}
                                                                    checked={_checkData[parentIndex][rowIndex][index] !== null ? true : false} 
                                                                    color={checkBoxColor} 
                                                                    style={{ marginLeft: -10 }}/>
                                                            </View>
                                                            :
                                                            null
                                                        
                                                    }
                                                    
                                                </View> 
                                                :
                                                viewOnly ? null 
                                                    : 
                                                    <TouchableHighlight style={styles.columContainer} key={index} onPress={() => onItemPress(value)} >
                                                        <View style={styles.imageContainer}>
                                                            <Image 
                                                                resizeMode='stretch'
                                                                source={require('../../../assets/images/plus.png')}
                                                                style={styles.imagePlus} />
                                                        </View>
                                                    </TouchableHighlight>
                                            )
                                        }
                                        {
                                            value.length === 1 && <View style={styles.columContainer} />
                                        }
                                    </View>
                                )
                            }
                        </View>
                    )
                }
            </Swiper>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    rowContainer: { flex: 0.24, flexDirection: 'row', margin: 2 },
    columContainer: { 
        flexDirection: 'row', 
        flex: 0.5, 
        overflow: 'hidden', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    imageContainer: { 
        width: 120, 
        height: 120, 
        borderRadius: 10,
        justifyContent: 'center', 
        alignItems: 'center',
        overflow: 'hidden'
    },
    image: { 
        width: 120, 
        height: 120,
        justifyContent: 'center',
        alignItems: 'center' 
    },
    imagePlus: {
        width: 120,
        height: 120
    },
    dot: {
        backgroundColor: '#FFFFFF',
        borderWidth: 0.5,
        width: 8, 
        height: 8, 
        borderRadius: 4, 
        marginLeft: 3, 
        marginRight: 3, 
        marginTop: 3, 
        marginBottom: 3,
    },
    activeDot: {
        backgroundColor: 'blue',
        width: 8, 
        height: 8, 
        borderRadius: 4, 
        marginLeft: 3, 
        marginRight: 3, 
        marginTop: 3, 
        marginBottom: 3,
    }
})