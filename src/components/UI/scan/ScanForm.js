import React from 'react'
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import ICamera from '../../UI/camera/ICamera'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

const ScanForm = (props) => {
    const { errorMessage, setErrorMessage, isLoading, onBarCodeRead } = props

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 0.40 }}>
                <ICamera onBarCodeRead={onBarCodeRead} />
            </View>
            <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000' }}>
                <Text style={{ color: '#FFFFFF', fontSize: 24, textAlign: 'center' }}>Align the QRcode within the</Text>
                <Text style={{ color: '#FFFFFF', fontSize: 24, textAlign: 'center' }}>frame to start scanning</Text>
            </View>
            <View style={{ flex: 0.40, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000' }}>
                <Image
                    resizeMode='contain'
                    source={require('../../../assets/images/logo_app.jpg')}
                    style={{ 
                        width: '100%',
                        height: '100%'
                    }} />
            </View>
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}
export default ScanForm

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
})