import React from 'react'
import { Image, StyleSheet, TouchableHighlight } from 'react-native'

export default (props) => {
    const { onPress } = props

    return (
        <TouchableHighlight style={styles.container} onPress={onPress} >
            <Image
                resizeMode='contain'
                source={require('../../../assets/images/header_logo.jpg')}
                style={[ styles.logo ]} />
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 0.3
    },
    logo: { 
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'red',
        width: undefined,
        height: undefined
    }
})