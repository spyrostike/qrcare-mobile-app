import React from 'react'
import { Image, StyleSheet, TouchableHighlight } from 'react-native'

export default (props) => {
    const { onPress, logoStyle } = props

    return (
        <TouchableHighlight style={styles.container} onPress={onPress} >
            <Image
                resizeMode='contain'
                source={require('../../../assets/images/logo_app.jpg')}
                style={[ styles.logo, logoStyle ]} />
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    logo: { 
        flex: 1,
        width: undefined,
        height: undefined
    }
})