import React, { useState } from 'react'
import { Image, SafeAreaView, StyleSheet, TouchableHighlight, View } from 'react-native'
import Video from 'react-native-video'
import IButton from '../../../components/UI/button/IButton'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import { API_VIDEO_PATH } from '../../../../appConfig'

const Index = (props) => {

    const { 
        isLoading, 
        errorMessage, 
        setErrorMessage, 
        gotoAuthenScreen, 
        gotoScanScreen } = props

    return (
        <SafeAreaView  style={styles.container}>
            <View style={styles.logoSection}>

                <Image
                    resizeMode='contain'
                    source={require('../../../assets/images/logo_app.jpg')}
                    style={styles.backgroundVideo} />

                <IButton 
                    text='Log in' 
                    onPress={gotoAuthenScreen} 
                    width={'15%'} 
                    textStyle={{ color: '#FFFFFF' }}
                    constainerStyle={{ 
                        position:'absolute', 
                        right: 10, 
                        top: 15,
                        backgroundColor: 'green',
                        borderTopRightRadius: 10,
                        borderBottomRightRadius: 10,
                        borderTopLeftRadius: 10,
                        borderBottomLeftRadius: 10 }} />   
                         
            </View>
            <View style={styles.videoSection}>
                <Video             
                    source={
                        {
                            uri: `${API_VIDEO_PATH}/title_video.mp4`
                        }
                    }                    
                    onBuffer={this.onBuffer}                
                    onError={this.videoError}               
                    style={{
                        width: '100%',
                        height: '100%'
                    }} 
                    resizeMode='cover'
                    repeat={true} />
            </View>
            <View style={styles.qrcodeSection}>
                <TouchableHighlight style={{ width: '50%', }} onPress={gotoScanScreen}>
                    <Image
                        resizeMode='contain'
                        source={require('../../../assets/images/qr_scaner.png')}
                        style={{ 
                            width: '100%',
                            height: '100%'
                        }} />
                </TouchableHighlight>
            </View>
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView >
    )
  
}

export default Index

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    logoSection: { flex: 1, borderWidth: 1, justifyContent: 'center', alignItems: 'center' },
    videoSection: { flex: 1 },
    qrcodeSection: { flex: 1, borderWidth: 1, justifyContent: 'center', alignItems: 'center' },

    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    buttonGroup: {
        flexDirection: 'row',
        alignSelf: 'center',
        paddingBottom: 5,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    player: {
        alignSelf: 'stretch'
    },
    backgroundVideo: {
        width:'100%',
        height: '100%'
    }
})