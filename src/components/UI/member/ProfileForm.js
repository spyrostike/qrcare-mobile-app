import React, { useEffect, useState } from 'react'
import { Dimensions, Keyboard, SafeAreaView, ScrollView, StyleSheet, TouchableWithoutFeedback, View } from 'react-native'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import IPickerWithLabel from '../input/IPickerWithLabel'
import IDatePickerWithLabel from '../input/IDatePickerWithLabel'
import IButton from '../button/IButton'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

export default (props) => {
    const [ keyboardShow, setKeyboardShow ] = useState(false)
    const [ imageHeight, setImageHeight ] = useState(0)

    const { 
        methodType,
        profileImage,
        firstName,
        lastName,
        gender,
        email,
        bloodType,
        birthDate,
        relationship,
        identificationNo,
        contactNo1,
        contactNo2,
        address1,
        address2,
        address3,
        isLoading,
        setProfileImage,
        setFirstName,
        setLastName,
        setGender,
        setEmail,
        setBloodType,
        setBirthDate,
        setRelationship,
        setIdentificationNo,
        setContactNo1,
        setContactNo2,
        setAddress1,
        setAddress2,
        setAddress3,
        errorMessage,
        setErrorMessage,
        onSubmit } = props

    const genderItems = [
        { label: 'male', value: 1 },
        { label: 'female', value: 2 }
    ]

    const bloodTypeItems = [
        { label: 'AB', value: 1 },
        { label: 'A', value: 2 },
        { label: 'B', value: 3 },
        { label: 'O', value: 4 }
    ]
    
    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            _keyboardDidShow,
        )

        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            _keyboardDidHide,
        )

        return () => {
            keyboardDidShowListener.remove()
            keyboardDidHideListener.remove();
        }

    }, [])

    const _keyboardDidShow = () => {
        setKeyboardShow(true)
    }
    
    const _keyboardDidHide = () => {
        setKeyboardShow(false)
    }
    
    const _onImageLayout = (event) => {
        const { x, y, height, width } = event.nativeEvent.layout
        setImageHeight(height)
    }

    return (
        <SafeAreaView style={styles.container}>
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    {
                        !keyboardShow ? 
                            <View style={styles.image} onLayout={(event) => _onImageLayout(event)} >
                                <IPhotoChooser 
                                    photo={profileImage} 
                                    onPhotoChange={setProfileImage} 
                                    iconType='Octicons' 
                                    iconName='person'
                                    cropImage={true}
                                    imageStyle={{ 
                                        width: imageHeight, 
                                        height: imageHeight
                                    }} />
                            </View>
                            :
                            null
                    }
                    <ScrollView style={styles.scroll}>
                        <ITextInputWithLabel label='First name : ' value={firstName} onChangeText={setFirstName} />
                        <ITextInputWithLabel label='Last name : ' value={lastName} onChangeText={setLastName} />
                        <ITextInputWithLabel label='Email : ' value={email} onChangeText={setEmail} keyboardType={'email-address'} />
                        <IPickerWithLabel label='Gender : ' selectedValue={gender} onValueChange={setGender} items={genderItems} />
                        <IPickerWithLabel label='Blood type : ' selectedValue={bloodType} onValueChange={setBloodType} items={bloodTypeItems} />
                        <IDatePickerWithLabel label='Date of birth : ' date={birthDate} onDayPress={setBirthDate} />
                        <ITextInputWithLabel label='Relationship : ' value={relationship} onChangeText={setRelationship} />
                        <ITextInputWithLabel label='ID no. : ' value={identificationNo} onChangeText={setIdentificationNo} />
                        <ITextInputWithLabel label='Contact no. : ' value={contactNo1} onChangeText={setContactNo1} />
                        <ITextInputWithLabel label='Contact no. : ' value={contactNo2} onChangeText={setContactNo2} labelStyle={{ color: '#000000' }} />
                        <ITextInputWithLabel label='Address : ' value={address1} onChangeText={setAddress1} />
                        <ITextInputWithLabel label='Address : ' value={address2} onChangeText={setAddress2} labelStyle={{ color: '#000000' }} />
                        <ITextInputWithLabel label='Address : ' value={address3} onChangeText={setAddress3} labelStyle={{ color: '#000000' }} />
                    </ScrollView>
                    <View style={styles.footer}>
                        <IButton text={methodType === 'edit' ? 'Submit' : 'Next' } onPress={onSubmit} width={'50%'} />
                    </View>
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    image: { flex: 0.4, overflow: 'hidden',  },
    scroll: { flex: 0.5, paddingHorizontal: 5 },
    footer: { height: 60, flexDirection: 'row', justifyContent: 'space-around' }
})