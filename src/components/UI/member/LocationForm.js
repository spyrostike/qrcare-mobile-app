import React from 'react'
import { Dimensions, SafeAreaView, StyleSheet, View } from 'react-native'
import IPhotoChooser from '../photo/IPhotoChooser'
import IMap from '../map/IMap'
import IButton from '../button/IButton'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

const dimensions = Dimensions.get('window')

export default (props) => {
    
    const { 
        methodType,
        currentLocation,
        locationImage, 
        setLocationImage, 
        setCurrentLocation, 
        isLoading, 
        errorMessage, 
        setErrorMessage, 
        onSubmit } = props

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.map}>
                <IMap 
                    // draggable={true} 
                    disabledAutoGetLocation={methodType === 'edit'}
                    location={currentLocation}
                    onLocationChange={setCurrentLocation} />
            </View>
            <View style={styles.image}>
                <IPhotoChooser 
                    photo={locationImage} 
                    onPhotoChange={setLocationImage} 
                    iconType='Entypo' 
                    iconName='folder-images' 
                    imageStyle={{ 
                        width: dimensions.width, 
                        height: dimensions.height * 0.33,
                    }} />
            </View>
            <View style={styles.footer}>
                <IButton text='Done' onPress={onSubmit} width={'50%'} />
            </View>
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    map: { flex: 0.5, overflow: 'hidden', paddingVertical: 20 },
    image: { flex: 0.43, padding: 20 },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' }

})