import React from 'react'
import { Keyboard, SafeAreaView, StyleSheet, TouchableWithoutFeedback, View } from 'react-native'
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import IButton from '../button/IButton'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

export default (props) => {

    const { 
        currentPassword, 
        newPassword,
        repeatPassword, 
        isLoading, 
        errorMessage, 
        setCurrentPassword,
        setNewPassword,
        setRepeatPassword,
        setErrorMessage,
        onSubmit,
        onLogoPress } = props

    return (
        <SafeAreaView style={styles.container}>
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    <View style={styles.body}>
                        <ITextInputWithLabel 
                            label='Current Password : ' 
                            value={currentPassword} 
                            onChangeText={setCurrentPassword} 
                            secureTextEntry={true} />
                        <ITextInputWithLabel 
                            label='New password : ' 
                            value={newPassword} 
                            onChangeText={setNewPassword} 
                            secureTextEntry={true} />
                        <ITextInputWithLabel 
                            label='Repeat password : ' 
                            value={repeatPassword} 
                            onChangeText={setRepeatPassword} 
                            secureTextEntry={true} />
                    </View>
                    <View style={styles.footer}>
                        <IButton text='Submit' onPress={onSubmit} width={'50%'} />
                    </View>
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    body: { flex: 0.9, paddingHorizontal: 5 },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' }
})