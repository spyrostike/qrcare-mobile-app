import React from 'react'
import { Dimensions, SafeAreaView, StyleSheet, View } from 'react-native'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextArea from '../input/ITextArea' 
import IMapPreview from '../map/IMapPreview'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

const dimensions = Dimensions.get('window')

export default (props) => {
    
    const { 
        currentLocation,
        locationImage, 
        address1,
        setLocationImage, 
        isLoading, 
        errorMessage, 
        setErrorMessage } = props

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.map}>
                <IMapPreview 
                    // draggable={true} 
                    location={currentLocation} />
            </View>
            <View style={styles.image}>
                <IPhotoChooser 
                    photo={locationImage} 
                    onPhotoChange={setLocationImage} 
                    disabled={true}
                    iconType='Entypo' 
                    iconName='folder-images'
                    imageStyle={{ 
                        width: dimensions.width, 
                        height: dimensions.height * 0.3,
                    }} />
            </View>
            <ITextArea 
                label='Address : '
                rowSpan={3} 
                value={address1} 
                disabled={true} 
                containerStyle={{ 
                    borderBottomColor: '#000000', 
                    backgroundColor: '#000000',
                    flex: 0.1
                }}
                inputStyle={{ 
                    backgroundColor: '#000000', 
                    color: '#FFFFFF'
                }} />
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    map: { flex: 0.4, overflow: 'hidden', paddingVertical: 20 },
    image: { flex: 0.5, padding: 20 },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' }

})