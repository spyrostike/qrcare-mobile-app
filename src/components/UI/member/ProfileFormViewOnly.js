import React, { useState } from 'react'
import { Linking, SafeAreaView, ScrollView, StyleSheet, View } from 'react-native'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextWithLabel from '../text/ITextWithLabel'
import ITextArea from '../input/ITextArea' 
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

export default (props) => {
    const [ imageHeight, setImageHeight ] = useState(0)

    const { 
        profileImage,
        firstName,
        lastName,
        gender,
        email,
        bloodType,
        birthDate,
        relationship,
        identificationNo,
        contactNo1,
        contactNo2,
        address1,
        address2,
        address3,
        isLoading,
        errorMessage,
        setErrorMessage } = props

    const _onImageLayout = (event) => {
        const { x, y, height, width } = event.nativeEvent.layout
        setImageHeight(height)
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.image} onLayout={(event) => _onImageLayout(event)} >
                <IPhotoChooser 
                    photo={profileImage} 
                    iconType='Octicons' 
                    iconName='person'
                    disabled={true}
                    imageStyle={{ 
                        width: imageHeight, 
                        height: imageHeight
                    }} />
            </View>
            <ScrollView style={styles.scroll}>
                <ITextWithLabel label='First name : ' value={firstName} />
                <ITextWithLabel label='Last name : ' value={lastName} />
                <ITextWithLabel label='Email : ' value={email} />
                <ITextWithLabel label='Gender : ' value={gender} />
                <ITextWithLabel label='Blood type : ' value={bloodType}/>
                <ITextWithLabel label='Date of birth : ' value={birthDate} />
                <ITextWithLabel label='Relationship : ' value={relationship} />
                <ITextWithLabel label='ID no. : ' value={identificationNo} />
                <ITextWithLabel 
                    label='Contact no. : ' 
                    value={contactNo1}
                    textStyle={{ color: '#3BB9FF' }}
                    onPress={() => Linking.openURL(`tel:${contactNo1}`)} />
                { contactNo2 ? 
                    <ITextWithLabel 
                        label='Contact no. : ' 
                        value={contactNo2} 
                        labelStyle={{ color: '#000000' }}
                        textStyle={{ color: '#3BB9FF' }}
                        onPress={() => Linking.openURL(`tel:${contactNo2}`)}  /> 
                    : 
                    null }
                <ITextArea 
                    label='Address : '
                    rowSpan={4} 
                    value={address1} 
                    disabled={true} 
                    containerStyle={{ 
                        borderBottomColor: '#000000', 
                        backgroundColor: '#000000', 
                    }}
                    inputStyle={{ 
                        backgroundColor: '#000000', 
                        color: '#FFFFFF'
                    }} />

                {
                    address2 ? 
                        <ITextArea 
                            label='Address : '
                            rowSpan={4} 
                            value={address2} 
                            disabled={true} 
                            containerStyle={{ 
                                borderBottomColor: '#000000', 
                                backgroundColor: '#000000', 
                            }}
                            inputStyle={{ 
                                backgroundColor: '#000000', 
                                color: '#FFFFFF'
                            }} />
                            :
                            null
                }

                {
                    address3 ? 
                        <ITextArea 
                            label='Address : '
                            rowSpan={4} 
                            value={address3} 
                            disabled={true} 
                            containerStyle={{ 
                                borderBottomColor: '#000000', 
                                backgroundColor: '#000000', 
                            }}
                            inputStyle={{ 
                                backgroundColor: '#000000', 
                                color: '#FFFFFF'
                            }} />
                            :
                            null
                }

                
            </ScrollView>
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    image: { flex: 0.4, overflow: 'hidden',  },
    scroll: { flex: 0.6, paddingHorizontal: 5 }
})