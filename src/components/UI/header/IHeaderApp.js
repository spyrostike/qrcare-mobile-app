import React from 'react'
import { Platform , SafeAreaView, StyleSheet } from 'react-native'
import IHeaderLogo from '../logo/IHeaderLogo'
import ISetting from '../setting/ISetting'
import Navigator from '../../../services/Navigator'
import Screen from '../../../services/Screen'

export default () => {
    const { key } = Navigator.getCurrentRoute()

    const _onLogoPress = () => {
        const { routes, index } = Navigator.getCurrentRoute()
        if (routes[index].routeName === 'ViewOnlyCreaturePreview' || routes[index].routeName === 'Entrance') {
            const { fromScreen } = routes[index].params
            Navigator.navigate(fromScreen)
            return
        }
        
        Navigator.back()
    }

    return (
        <SafeAreaView style={styles.header} >
            <IHeaderLogo onPress={_onLogoPress} />
            { key === 'Member' ? <ISetting /> : null }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    header: { 
        height: Screen.isPhone() ? 100 : Platform.OS === 'android' ? 60 : 100, 
        padding: 5, 
        flexDirection: 'row', 
        backgroundColor: '#000000'
    }
})
