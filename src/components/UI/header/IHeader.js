import React from 'react'
import { StyleSheet, View } from 'react-native'
import IHeaderLogo from '../logo/IHeaderLogo'
import ISetting from '../setting/ISetting'

export default (props) => {
    const { onLogoPress, hideSetting } = props

    return (
        <View style={styles.header} >
            <IHeaderLogo onPress={onLogoPress} />
            { hideSetting !== true ? <ISetting /> :null }
        </View>
    )
}

const styles = StyleSheet.create({
    header: { flex: 0.1,  padding: 5, flexDirection: 'row' }
})
