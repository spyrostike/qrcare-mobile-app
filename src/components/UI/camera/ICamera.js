import React, { useState } from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import { Icon } from 'native-base'
import { RNCamera } from 'react-native-camera'
import IBarcodeFinder from './IBarcodeFinder'

export default ICamera = (props) => {
    const [ ref, setRef ] = useState(null)
    const [ type, setType ] = useState(RNCamera.Constants.Type.back)

    const { permissionDialogTitle, permissionDialogMessage, takePicture, onBarCodeRead, onClose } = props
    
    const _takePicture = async () => {
        if (ref) {
            const options = { quality: 0.5, base64: true, fixOrientation: true }
            const data = await ref.takePictureAsync(options)
            takePicture ? takePicture(data) : null
        }
    }

    const _onClose = () => {
        onClose()
    }

    return (
        <SafeAreaView style={styles.container}>
            {
                !onBarCodeRead ? 
                    <View style={[ styles.header ]}>
                        <Icon 
                            type='EvilIcons' 
                            name='close' 
                            style={styles.icon}
                            onPress={_onClose} />
                    </View>
                    : null
            }
            
            <RNCamera
                ref={ ref => setRef(ref) }
                flashMode={RNCamera.Constants.FlashMode.on}
                onBarCodeRead={(data) => onBarCodeRead ? onBarCodeRead(data) : null} 
                type={type}
                style={{ flex: 1, backgroundColor: 'green' }}
                androidCameraPermissionOptions={{ 
                    title: permissionDialogTitle ? permissionDialogTitle : 'Permission to use camera',
                    message: permissionDialogMessage ? permissionDialogMessage : 'We need your permission to use your camera phone'
                }} >
                    {
                        onBarCodeRead ? <IBarcodeFinder width={280} height={220} borderColor="red" borderWidth={2} /> : null
                    }
            </RNCamera>

            {
                !onBarCodeRead ? 
                    <View style={[ styles.footer ]}>
                        <Icon 
                            type='AntDesign' 
                            name='sync' 
                            style={styles.icon}
                            onPress={() => setType(type === RNCamera.Constants.Type.back ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back)} />
                        <Icon type='FontAwesome5' name='camera-retro' style={styles.icon} onPress={_takePicture} />
                        <Icon type='FontAwesome5' name='camera-retro' style={[ styles.icon, { color: '#000000' }]} />
                    </View>
                    : null
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { 
        backgroundColor: '#000000',
        flex: 1
    },
    header: {   
        padding: 10,
        flex: 0.1,
        flexDirection: 'row', 
        // alignItems: 'center', 
        backgroundColor: '#000000'
    },
    footer: {
        padding: 10,
        flex: 0.1,
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'center', 
        backgroundColor: '#000000'
    },
    icon: {
        fontSize: 35, 
        color: 'blue' 
    }
})

