import React from "react"
import { StyleSheet, View } from "react-native"

export default IBarcodeFinder = (props) => {

    return (
        <View style={[styles.container]}>
            <View style={[styles.finder, { width: props.width, height: props.height }]}>
                <View
                    style={[
                    { borderColor: props.borderColor },
                    styles.topLeftEdge,
                    {
                        borderLeftWidth: props.borderWidth,
                        borderTopWidth: props.borderWidth
                    }
                    ]}
                />
                <View
                    style={[
                    { borderColor: props.borderColor },
                    styles.topRightEdge,
                    {
                        borderRightWidth: props.borderWidth,
                        borderTopWidth: props.borderWidth
                    }
                    ]}
                />
                <View
                    style={[
                    { borderColor: props.borderColor },
                    styles.bottomLeftEdge,
                    {
                        borderLeftWidth: props.borderWidth,
                        borderBottomWidth: props.borderWidth
                    }
                    ]}
                />
                <View
                    style={[
                    { borderColor: props.borderColor },
                    styles.bottomRightEdge,
                    {
                        borderRightWidth: props.borderWidth,
                        borderBottomWidth: props.borderWidth
                    }
                    ]}
                />
            </View>
        </View>
    )
}

var styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
    },
    finder: {
        alignItems: "center",
        justifyContent: "center"
    },
    topLeftEdge: {
        position: "absolute",
        top: 0,
        left: 0,
        width: 40,
        height: 20
    },
    topRightEdge: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 40,
        height: 20
    },
    bottomLeftEdge: {
        position: "absolute",
        bottom: 0,
        left: 0,
        width: 40,
        height: 20
    },
    bottomRightEdge: {
        position: "absolute",
        bottom: 0,
        right: 0,
        width: 40,
        height: 20
    }
})