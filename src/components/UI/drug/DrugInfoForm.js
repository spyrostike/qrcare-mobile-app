import React, { useEffect, useState } from 'react'
import { Keyboard, SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import { Label, Switch } from 'native-base'
import ViewMoreText from 'react-native-view-more-text'
import Navigator from '../../../services/Navigator'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import ITextWithLabel from '../text/ITextWithLabel'
import ITextArea from '../input/ITextArea' 
import IButton from '../button/IButton'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IConfirmDialog from '../dialog/IConfirmDialog'
import Screen from '../../../services/Screen'

export default (props) => {
    const [ keyboardShow, setKeyboardShow ] = useState(false)
    const [ imageHeight, setImageHeight ] = useState(0)

    const { 
        methodType,
        image, 
        name, 
        description,
        isLoading,
        isEdit,
        dialogMessage, 
        errorMessage,
        setImage, 
        setName, 
        setDescription, 
        setErrorMessage,
        setIsEdit, 
        onConfirm, 
        onCancel,
        onSubmit } = props

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            _keyboardDidShow,
        )

        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            _keyboardDidHide,
        )

        return () => {
            keyboardDidShowListener.remove()
            keyboardDidHideListener.remove()
        }

    }, [])

    const _keyboardDidShow = () => {
        setKeyboardShow(true)
    }
    
    const _keyboardDidHide = () => {
        setKeyboardShow(false)
    }

    const _onImageLayout = (event) => {
        const { x, y, height, width } = event.nativeEvent.layout
        setImageHeight(height)
    }

    return (
        <SafeAreaView style={styles.container} >
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    {
                        !keyboardShow ? 
                            <View style={{ flex: 0.1, paddingHorizontal: 10, justifyContent: 'center', borderWidth: 1, borderColor: '#000000' }} >
                                { methodType === 'edit' ?
                                    <Switch 
                                        value={isEdit} 
                                        onValueChange={setIsEdit}
                                        trackColor={{ true: '#3BB9FF', false: '#FFFFFF' }}  
                                        thumbColor='#FFFFFF' />
                                        : null
                                }
                            </View>
                            : 
                            null
                    }
                    {
                        !keyboardShow ? 
                            <View 
                                onLayout={(event) => _onImageLayout(event)}
                                style={[ styles.image, { borderWidth: 1, borderColor: '#000000' }]} >
                                <IPhotoChooser 
                                    photo={image} 
                                    onPhotoChange={setImage} 
                                    iconType='Entypo' 
                                    iconName='folder-images' 
                                    disabled={!isEdit}
                                    imageStyle={{ 
                                        width: imageHeight, 
                                        height: imageHeight
                                    }} />
                            </View>
                            :
                            null
                    }
                    {
                        isEdit ? 
                            <View style={styles.body} >
                                <Label style={styles.label} >Drug name</Label>
                                <ITextInputWithLabel value={name} onChangeText={setName} />
                                <Label style={styles.label} >Description</Label>
                                <ITextArea rowSpan={Screen.isPhone() ? 3 : 7} value={description} onChangeText={setDescription} />
                            </View>
                            :
                            <View style={styles.body} >
                                <Label style={styles.label} >Drug name</Label>
                                <Text style={{ color: '#FFFFFF' }}>{name}</Text>
                                <Label style={styles.label} >Description</Label>
                                {
                                    description? 
                                        <ViewMoreText
                                            numberOfLines={6}
                                            renderViewMore={
                                                () => <Text 
                                                            onPress={() => Navigator.navigate('MemberDescriptionMore', { description: description })} 
                                                            style={{ color: 'blue', fontSize: 14 }}>
                                                                View more
                                                    </Text>
                                            } >
                                                <Text style={{ color: '#FFFFFF', fontSize: 14 }}>{ description }</Text>
                                        </ViewMoreText>
                                    :
                                    null
                                }
                            </View>
                    
                    }
                    {
                        isEdit && !keyboardShow ? 
                            <View style={styles.footer} >
                                <IButton text='Done' onPress={onSubmit} width={'50%'} />
                            </View>
                            :
                            null
                    }
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IConfirmDialog message={dialogMessage} onConfirm={onConfirm} onCancel={onCancel} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    image: { flex: 0.3, overflow: 'hidden' },
    body: { 
        flex: 0.9, 
        alignItems: 'center', 
        paddingHorizontal: 20,
    },
    footer: { flex: 0.15, flexDirection: 'row', justifyContent: 'space-around', paddingVertical: 5 },
    label: { color: '#FFFFFF', marginVertical: 10 }

})