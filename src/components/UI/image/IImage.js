import React, { useState } from 'react'
import { ActivityIndicator, Image, Platform, StyleSheet, View, TouchableHighlight } from 'react-native'
import { Icon } from 'native-base'

export default (props) => {
    const [ isLoading, setIsloading ] = useState(true)
    const [ isError, setIsError ] = useState(false)
    
    const { uri, containerStyle, imageStyle, onPress, onLongPress, checked } = props

    return (
        <TouchableHighlight onPress={() => onPress && onPress()} onLongPress={() => onLongPress && onLongPress()} >
            <View style={[ styles.container, containerStyle ]} >
            
                    <Image 
                        resizeMode='contain' 
                        style={[ styles.image, imageStyle ]} 
                        source={{ 
                            // uri: API_IMAGE_PATH + '/' + item.path + '/' + item.name 
                            uri: uri
                        }}
                        onLoadStart={() => {console.log('onLoadStart');setIsloading(true)}}
                        onLoadEnd={() => {console.log('onLoadEnd');setIsloading(false)}}
                        onError={() => { 
                            setIsError(true)
                            setIsloading(false)
                        }} />
                {
                    isError ?
                        <TouchableHighlight 
                            style={[ styles.imageContainer, imageStyle ]} 
                            onPress={() => onPress && onPress()}
                            onLongPress={() => onLongPress && onLongPress()} >
                            <Icon type='Entypo' name='warning' style={{ fontSize: 90, color: 'blue' }} />
                        </TouchableHighlight>
                        : 
                        null
                }   

                {
                    checked ?
                        <TouchableHighlight 
                            style={[ styles.imageContainer, { zIndex: 999 }  ]} 
                            onPress={() => onPress && onPress()}
                            onLongPress={() => onLongPress && onLongPress()} >
                            <Icon type='FontAwesome5' name='check' style={{ fontSize: 60, color: 'red' }} />
                        </TouchableHighlight>
                        : 
                        null
                }   


                <ActivityIndicator
                    style={styles.activityIndicator}
                    animating={isLoading} />
            </View>

        </TouchableHighlight>
    )

}

const styles = StyleSheet.create({
    container: { flex: 1 },
    image: { height: Platform.OS ==='ios' ? 0 : null },
    activityIndicator: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    imageContainer: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0 
    }
})
