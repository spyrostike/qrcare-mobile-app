import React from 'react'
import { ProgressDialog } from 'react-native-simple-dialogs'

export default (props) => {
    const { isLoading, title } = props

    return (
        <ProgressDialog
            visible={isLoading}
            message={title || 'กำลังโหลดข้อมูล'}
            animationType={'fade'}
            dialogStyle={{ borderRadius: 5 }} />
    )
}
