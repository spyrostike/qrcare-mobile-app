import React from 'react'
import { ConfirmDialog } from 'react-native-simple-dialogs'
import { Text, View } from 'react-native'

export default (props) => {
    const { message, onConfirm, onCancel } = props

    return (
        <ConfirmDialog
            title='Error'
            visible={message != null}
            positiveButton={{
                title: 'ok',
                titleStyle: { color: '#000000' },
                onPress: () => onConfirm()
            }}
            negativeButton={{
                title: 'cancel',
                titleStyle: { color: '#000000' },
                onPress: () => onCancel()
            }}
            animationType={'fade'}
            dialogStyle={{ borderRadius: 5 }} >

            <View>
                <Text>{message !== null ? message: ''}</Text>
            </View>
        </ConfirmDialog>
    )
}
