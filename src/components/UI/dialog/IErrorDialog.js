import React, { useState } from 'react'
import { ConfirmDialog } from 'react-native-simple-dialogs'
import { Text, View } from 'react-native'

export default (props) => {
    const [ errorMsg, setErrorMsg ] = useState(null)

    const { errorMessage, setErrorMessage } = props

    setTimeout(() => {
        setErrorMsg(errorMessage)
    }, 300)

    return (
        <ConfirmDialog
            title='Error'
            visible={errorMsg != null}
            positiveButton={{
                title: 'ตกลง',
                titleStyle: { color: '#000000' },
                onPress: () => setErrorMessage(null)
            }}
            animationType={'fade'}
            dialogStyle={{ borderRadius: 5 }} >

            <View>
                <Text>{errorMsg !== null ? errorMsg: ''}</Text>
            </View>
        </ConfirmDialog>
    )
}
