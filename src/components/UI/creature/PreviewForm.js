import React, { useEffect, useState } from 'react'
import { Image, Keyboard, SafeAreaView, ScrollView, StyleSheet, TouchableWithoutFeedback, View } from 'react-native'
import { Switch } from 'native-base'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextWithLabel from '../text/ITextWithLabel'
import IImageChooserInput from '../photo/IImageChooserInput'
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import IPickerWithLabel from '../input/IPickerWithLabel'
import IDatePickerWithLabel from '../input/IDatePickerWithLabel'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IButton from '../button/IButton'

export default (props) => {
    const [ keyboardShow, setKeyboardShow ] = useState(false)

    const { 
        viewOnly,
        creatureImage,
        firstName,
        lastName,
        birthCertificate,
        gender,
        bloodType,
        birthDate,
        genderName,
        bloodTypeName,
        drug,
        drugAllergy,
        foodAllergy,
        weight,
        height,
        isLoading,
        isEdit,
        setCreatureImage,
        setLastName,
        setGender,
        setBloodType,
        setBirthCertificate,
        setIsEdit,
        setFirstName,
        setBirthDate,
        setWeight,
        setHeight,
        errorMessage,
        setErrorMessage, 
        goToScreen,
        onSubmit } = props
        
    const genderItems = [
        { label: 'male', value: 1 },
        { label: 'female', value: 2 }
    ]

    const bloodTypeItems = [
        { label: 'AB', value: 1 },
        { label: 'A', value: 2 },
        { label: 'B', value: 3 },
        { label: 'O', value: 4 }
    ]
    
    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            _keyboardDidShow,
        )

        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            _keyboardDidHide,
        )

        return () => {
            keyboardDidShowListener.remove()
            keyboardDidHideListener.remove();
        }

    }, [])

    const _keyboardDidShow = () => {
        setKeyboardShow(true)
    }
    
    const _keyboardDidHide = () => {
        setKeyboardShow(false)
    }

    return (
        <SafeAreaView style={styles.container}>
            {
                !keyboardShow ? 
                    <View style={{ flex: 0.1, paddingHorizontal: 10, justifyContent: 'center' }} >
                        {
                            !viewOnly ?
                                <Switch 
                                    value={isEdit} 
                                    onValueChange={setIsEdit}
                                    trackColor={{ true: '#3BB9FF', false: '#FFFFFF' }}  
                                    thumbColor='#FFFFFF' />
                                    : null
                        }
                    </View>
                    :
                    null
            }
            {
                !keyboardShow ? 
                    <View style={[ styles.image, isEdit && { flex: 0.3} ]}>
                        <IPhotoChooser 
                            photo={creatureImage} 
                            onPhotoChange={setCreatureImage} 
                            disabled={!isEdit}
                            iconType='Octicons' 
                            iconName='person'/>
                    </View>
                    :
                    null
            }
            
            {
                isEdit ? 
                    null
                    : 
                    <ScrollView style={[ styles.info, viewOnly && { flex: 0.7 } ]}>
                        <ITextWithLabel label='Firstname : ' value={firstName} />
                        <ITextWithLabel label='Lastname : ' value={lastName} />
                        <ITextWithLabel label='Gender : ' value={genderName} />
                        <ITextWithLabel label='Date of birth : ' value={birthDate} />
                        <ITextWithLabel label='Blood type : ' value={bloodTypeName} />
                        <ITextWithLabel label='Weight : ' value={weight} />
                        <ITextWithLabel label='Height : ' value={height} />
                        <ITextWithLabel 
                            label='Drug : ' 
                            value={drug} onPress={() => goToScreen('DrugList')} 
                            textStyle={{ color: drug === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                        <ITextWithLabel 
                            label='Drug allergy : ' 
                            value={drugAllergy} 
                            onPress={() => goToScreen('DrugAllergyList')} 
                            textStyle={{ color: drugAllergy === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                        <ITextWithLabel 
                            label='Food allergy : ' 
                            value={foodAllergy} 
                            onPress={() => goToScreen('FoodAllergyList')} 
                            textStyle={{ color: foodAllergy === 'None' ? '#FFFFFF' : '#3BB9FF' }} />
                    </ScrollView>
            }
            {
                isEdit ?
                    <ScrollView style={styles.scroll}>
                        <ITextInputWithLabel label='Firstname : ' value={firstName} onChangeText={setFirstName} />
                        <ITextInputWithLabel label='Lastname : ' value={lastName} onChangeText={setLastName} />
                        <IImageChooserInput label='Birth certificate : ' value={birthCertificate} onImageChange={setBirthCertificate} />
                        <IPickerWithLabel label='Gender : ' selectedValue={gender} onValueChange={setGender} items={genderItems} />
                        <IPickerWithLabel label='Blood type : ' selectedValue={bloodType} onValueChange={setBloodType} items={bloodTypeItems} />
                        <IDatePickerWithLabel label='Date of birth : ' date={birthDate} onDayPress={setBirthDate} />
                        <ITextInputWithLabel label='Weight : ' value={weight} onChangeText={setWeight} keyboardType={'numeric'} />
                        <ITextInputWithLabel label='Height : ' value={height} onChangeText={setHeight} keyboardType={'numeric'} />
                    </ScrollView>
                    :
                        viewOnly ?
                            <View style={styles.footer}>
                                <TouchableWithoutFeedback onPress={() => goToScreen('MedicalInfo') } >
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../../assets/images/medical.png')}
                                        style={styles.imageFooterViewOnly} />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => goToScreen('TreatmentHistoryList') } >
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../../assets/images/treatment.png')}
                                        style={styles.imageFooterViewOnly} />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => goToScreen('VaccineBookList') } >
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../../assets/images/vaccine.png')}
                                        style={styles.imageFooterViewOnly} />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => goToScreen('ParentInfo') } >
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../../assets/images/parent.png')}
                                        style={styles.imageFooterViewOnly} />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => goToScreen('LocationInfo') } >
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../../assets/images/location.png')}
                                        style={styles.imageFooterViewOnly} />
                                </TouchableWithoutFeedback>
                            </View>
                            :
                            <View style={styles.footer}>
                                <TouchableWithoutFeedback onPress={() => goToScreen('MedicalInfo') } >
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../../assets/images/medical.png')}
                                        style={styles.imageFooter} />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => goToScreen('TreatmentHistoryList') } >
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../../assets/images/treatment.png')}
                                        style={styles.imageFooter} />
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => goToScreen('VaccineBookList') } >
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../../assets/images/vaccine.png')}
                                        style={styles.imageFooter} />
                                </TouchableWithoutFeedback>
                            </View>
            }
            {
                isEdit ?
                    <View style={styles.footer}>
                        <IButton text='Submit' onPress={onSubmit} width={'50%'} />
                    </View>
                :
                null
            }
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    image: { flex: 0.3, overflow: 'hidden' },
    photo: {
        width: 140, 
        height: 140, 
        alignSelf: 'center',
        overflow: 'hidden'
    },
    info: { flex: 0.6, paddingHorizontal: 5 },
    scroll: { flex: 0.5, paddingHorizontal: 5 },
    imageFooter: {
        width: 100, 
        height: '100%'
    },
    imageFooterViewOnly: {
        width: 80, 
        height: '100%'
    },
    footer: {
        flex: 0.2,
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
})