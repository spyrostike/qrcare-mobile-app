import React from 'react'
import { Image, SafeAreaView, StyleSheet, TouchableHighlight, View } from 'react-native'
import IGridData from '../list/IGridData'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

export default (props) => {
    const { 
        list, 
        onItemPress, 
        errorMessage, 
        setErrorMessage, 
        isLoading,
        gotoScanScreen,
        onItemLongPress } = props
    
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.grid} >
                <IGridData 
                    items={list} 
                    onItemPress={onItemPress} 
                    onItemLongPress={onItemLongPress} />
            </View>
            <View style={styles.footer} >
                <TouchableHighlight style={{ width: '50%', }} onPress={gotoScanScreen}>
                    <Image
                        resizeMode='contain'
                        source={require('../../../assets/images/qr_scaner.png')}
                        style={{ 
                            width: '100%',
                            height: '100%'
                        }} />
                </TouchableHighlight>
            </View>
            {/* <ILoadingDialog isLoading={isLoading} /> */}
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, flexDirection: 'column', backgroundColor: '#000000' },
    grid: { flex: 0.9 },
    footer: { flex: 0.1, justifyContent: 'center', alignItems: 'center' }
})