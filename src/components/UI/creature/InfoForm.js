import React, { useEffect, useState } from 'react'
import { Keyboard, SafeAreaView, ScrollView, StyleSheet, TouchableWithoutFeedback, View } from 'react-native'
import IPhotoChooser from '../photo/IPhotoChooser'
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import IPickerWithLabel from '../input/IPickerWithLabel'
import IDatePickerWithLabel from '../input/IDatePickerWithLabel'
import IButton from '../button/IButton'
import IImageChooserInput from '../photo/IImageChooserInput'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

export default (props) => {
    const [ keyboardShow, setKeyboardShow ] = useState(false)

    const { 
        creatureImage,
        firstName,
        lastName,
        birthCertificate,
        gender,
        bloodType,
        birthDate,
        weight,
        height,
        isLoading,
        errorMessage,
        setCreatureImage,
        setFirstName,
        setLastName,
        setBirthCertificate,
        setGender,
        setBloodType,
        setBirthDate,
        setWeight,
        setHeight,
        setErrorMessage, 
        onLogoPress,
        onSubmit } = props

    const genderItems = [
        { label: 'male', value: 1 },
        { label: 'female', value: 2 }
    ]

    const bloodTypeItems = [
        { label: 'AB', value: 1 },
        { label: 'A', value: 2 },
        { label: 'B', value: 3 },
        { label: 'O', value: 4 }
    ]

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            _keyboardDidShow,
        )

        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            _keyboardDidHide,
        )

        return () => {
            keyboardDidShowListener.remove()
            keyboardDidHideListener.remove();
        }

    }, [])

    const _keyboardDidShow = () => {
        setKeyboardShow(true)
    }
    
    const _keyboardDidHide = () => {
        setKeyboardShow(false)
    }
    
    return (
        <SafeAreaView style={styles.container}>
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    {
                        !keyboardShow ? 
                            <View style={styles.image}>
                                <IPhotoChooser 
                                    photo={creatureImage} 
                                    onPhotoChange={setCreatureImage} 
                                    iconType='Octicons' 
                                    iconName='person'/>
                            </View>
                            :
                            null
                    }
                    <ScrollView style={styles.scroll}>
                        <ITextInputWithLabel label='Firstname : ' value={firstName} onChangeText={setFirstName} />
                        <ITextInputWithLabel label='Lastname : ' value={lastName} onChangeText={setLastName} />
                        <IImageChooserInput label='Birth certificate : ' value={birthCertificate} onImageChange={setBirthCertificate} />
                        <IPickerWithLabel label='Gender : ' selectedValue={gender} onValueChange={setGender} items={genderItems} />
                        <IPickerWithLabel label='Blood type : ' selectedValue={bloodType} onValueChange={setBloodType} items={bloodTypeItems} />
                        <IDatePickerWithLabel label='Date of birth : ' date={birthDate} onDayPress={setBirthDate} />
                        <ITextInputWithLabel label='Weight : ' value={weight} onChangeText={setWeight} keyboardType={'numeric'} />
                        <ITextInputWithLabel label='Height : ' value={height} onChangeText={setHeight} keyboardType={'numeric'} />
                    </ScrollView>
                    <View style={styles.footer}>
                        <IButton text='Submit' onPress={onSubmit} width={'50%'} />
                    </View>
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    image: { flex: 0.4, overflow: 'hidden' },
    scroll: { flex: 0.5, paddingHorizontal: 5 },
    footer: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
})