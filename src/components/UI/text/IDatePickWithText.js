import React from 'react'
import { View } from 'react-native'
import DatePicker from 'react-native-datepicker'

export default (props) => {

    const { onDayPress, date, containerStyle, width, alignItems, disabled } = props 

    const _onDayPress = (value) => {
        onDayPress && onDayPress(value)
    }

    return (
        <View style={[ { flex: 1 }, containerStyle ]} >
           <DatePicker
                style={{ width: width || '100%' }}
                date={date}
                mode='date'
                disabled={disabled}
                placeholder='select date'
                format='DD/MM/YYYY'
                confirmBtnText='Confirm'
                cancelBtnText='Cancel'
                showIcon={false}
                disabled={disabled}
                customStyles={{
                    dateText: {
                        color: '#FFFFFF'
                    },
                    dateInput: {
                        paddingLeft: 0,
                        alignItems: alignItems || 'flex-start',
                        borderWidth: 0
                    },
                    disabled: {
                        backgroundColor: '#000000'
                    }
                }}
                onDateChange={_onDayPress} />

        </View>
    )
}
