import React from 'react'
import { View } from 'react-native'
import DatePicker from 'react-native-datepicker'

export default (props) => {

    const { onTimeChange, time, containerStyle, width, alignItems, disabled } = props 

    const _onTimeChange = (value) => {
        onTimeChange && onTimeChange(value)
    }

    return (
        <View style={[ { flex: 1 }, containerStyle ]} >
           <DatePicker
                style={{ width: width || '100%' }}
                date={time}
                mode='time'
                is24Hour={true}
                placeholder='select date'
                confirmBtnText='Confirm'
                cancelBtnText='Cancel'
                dis
                disabled={disabled}
                showIcon={false}
                customStyles={{
                    dateText: {
                        color: '#FFFFFF'
                    },
                    dateInput: {
                        paddingLeft: 0,
                        alignItems: alignItems || 'flex-start',
                        borderWidth: 0
                    },
                    disabled: {
                        backgroundColor: '#000000'
                    }
                }}
                onDateChange={_onTimeChange} />

        </View>
    )
}
