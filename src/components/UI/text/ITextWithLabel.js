  
import React from 'react'
import { StyleSheet, Text, TouchableHighlight, View } from 'react-native'

export default (props) => {

    const { 
        label, 
        value,
        labelStyle,
        textStyle,
        containerStyle,
        onPress } = props

    return (
        <View style={[ styles.container, containerStyle ]} >
            {
                label && <Text style={[ styles.label, labelStyle ]} >{ label }</Text>
            }

            {
                onPress ? 
                    <TouchableHighlight onPress={onPress} >
                        <Text style={[ styles.text, textStyle ]} >{value}</Text>
                    </TouchableHighlight>
                    :
                    <Text style={[ styles.text, textStyle ]} >{value}</Text> 
            }
            
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        // backgroundColor: '#FFFFFF', 
        flexDirection: 'row',
        marginVertical: 5,
        paddingHorizontal: 5,
        borderBottomWidth: 0,
        justifyContent: 'flex-start'
    },
    label: {
        color: '#FFFFFF'
    },
    text: { 
        color: '#FFFFFF',
        flex: 1
    }
})