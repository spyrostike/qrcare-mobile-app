import React from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import IGridData from '../list/IGridData'
import IButton from '../button/IButton'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'

export default (props) => {
    const { 
        viewOnly, 
        items, 
        checkItems,
        mode,
        onItemPress, 
        errorMessage, 
        setErrorMessage, 
        isLoading,
        onItemLongPress,
        onCheckBoxPress,
        onSubmit,
        onClearCheckList } = props
    
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.grid} >
                <IGridData 
                    viewOnly={viewOnly} 
                    items={items} 
                    onItemPress={onItemPress} 
                    onItemLongPressCustom={() => onItemLongPress && onItemLongPress('delete')}
                    checkItems={checkItems}
                    onCheckBoxPress={onCheckBoxPress}
                    checkBoxColor={'red'}
                    checkBoxMode={mode === 'delete' ? true : false} />
            </View>
            {
                mode === 'delete' ? 
                    <View style={styles.footer} >
                        <IButton text='Delete' onPress={onSubmit} width={'30%'} />
                        <IButton text='Cancel' onPress={onClearCheckList} width={'30%'} />
                    </View>
                    :
                    null
            }   
            <ILoadingDialog isLoading={isLoading} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, flexDirection: 'column', backgroundColor: '#000000' },
    header: { flex: 0.1,  padding: 5, flexDirection: 'row' },
    grid: { flex: 1 },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' }
})