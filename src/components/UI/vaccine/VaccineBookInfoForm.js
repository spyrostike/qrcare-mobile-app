import React from 'react'
import { Dimensions, SafeAreaView, ScrollView, StyleSheet, View } from 'react-native'
import { Switch } from 'react-native'
import IPickerWithLabel from '../input/IPickerWithLabel'
import ITextWithLabel from '../text/ITextWithLabel'
import ITextInputWithLabel from '../input/ITextInputWithLabel'
import IButton from '../button/IButton'
import IVaccineButtonGroup from '../button/IVaccineButtonGroup'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IConfirmDialog from '../dialog/IConfirmDialog'

const dimensions = Dimensions.get('window')

export default (props) => {
    const { 
        methodType,
        vaccineType,
        vaccineTypeName,
        other,
        items,
        isLoading,
        errorMessage,
        dialogMessage,
        isEdit, 
        setVaccineType,
        setOther,
        setErrorMessage, 
        setIsEdit, 
        onItemPress, 
        onItemLongPress, 
        onConfirm,
        onCancel, 
        onSubmit } = props

    const vaccineTypeItems = [
        { label: 'BCG', value: 1 },
        { label: 'HBV', value: 2 },
        { label: 'Dtap, Tdap', value: 3 },
        { label: 'IPV', value: 4 },
        { label: 'Live JE', value: 5 },
        { label: 'Hib', value: 6 },
        { label: 'HAV', value: 7 },
        { label: 'VZV or MMRV', value: 8 },
        { label: 'Influenza', value: 9 },
        { label: 'PCV', value: 10 },
        { label: 'Rota', value: 11 },
        { label: 'HPV', value: 12 },
        { label: 'DEN', value: 13 },
        { label: 'Other', value: 14 }
    ]

    return (
        <SafeAreaView  style={styles.container} >
            {
                methodType === 'edit' ?
                    <View style={{ flex: 0.05, paddingHorizontal: 10 }} >
                        <Switch 
                            value={isEdit} 
                            onValueChange={setIsEdit}
                            trackColor={{ true: '#3BB9FF', false: '#FFFFFF' }}  
                            thumbColor='#FFFFFF' />
                    </View>
                    :
                    null
            }
            <ScrollView style={{ flex: methodType === 'edit' ? 0.85 : 0.9 }} scrollEnabled={false} >
                <View style={{ height: methodType === 'edit' ? dimensions.height * 0.90 : dimensions.height * 0.95 }} >
                    <View style={styles.body} >
                        {
                            items && items.length === 0 && isEdit ? 
                                <IPickerWithLabel label='Vaccine name : ' selectedValue={vaccineType} onValueChange={setVaccineType} items={vaccineTypeItems} />
                                :
                                <ITextWithLabel label='Vaccine name : ' value={vaccineTypeName} />
                        }
                        {
                            vaccineType === 14 ?
                                items && items.length === 0 && isEdit ? 
                                    <ITextInputWithLabel label='Other : ' value={other} onChangeText={setOther} />
                                    :
                                    <ITextWithLabel label='Other : ' value={other} />
                                :
                                null
                        }
                    </View>
                    <View style={styles.vaccineButtonGroupContainer}>
                        <IVaccineButtonGroup items={items} onItemPress={onItemPress} onItemLongPress={isEdit ? onItemLongPress : null} />
                    </View>
                    <View style={styles.footer} >
                        {
                            isEdit ? <IButton text='Done' onPress={onSubmit} width={'50%'} /> : null
                        }
                    </View>
                </View>
            </ScrollView>
            <ILoadingDialog isLoading={isLoading} />
            <IConfirmDialog message={dialogMessage} onConfirm={onConfirm} onCancel={onCancel} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    body: { 
        flex: 0.2, 
        paddingHorizontal: 20,
    },
    vaccineButtonGroupContainer: { 
        backgroundColor: '#000000',
        flex: 0.6, 
        // padding: 5, 
        flexDirection: 'row'
    },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around', backgroundColor: '#000000' },
    label: { color: '#FFFFFF', marginVertical: 10 }

})