import React from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import ITextWithLabel from '../text/ITextWithLabel'
import IVaccineButtonGroup from '../button/IVaccineButtonGroup'
import ILoadingDialog from '../dialog/ILoadingDialog'
import IErrorDialog from '../dialog/IErrorDialog'
import IConfirmDialog from '../dialog/IConfirmDialog'

export default (props) => {
    const { 
        vaccineType,
        vaccineTypeName, 
        other,
        items,
        isLoading,
        errorMessage,
        dialogMessage,
        setErrorMessage, 
        onItemPress, 
        onConfirm,
        onCancel } = props

    return (
        <SafeAreaView  style={styles.container} >
            <View style={styles.body} >
                <ITextWithLabel label='Vaccine name : ' value={vaccineTypeName} />
                { vaccineType === 14 ?<ITextWithLabel label='Other: ' value={other} /> : null }
            </View>
            <View style={styles.vaccineButtonGroupContainer}>
                <IVaccineButtonGroup items={items} onItemPress={onItemPress} />
            </View>

            <ILoadingDialog isLoading={isLoading} />
            <IConfirmDialog message={dialogMessage} onConfirm={onConfirm} onCancel={onCancel} />
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    body: { 
        flex: 0.2,
        paddingHorizontal: 20
    },
    vaccineButtonGroupContainer: { 
        backgroundColor: '#000000',
        flex: 0.6, 
        padding: 5, 
        flexDirection: 'row'
    },
    label: { color: '#FFFFFF', marginVertical: 10 }

})