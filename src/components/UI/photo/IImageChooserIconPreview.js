  
import React, { useState } from 'react'
import { Image, Modal, StyleSheet, TouchableHighlight, View } from 'react-native'
import { Icon, Item, Label } from 'native-base'
import ImageViewer from 'react-native-image-zoom-viewer'
import IHeaderImageViewer from '../photo/IHeaderImageViewer'

export default (props) => {
    const [ visible, setVisible ] = useState(false)
    const { 
        label, 
        items, 
        containerStyle, 
        labelStyle } = props
    
    const _renderHeader = () => {
        return (
            <IHeaderImageViewer onPress={() => setVisible(false) } />
        )
    }

    return (
        <Item style={[ styles.container, containerStyle ]} >
            {
                label && <Label style={[ styles.label, labelStyle]} >{ label }</Label>
            }
            { 
                <TouchableHighlight 
                    style={[ styles.imageContainer ]} 
                    onPress={() => setVisible(true)} >
                    {
                        items && items.length > 0 ? 
                            <Image
                                resizeMode='contain'
                                source={require('../../../assets/images/green_light.png')}
                                style={[ styles.image, { width: 50, height: 50 } ]} />
                            :
                            <Image
                                resizeMode='contain'
                                source={require('../../../assets/images/none_light.png')}
                                style={[ styles.image, { width: 50, height: 50 } ]} />
                    }
                    
                </TouchableHighlight>
            }

            <Modal visible={visible} transparent={true}>
                <ImageViewer 
                    renderHeader={() => _renderHeader()}
                    saveToLocalByLongPress={false}
                    enableSwipeDown
                    onSwipeDown={() => setVisible(false)}
                    imageUrls={items}/>
            </Modal>
        </Item>
    )

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#000000', 
        marginVertical: 5,
        paddingHorizontal: 5,
        borderColor: '#000000' 
    },
    label: {
        color: '#FFFFFF'
    },
    input: { 
        // backgroundColor: '#FFFFFF'
    },
    imageContainer: {
        backgroundColor: '#000000', 
        right: 5,
        width: 50
    },
    image: { 
        // flex: 1,
        width: 35,
        height: 35
    }
})