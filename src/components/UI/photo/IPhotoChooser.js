import React, { useState } from 'react'
import { Image, Modal, PermissionsAndroid, StyleSheet, TouchableHighlight, View } from 'react-native'
import { Icon } from 'native-base'
import ModalSelector from 'react-native-modal-selector'
import ImagePicker from 'react-native-image-crop-picker'
import ImageViewer from 'react-native-image-zoom-viewer'
import IHeaderImageViewer from '../photo/IHeaderImageViewer'

export default (props) => {
    const [ visible, setVisible ] = useState(false)

    const { photo, onPhotoChange, iconType, iconName, imageStyle, disabled } = props

    const  checkPermission = async () => {
        permissions = [
            PermissionsAndroid.PERMISSIONS.CAMERA,
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
        ]

        if (Platform.OS === 'android') {
            await PermissionsAndroid.requestMultiple(permissions)
         }
    }

    checkPermission()

    const data = [
        { key: 0, label: 'Camera' },
        { key: 1, label: 'Select photo' }
    ]

    photo && data.push({ key: 2, label: 'Preview' })

    const images = [{
        url: photo,
        props: { }
    }]
    
    const _camaraPickerAndCrop = () => {
        ImagePicker.openCamera({
            mediaType: 'photo'
        })
        .then(image => {
            ImagePicker.openCropper({
                path: image.path,
                // width: 300,
                // height: 300
            }).then(image => {
                onPhotoChange ? onPhotoChange(image) : null
            })
            
        })
        .catch((err) => { console.log("openCamera catch" + err.toString()) })
    }

    const _imagePickerAndCrop = () => {
        ImagePicker.openPicker({
            mediaType:  'photo'
        })
        .then(image => {
            ImagePicker.openCropper({
                path: image.path,
                // width: 300,
                // height: 300
            }).then(image => {
                onPhotoChange ? onPhotoChange(image) : null
            })
        })
        .catch((err) => { console.log("openCamera catch" + err.toString()) })

    }

    const _onChange = (index) => {
        if (index === 0) _camaraPickerAndCrop()
        else if (index === 1) _imagePickerAndCrop()
        else if (index === 2) setVisible(true)
    }

    const _renderHeader = () => {
        return (
            <IHeaderImageViewer onPress={() => setVisible(false) } />
        )
    }

    return (
        <View style={styles.container}>
            {
                disabled ?
                    <TouchableHighlight onPress={() => setVisible(true) } >
                        <Image 
                            resizeMode='cover'
                            source={{ uri: photo }}
                            style={[ styles.photo, imageStyle ]} />
                    </TouchableHighlight>
                    :
                    <ModalSelector
                        data={data}
                        initValue="Select something yummy!"
                        accessible={true}
                        optionContainerStyle={{ backgroundColor: '#FFFFFF' }}
                        cancelStyle={{ backgroundColor: '#FFFFFF' }}
                        disabled={disabled}
                        onChange={(value)=> _onChange(value.key) } >
                        {
                            photo ?
                                <Image 
                                    resizeMode='cover'
                                    source={{ uri: photo }}
                                    style={[ styles.photo, imageStyle ]} />
                                : 
                                <Icon 
                                    type={ iconType ? iconType : 'Entypo' }  
                                    name={ iconName ? iconName : 'image' }  
                                    style={[ styles.photoPreview, { fontSize: imageStyle ? imageStyle.height : 160 } ]} />

                        }

                    </ModalSelector>
            }
            

            <Modal visible={visible} transparent={true}>
                <ImageViewer 
                    renderHeader={() => _renderHeader()}
                    saveToLocalByLongPress={false}
                    enableSwipeDown
                    onSwipeDown={() => setVisible(false)}
                    imageUrls={images}/>
            </Modal>
        </View>
    )

}

const styles = StyleSheet.create({
    container: { 
        flex: 1, 
        padding: 5,
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    photo: {
        width: 160, 
        height: 160, 
        alignSelf: 'center',
        overflow: 'hidden'
    },
    photoPreview: { 
        fontSize: 160, 
        color: 'blue'
    },
    album: { 
        position: 'absolute', 
        bottom: 5, 
        right: 55, 
        fontSize: 35, 
        color: 'blue' 
    },
    camera: { 
        position: 'absolute', 
        bottom: 5, 
        right: 5, 
        fontSize: 35, 
        color: 'blue' 
    }
})
