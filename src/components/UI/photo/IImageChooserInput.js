  
import React, { useState } from 'react'
import { Image, Modal, PermissionsAndroid, StyleSheet, TouchableHighlight, View } from 'react-native'
import { Input, Item, Label } from 'native-base'
import ModalSelector from 'react-native-modal-selector'
import ImagePicker from 'react-native-image-crop-picker'
import ImageViewer from 'react-native-image-zoom-viewer'
import IHeaderImageViewer from '../photo/IHeaderImageViewer'

export default (props) => {
    const [ visible, setVisible ] = useState(false)

    const { 
        label, 
        value, 
        containerStyle, 
        labelStyle, 
        inputStyle,
        onImageChange } = props

    let modalSelectItems = [
        { key: 0, label: 'Camera' },
        { key: 1, label: 'Select photo' }
    ]
    
    value && modalSelectItems.push({ key: 2, label: 'Preview' })

    const images = [{
        url: value,
        props: { }
    }]

    const  _checkPermission = async () => {
        permissions = [
            PermissionsAndroid.PERMISSIONS.CAMERA,
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
        ]

        if (Platform.OS === 'android') {
            await PermissionsAndroid.requestMultiple(permissions)
        }
    }

    _checkPermission()

    const _camaraPickerAndCrop = () => {
        ImagePicker.openCamera({
            mediaType: 'photo'
        })
        .then(image => {
            ImagePicker.openCropper({
                path: image.path,
                // width: 300,
                // height: 300
            }).then(image => {
                onImageChange ? onImageChange(image) : null
            })
        })
        .catch((err) => { console.log("openCamera catch" + err.toString()) })
    }

    const _imagePickerAndCrop = () => {
        ImagePicker.openPicker({
            mediaType:  'photo',
            // width: 300,
            // height: 300
        })
        .then(image => {
            ImagePicker.openCropper({
                path: image.path
            }).then(image => {
                onImageChange ? onImageChange(image) : null
            })
        })
        .catch((err) => { console.log("openCamera catch" + err.toString()) })

    }

    const _onChange = (index) => {
        if (index === 0) _camaraPickerAndCrop()
        else if (index === 1) _imagePickerAndCrop()
        else if (index === 2) setVisible(true)
    }

    const _renderHeader = () => {
        return (
            <IHeaderImageViewer onPress={() => setVisible(false) } />
        )
    }

    return (
        <Item style={[ styles.container, containerStyle ]} >
            {
                label && <Label style={[ styles.label, labelStyle]} >{ label }</Label>
            }

            <Input style={[ styles.input, inputStyle]} disabled />

            { 
                <TouchableHighlight style={[ styles.imageContainer, { right: 40 } ]} underlayColor='#FFFFFF' >
                    {
                        value ? 
                            <Image
                                resizeMode='contain'
                                source={require('../../../assets/images/green_light.png')}
                                style={[ styles.image, { width: 50, height: 50 } ]} />
                            :
                            <Image
                                resizeMode='contain'
                                source={require('../../../assets/images/none_light.png')}
                                style={[ styles.image, { width: 50, height: 50 } ]} />
                    }
                    
                </TouchableHighlight>
            }
            
            
            {/* <TouchableHighlight style={styles.imageContainer} underlayColor='#FFFFFF' onPress={() => set} > */}

            <ModalSelector
                data={modalSelectItems}
                initValue="Select something yummy!"
                accessible={true}
                optionContainerStyle={{ backgroundColor: '#FFFFFF' }}
                cancelStyle={{ backgroundColor: '#FFFFFF' }}
                onChange={(value)=> _onChange(value.key) } >
                <Image
                    resizeMode='cover'
                    source={require('../../../assets/images/add_pic.png')}
                    style={[ styles.image ]} />
            </ModalSelector>

            <Modal visible={visible} transparent={true}>
                <ImageViewer 
                    renderHeader={() => _renderHeader()}
                    saveToLocalByLongPress={false}
                    enableSwipeDown
                    onSwipeDown={() => setVisible(false)}
                    imageUrls={images}/>
            </Modal>

        </Item>
    )

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF', 
        marginVertical: 5,
        paddingHorizontal: 5,
        borderRadius: 5
    },
    label: {
        color: '#000000'
    },
    input: { 
        // backgroundColor: '#FFFFFF'
    },
    imageContainer: {
        position: 'absolute',
        right: 5,
        flex: 0.1
    },
    image: { 
        // flex: 1,
        width: 35,
        height: 35
    }
})