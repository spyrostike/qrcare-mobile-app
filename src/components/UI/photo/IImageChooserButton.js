import React from 'react'
import { PermissionsAndroid } from 'react-native'
import ModalSelector from 'react-native-modal-selector'
import ImagePicker from 'react-native-image-crop-picker'
import IButton from '../button/IButton'

export default (props) => {
    const { 
        text, 
        constainerStyle, 
        buttonConstainerStyle,
        textStyle,
        onSuccess,
        disabled,
        width } = props
        
    const modalSelectItems = [
        { key: 0, label: 'Camera' },
        { key: 1, label: 'Select photo' }
    ]

    const  _checkPermission = async () => {
        permissions = [
            PermissionsAndroid.PERMISSIONS.CAMERA,
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
        ]

        if (Platform.OS === 'android') {
            await PermissionsAndroid.requestMultiple(permissions)
        }
    }

    _checkPermission()

    const _camaraPickerAndCrop = () => {
        ImagePicker.openCamera({
            mediaType: 'photo'
        })
        .then(image => {
            ImagePicker.openCropper({
                path: image.path,
                // width: 300,
                // height: 300
            }).then(image => {
                onSuccess ? onSuccess(image) : null
            })
        })
        .catch((err) => { console.log("openCamera catch" + err.toString()) })
    }

    const _imagePickerAndCrop = () => {
        ImagePicker.openPicker({
            mediaType:  'photo',
            // width: 300,
            // height: 300
        })
        .then(image => {
            ImagePicker.openCropper({
                path: image.path
            }).then(image => {
                onSuccess ? onSuccess(image) : null
            })
        })
        .catch((err) => { console.log("openCamera catch" + err.toString()) })

    }

    return (
            <ModalSelector
                data={modalSelectItems}
                disabled={disabled}
                initValue="Select something yummy!"
                accessible={true}
                style={{ width: width ? width : '50%', justifyContent: 'center' }}
                optionContainerStyle={{ backgroundColor: '#FFFFFF' }}
                cancelStyle={{ backgroundColor: '#FFFFFF' }}
                onChange={(value)=> value.key === 0 ? _camaraPickerAndCrop() : _imagePickerAndCrop() } >
                <IButton 
                    text={text} 
                    disabled={disabled}
                    constainerStyle={buttonConstainerStyle} 
                    width={'100%'}
                    textStyle={textStyle} />
            </ModalSelector>
    )
}