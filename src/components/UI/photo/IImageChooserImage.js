import React from 'react'
import { Image, PermissionsAndroid, StyleSheet, View } from 'react-native'
import ModalSelector from 'react-native-modal-selector'
import ImagePicker from 'react-native-image-crop-picker'

export default (props) => {

    const { onPhotoChange, imageStyle, disabled } = props

    const  checkPermission = async () => {
        permissions = [
            PermissionsAndroid.PERMISSIONS.CAMERA,
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
        ]

        if (Platform.OS === 'android') {
            await PermissionsAndroid.requestMultiple(permissions)
         }
    }

    checkPermission()

    const data = [
        { key: 0, label: 'Camera' },
        { key: 1, label: 'Select photo' }
    ]
    
    const _camaraPickerAndCrop = () => {
        ImagePicker.openCamera({
            mediaType: 'photo'
        })
        .then(image => {
            ImagePicker.openCropper({
                path: image.path,
                // width: 300,
                // height: 300
            }).then(image => {
                onPhotoChange ? onPhotoChange(image) : null
            })
            
        })
        .catch((err) => { console.log("openCamera catch" + err.toString()) })
    }

    const _imagePickerAndCrop = () => {
        ImagePicker.openPicker({
            mediaType:  'photo'
        })
        .then(image => {
            ImagePicker.openCropper({
                path: image.path,
                // width: 300,
                // height: 300
            }).then(image => {
                onPhotoChange ? onPhotoChange(image) : null
            })
        })
        .catch((err) => { console.log("openCamera catch" + err.toString()) })

    }

    const _onChange = (index) => {
        if (index === 0) _camaraPickerAndCrop()
        else if (index === 1) _imagePickerAndCrop()
    }
    
    return (
        <View style={styles.container}>
            {
                disabled ? 
                    <Image resizeMode='contain' style={[ styles.imageThumbnail, imageStyle ]} source={require('../../../assets/images/plus.png')} />
                    :
                    <ModalSelector
                        data={data}
                        initValue="Select something yummy!"
                        accessible={true}
                        optionContainerStyle={{ backgroundColor: '#FFFFFF' }}
                        cancelStyle={{ backgroundColor: '#FFFFFF' }}
                        disabled={disabled}
                        onChange={(value)=> _onChange(value.key) } >
                        <Image resizeMode='contain' style={[ styles.imageThumbnail, imageStyle ]} source={require('../../../assets/images/plus.png')} />  
                    </ModalSelector>
            }
        </View>
    )

}

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    imageThumbnail: { width: 160, height: 160 }
})
