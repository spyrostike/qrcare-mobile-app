import React from 'react'
import { Platform, View } from 'react-native'
import { Icon } from 'native-base'

export default (props) => {
    const { onPress } = props

    return (
        <View style={[ { flexDirection: 'row' }, Platform.OS === 'ios' && { zIndex: 99 } ]}>
            <Icon 
                type='Ionicons' 
                name='md-close' 
                style={{ fontSize: 40, color: '#FFFFFF', top: 20, right: 10, position: 'absolute', zIndex: 999 }}
                onPress={onPress} />
        </View>
    )
}
