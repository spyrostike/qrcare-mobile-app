import { useStoreActions } from 'easy-peasy'

export default class {
    addDrugAllergyAction = useStoreActions(actions => actions.drugAllergy.addDrugAllergy)
    editDrugAllergyAction = useStoreActions(actions => actions.drugAllergy.editDrugAllergy)
    getDrugAllergyByIdAction = useStoreActions(actions => actions.drugAllergy.getDrugAllergyById)
    addFileAction = useStoreActions(actions => actions.file.addFile)

    addDrugAllergy = (value) => this.addDrugAllergyAction(value)

    editDrugAllergy = payload => this.editDrugAllergyAction(payload)

    getDrugAllergyById = (payload) => this.getDrugAllergyByIdAction(payload)

    addFile = payload => this.addFileAction(payload)
}