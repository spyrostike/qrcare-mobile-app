import { genenrateFileObjToServer } from '../../utils/ObjectUtil'

export default class {
    
    constructor(store) {
        this.store = store
    }

    addDrugAllergy = payload => {
        return new Promise((resolve, reject) => {
            this.store.addDrugAllergy(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editDrugAllergy = payload => {
        return new Promise((resolve, reject) => {
            this.store.editDrugAllergy(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getDrugAllergyById = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getDrugAllergyById(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    addFile = (payload) => {
        const formData = genenrateFileObjToServer(payload)
        return new Promise((resolve, reject) => {
            this.store.addFile(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
