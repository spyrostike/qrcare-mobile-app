import React from 'react'
import DrugAllergyInfoController from './DrugAllergyInfoController'
import DrugAllergyInfoViewModel from './DrugAllergyInfoViewModel'
import DrugAllergyInfoAction from './DrugAllergyInfoAction'

export default () => {
    const action = new DrugAllergyInfoAction()
    const viewModel = new DrugAllergyInfoViewModel(action)

    return (
        <DrugAllergyInfoController viewModel={viewModel} />
    )
} 
