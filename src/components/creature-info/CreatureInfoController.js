import React, { useState } from 'react'
import moment from 'moment'
import CreatureInfoView from './CreatureInfoView'
import Navigator from '../../services/Navigator'

export default (props) => {
    // const [ id, setId ] = useState(null)
    // const [ creatureImage, setCreatureImage ] = useState(null)
    // const [ creatureImageId, setCreatureImageId ] = useState(null)
    // const [ firstName, setFirstName ] = useState('Doug')
    // const [ lastName, setLastName ] = useState('yengkee')
    // const [ birthCertificate, setBirthCertificate ] = useState(null)
    // const [ birthCertificateId, setBirthCertificateId ] = useState(null)
    // const [ gender, setGender ] = useState(1)
    // const [ bloodType, setBloodType ] = useState(1)
    // const [ birthDate, setBirthDate ] = useState(moment().format('DD/MM/YYYY'))
    // const [ weight, setWeight ] = useState('12')
    // const [ height, setHeight ] = useState('14.5')
    // const [ isLoading, setIsLoading ] = useState(false)
    // const [ errorMessage, setErrorMessage ] = useState(null)

    const [ id, setId ] = useState(null)
    const [ creatureImage, setCreatureImage ] = useState(null)
    const [ creatureImageId, setCreatureImageId ] = useState(null)
    const [ firstName, setFirstName ] = useState(null)
    const [ lastName, setLastName ] = useState(null)
    const [ birthCertificate, setBirthCertificate ] = useState(null)
    const [ birthCertificateId, setBirthCertificateId ] = useState(null)
    const [ gender, setGender ] = useState(1)
    const [ bloodType, setBloodType ] = useState(1)
    const [ birthDate, setBirthDate ] = useState(moment().format('DD/MM/YYYY'))
    const [ weight, setWeight ] = useState(null)
    const [ height, setHeight ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { viewModel } = props

    const _onSubmit = () => {
        if (!_validate()) return

        const data = { 
            id: id,
            creatureImageId: creatureImageId,
            firstName: firstName, 
            lastName: lastName,
            birthCertificateId: birthCertificateId,
            gender: gender, 
            bloodType: bloodType, 
            birthDate: moment(birthDate, 'DD/MM/YYYY').format(), 
            weight: weight,
            height: height
        }

        if (id === null) {
            _addCreature(data)
        }
    }
    
    const _addCreature = async (data) => {
        setIsLoading(true)

        try {
            const result = await viewModel.addCreature(data)
            setIsLoading(false)
            // _prepareData(result)

            const { routes, index } = Navigator.getCurrentRoute()
            const { onComplete } = routes[index].params
            // console.log('routes', routes)
            // Navigator.pop(1, true)
            // Navigator.navigate('MemberCreaturePreview', { creatureId: result.id })
            onComplete(result.id)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    } 

    const _prepareData = (data) => {
        setId(data.id)
        setCreatureImage(data.creatureImage)
        setFirstName(data.firstName)
        setLastName(data.lastName)
        setBirthCertificate(data.birthCertificate)
        setGender(data.gender)
        setBloodType(data.bloodType)
        setBirthDate(moment(data.birthDate, 'YYYY-MM-DD').format('DD/MM/YYYY'))
        setWeight(data.weight)
        setHeight(data.height)
    }

    const _validate = () => {
        if (creatureImageId === null) { setErrorMessage('Image is required'); return false; }
        if (firstName === null || firstName.trim().length === 0) { setErrorMessage('Firstname is required'); return false; }
        if (lastName === null || lastName.trim().length === 0) { setErrorMessage('Lastname is required'); return false; }
        if (birthCertificateId === null) { setErrorMessage('Birth certificate is required'); return false; }
        if (gender === null) { setErrorMessage('Gender certificate is required'); return false; }
        if (bloodType === null) { setErrorMessage('Blood type is required'); return false; }
        if (birthDate === null || birthDate.trim().length === 0) { setErrorMessage('Birthdate is required'); return false; }
        if (weight === null || weight.trim().length === 0) { setErrorMessage('Weight is required'); return false; }
        if (!/^\d+(\.\d{1,2})?$/.test(weight)) { setErrorMessage('Username should be between 0-9 and (.) only. '); return false; }
        if (height === null || height.trim().length === 0) { setErrorMessage('Height is required'); return false; }

        return true
    }

    const _setCreatureImage = async (image) => {
        try {
            const file = await _setImage(image, 'creature-profile.js', 'creature-profile-image')
            setCreatureImageId(file.id)
            setCreatureImage(file.src)
        } catch (error) {

        }
    }
    
    const _setBirthCertificate = async (image) => {
        try {
            const file = await _setImage(image, 'creature-birth-certificate.js', 'creature-birth-certificate-image')
            setBirthCertificateId(file.id)
            setBirthCertificate(file.src)
        } catch (error) {

        }
    }

    const _setImage = async (image, name, tag) => {
        try {
            setIsLoading(true)

            const data = {
                name: name,
                tag: tag,
                type: image.mime,
                uri: image.path
 
            }
            console.log('image', image)
            const file = await viewModel.addFile(data)
            setIsLoading(false)
            return file
        } catch (error) {
            console.log('error', error)
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    return (
        <CreatureInfoView
            creatureImage={creatureImage}
            firstName={firstName}
            lastName={lastName}
            birthCertificate={birthCertificate}
            gender={gender}
            bloodType={bloodType}
            birthDate={birthDate}
            weight={weight}
            height={height}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setCreatureImage={_setCreatureImage}
            setFirstName={setFirstName}
            setLastName={setLastName}
            setBirthCertificate={_setBirthCertificate}
            setGender={setGender}
            setBloodType={setBloodType}
            setBirthDate={setBirthDate}
            setWeight={setWeight}
            setHeight={setHeight}
            setErrorMessage={setErrorMessage}
            onSubmit={_onSubmit} />
    )
} 
