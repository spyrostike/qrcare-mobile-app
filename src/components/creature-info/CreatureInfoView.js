import React from 'react'
import InfoForm from '../UI/creature/InfoForm'

export default (props) => {
    const { 
        creatureImage,
        firstName,
        lastName,
        birthCertificate,
        gender,
        bloodType,
        birthDate,
        weight,
        height,
        errorMessage,
        isLoading,
        setCreatureImage,
        setFirstName,
        setLastName,
        setBirthCertificate,
        setGender,
        setBloodType,
        setBirthDate,
        setWeight,
        setHeight,
        setErrorMessage,
        onSubmit } = props

    return (
        <InfoForm
            creatureImage={creatureImage}
            firstName={firstName}
            lastName={lastName}
            birthCertificate={birthCertificate}
            gender={gender}
            bloodType={bloodType}
            birthDate={birthDate}
            weight={weight}
            height={height}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setCreatureImage={setCreatureImage}
            setFirstName={setFirstName}
            setLastName={setLastName}
            setBirthCertificate={setBirthCertificate}
            setGender={setGender}
            setBloodType={setBloodType}
            setBirthDate={setBirthDate}
            setWeight={setWeight}
            setHeight={setHeight}
            setErrorMessage={setErrorMessage}
            onSubmit={onSubmit} />
    )
} 