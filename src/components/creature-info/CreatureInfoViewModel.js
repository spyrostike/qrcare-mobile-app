import { genenrateCreatureObjToServer, genenrateFileObjToServer } from '../../utils/ObjectUtil'

export default class {
    
    constructor(store) {
        this.store = store
    }
    
    addCreature = (payload) => {
        const qrcodeItem = this.store.getQRCodeItem()

        const formData = genenrateCreatureObjToServer(qrcodeItem, payload)

        return new Promise((resolve, reject) => {
            this.store.addCreature(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    addFile = (payload) => {
        const formData = genenrateFileObjToServer(payload)
        return new Promise((resolve, reject) => {
            this.store.addFile(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
