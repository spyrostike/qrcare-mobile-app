import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    qrcodeItem = useStoreState(state => state.qrcode.item)
    creature = useStoreState(state => state.creature.item)
    addCreatureAction = useStoreActions(actions => actions.creature.addCreature)
    addFileAction = useStoreActions(actions => actions.file.addFile)
    
    getQRCodeItem = () => {
        return this.qrcodeItem
    }

    getCreature = () => {
        return this.creature
    }

    addCreature = (payload) => {
        return this.addCreatureAction(payload)
    }

    addFile = payload => this.addFileAction(payload)
}