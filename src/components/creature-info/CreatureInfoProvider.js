import React from 'react'
import CreatureInfoController from './CreatureInfoController'
import CreatureInfoViewModel from './CreatureInfoViewModel'
import CreatureInfoAction from './CreatureInfoAction'

export default () => {
    const action = new CreatureInfoAction()
    const viewModel = new CreatureInfoViewModel(action)

    return (
        <CreatureInfoController viewModel={viewModel} />
    )
} 
