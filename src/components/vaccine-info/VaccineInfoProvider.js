import React from 'react'
import VaccineInfoController from './VaccineInfoController'
import VaccineInfoViewModel from './VaccineInfoViewModel'
import VaccineInfoAction from './VaccineInfoAction'

export default () => {
    const action = new VaccineInfoAction()
    const viewModel = new VaccineInfoViewModel(action)

    return (
        <VaccineInfoController viewModel={viewModel} />
    )
} 
