import React from 'react'
import VaccineInfoForm from '../UI/vaccine/VaccineInfoForm'
import VaccineInfoFormViewOnly from '../UI/vaccine/VaccineInfoFormViewOnly'

export default (props) => {
    const { 
        viewOnly,
        methodType,
        image, 
        hospitalName, 
        date, 
        description, 
        isLoading,
        isEdit,
        dialogMessage,
        errorMessage,
        setImage, 
        setHospitalName, 
        setDate, 
        setErrorMessage, 
        setIsEdit,
        setDescription, 
        onSubmit, 
        onCancel,
        onConfirm } = props

    return (
        <React.Fragment>
            {
                viewOnly ? 
                    <VaccineInfoFormViewOnly 
                        image={image}
                        hospitalName={hospitalName}
                        date={date} 
                        description={description}
                        isLoading={isLoading}
                        dialogMessage={dialogMessage}
                        errorMessage={errorMessage}
                        setErrorMessage={setErrorMessage}
                        onConfirm={onConfirm}
                        onCancel={onCancel} />
                    :
                    <VaccineInfoForm 
                        methodType={methodType}
                        image={image}
                        hospitalName={hospitalName}
                        date={date} 
                        description={description}
                        isLoading={isLoading}
                        dialogMessage={dialogMessage}
                        errorMessage={errorMessage}
                        isEdit={isEdit}
                        setImage={setImage}
                        setHospitalName={setHospitalName}
                        setDate={setDate}
                        setDescription={setDescription}
                        setErrorMessage={setErrorMessage}
                        setIsEdit={setIsEdit}
                        onSubmit={onSubmit}
                        onConfirm={onConfirm} />
            }
        </React.Fragment>
    )
} 