import React, { useMemo, useState } from 'react'
import moment from 'moment'
import VaccineInfoView from './VaccineInfoView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ id, setId ] = useState(null)
    const [ image, setImage ] = useState(null)
    const [ imageId, setImageId ] = useState(null)
    const [ oldImageId, setOldImageId ] = useState(null)
    const [ isImageChange, setIsImageChange ] = useState(false)
    const [ hospitalName, setHospitalName ] = useState(null)
    const [ date, setDate ] = useState(moment().format('DD/MM/YYYY'))
    const [ description, setDescription ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ dialogMessage, setDialogMessage ] = useState(null)
    const [ isEdit, setIsEdit ] = useState(null)

    const { routes, index } = Navigator.getCurrentRoute()
    const { methodType, creatureId, vaccineBookId, vaccineId, onComplete, viewOnly } = routes[index].params

    const { viewModel } = props

    const _getVaccineById = async () => {
        try {
            if (methodType === 'add')  { setIsEdit(true); return; }
            if (typeof vaccineId === 'undefined' || vaccineId === null) { setDialogMessage('ID not found'); return; } 
            setIsLoading(true)
            const result = await viewModel.getVaccineById(vaccineId)
            _prepareData(result)
        } catch (error) {
            setDialogMessage(error)
        }
        setIsLoading(false)
    }

    useMemo(() => {
        _getVaccineById()
    }, [])

    const _prepareData = (data) => {  
        const { id, image, imageId, hospitalName, date, description } = data

        setId(id)
        setImage(image)
        setImageId(imageId)
        setOldImageId(imageId)
        setHospitalName(hospitalName)
        setDate(moment(date, 'YYYY/MM/DD').format('DD/MM/YYYY'))
        setDescription(description)
    }

    const _onSubmit = () => {
        if (!_validate()) return

        let data = {
            id: id,
            imageId: imageId,
            oldImageId: isImageChange ? oldImageId : null,
            hospitalName: hospitalName,
            description: description,
            date: moment(date, 'DD/MM/YYYY').format(),
            vaccineBookId: vaccineBookId, 
            creatureId: creatureId
        }

        if (id === null) _addVaccine(data)
        else _editVaccine(data)
    }

    const _addVaccine = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.addVaccine(data)
            onComplete()
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    } 

    const _editVaccine = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.editVaccine(data)
            onComplete()
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    } 

    const _onRefresh = () => {
        setDialogMessage(null)
        setTimeout(() => {
            _getVaccineById()
        }, 300)
        
    }

    const _onCancel = () => {
        setDialogMessage(null)
        Navigator.back()
    }

    const _validate = () => {
        if (imageId === null)  { setErrorMessage('Image is required'); return false; }
        if (hospitalName === null || hospitalName.trim().length === 0) { setErrorMessage('Hospital name is required'); return false; }
        if (date === null) { setErrorMessage('Date is required'); return false; }

        return true
    }

    const _setImage = async (image) => {
        setIsLoading(true)

        try {
            const data = {
                name: 'vaccine.js',
                tag: 'vaccine-image',
                type: image.mime,
                uri: image.path
            }
            
            const file = await viewModel.addFile(data)
            
            setImageId(file.id)
            setImage(file.src)
            setIsImageChange(true)

            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    return (
        <VaccineInfoView 
            viewOnly={viewOnly}
            methodType={methodType}
            image={image}
            hospitalName={hospitalName}
            date={date}
            description={description}
            isLoading={isLoading}
            isEdit={isEdit}
            errorMessage={errorMessage}
            dialogMessage={dialogMessage}
            setImage={_setImage}
            setHospitalName={setHospitalName}
            setDate={setDate}
            setDescription={setDescription}
            setErrorMessage={setErrorMessage}
            setIsEdit={setIsEdit}
            onSubmit={_onSubmit}
            onConfirm={_onRefresh}
            onCancel={_onCancel} />
    )
}