import { useStoreActions } from 'easy-peasy'

export default class {
    addVaccineAction = useStoreActions(actions => actions.vaccine.addVaccine)
    editVaccineAction = useStoreActions(actions => actions.vaccine.editVaccine)
    getVaccineByIdAction = useStoreActions(actions => actions.vaccine.getVaccineById)
    addFileAction = useStoreActions(actions => actions.file.addFile)

    addVaccine = payload => this.addVaccineAction(payload)

    editVaccine = payload => this.editVaccineAction(payload)

    getVaccineById = payload => this.getVaccineByIdAction(payload)

    addFile = payload => this.addFileAction(payload)
}