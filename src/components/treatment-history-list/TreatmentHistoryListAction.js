import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    treatmentHistoryList = useStoreState(state => state.treatmentHistory.list)
    getTreatmentHistoryListAction = useStoreActions(actions => actions.vaccine.setList)
    getTreatmentHistoryListByCreatureIdAction = useStoreActions(actions => actions.treatmentHistory.getTreatmentHistoryListByCreatureId)
    removeTreatmentHistoryAction = useStoreActions(actions => actions.treatmentHistory.removeTreatmentHistory)
    removeTreatmentHistoryArrayAction = useStoreActions(actions => actions.treatmentHistory.removeTreatmentHistoryArray)

    getTreatmentHistoryList= () => this.treatmentHistoryList

    setTreatmentHistoryList = payload => this.getTreatmentHistoryListAction(payload)

    getTreatmentHistoryListByCreatureId = payload => this.getTreatmentHistoryListByCreatureIdAction(payload)

    removeTreatmentHistory = payload => this.removeTreatmentHistoryAction(payload)

    removeTreatmentHistoryArray = payload => this.removeTreatmentHistoryArrayAction(payload)
}