import React from 'react'
import TreatmentHistoryList from '../UI/treatment-history/TreatmentHistoryList'

export default (props) => {
    const { 
        viewOnly, 
        mode,
        list, 
        checkList,
        forceUpdate,
        onItemPress, 
        isLoading, 
        errorMessage, 
        setErrorMessage,
        setMode,
        removeItem, 
        onSubmit,
        onCheckBoxPress,
        initialCheckList } = props
    
    return (
        <React.Fragment >
            <TreatmentHistoryList 
                viewOnly={viewOnly}
                mode={mode}
                items={list}
                checkItems={checkList} 
                forceUpdate={forceUpdate}
                onItemPress={onItemPress} 
                isLoading={isLoading}
                errorMessage={errorMessage}
                setErrorMessage={setErrorMessage}
                setMode={setMode}
                removeItem={removeItem}
                onCheckBoxPress={onCheckBoxPress}
                onSubmit={onSubmit}
                onClearCheckList={initialCheckList} />
        </React.Fragment>
    )
} 