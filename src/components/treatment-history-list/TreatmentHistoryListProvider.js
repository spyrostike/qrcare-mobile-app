import React from 'react'
import TreatmentHistoryListController from './TreatmentHistoryListController'
import TreatmentHistoryListViewModel from './TreatmentHistoryListViewModel'
import TreatmentHistoryListAction from './TreatmentHistoryListAction'

export default () => {
    const action = new TreatmentHistoryListAction()
    const viewModel = new TreatmentHistoryListViewModel(action)

    return (
        <TreatmentHistoryListController viewModel={viewModel} />
    )
} 
