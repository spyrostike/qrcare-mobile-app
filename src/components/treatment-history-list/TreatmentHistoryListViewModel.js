export default class {
    
    constructor(store) {
        this.store = store
    }

    getTreatmentHistoryList = () => this.store.getTreatmentHistoryList()

    setTreatmentHistoryList = payload => this.store.setTreatmentHistoryList(payload)

    getTreatmentHistoryListByCreatureId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getTreatmentHistoryListByCreatureId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeTreatmentHistory = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeTreatmentHistory(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeTreatmentHistoryArray = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.removeTreatmentHistoryArray(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
