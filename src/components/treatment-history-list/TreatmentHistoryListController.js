import React, { useMemo, useState } from 'react'
import TreatmentHistoryListView from './TreatmentHistoryListView'
import Navigator from '../../services/Navigator'
import dayjs from 'dayjs'

export default (props) => {
    const [ mode, setMode ] = useState(null)
    const [ checkList, setCheckList ] = useState([])
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ forceUpdate, setForceUpdate ] = useState(null)

    const { viewModel } = props

    const { routes, index } = Navigator.getCurrentRoute()
    const { creatureId, viewOnly } = routes[index].params

    const prefixScreen = viewOnly ? 'ViewOnly' : 'Member'

    const _getTreatmentHistoryListByCreatureId = async () => {
        try {
            setIsLoading(true)
            const items = await viewModel.getTreatmentHistoryListByCreatureId(creatureId)
            _initialCheckList(items)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    useMemo(() => {
        viewModel.setTreatmentHistoryList([])
        _getTreatmentHistoryListByCreatureId()
    }, [])
 
    const _onItemPress = (value) => {
        if (mode) return 
        if (value) {
            Navigator.navigate(prefixScreen + 'TreatmentHistoryInfo', { 
                methodType: 'edit', 
                treatmentHistoryId: value.id,
                onComplete: async () => {
                    Navigator.back()
                    await _getTreatmentHistoryListByCreatureId()
                }
            })
        } else {
            Navigator.navigate(prefixScreen + 'TreatmentHistoryInfo', { 
                methodType: 'add',
                creatureId: creatureId,
                onComplete: async () => {
                    Navigator.back()
                    await _getTreatmentHistoryListByCreatureId()
                }
            })
        }
    }

    const _removeItem = async (value) => {
        setIsLoading(true)

        try {
            await viewModel.removeTreatmentHistory(value.id)
            await _getTreatmentHistoryListByCreatureId()
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _removeItems = async (items) => {
        setIsLoading(true)

        try {
            await viewModel.removeTreatmentHistoryArray({ items: items })
            await _getTreatmentHistoryListByCreatureId()
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _onSubmit = async () => {
        _initialCheckList(checkList)
        setMode(null)

        const items = checkList.filter((item) => item !== null)

        if (items.length === 0) return
        if (mode === 'delete') _removeItems(items)
        else if (mode === 'send-email') Navigator.navigate('MemberTreatmentHistoryEmail', { items: items, onComplete: () => { Navigator.back() } })

    }

    const _convertListForDisplay = () => {
        
        return viewModel.getTreatmentHistoryList().map(item => ({ 
            ...item,
            text1: item.department_name,
            text2: item.file_category === 5 ? item.file_category_other : item.file_category_name,
            text3: dayjs(item.date, 'YYYY/MM/DD').format('DD/MM/YYYY')
        }))
    }

    const _initialCheckList = (items) => {
        let list = []
        const checkListVM = items ? items : checkList
        for (var i = 0;i < checkListVM.length; i++) {
            list.push(null)
        }
        setCheckList(list)
    } 

    const _onCheckBoxPress = (index, item) => {
        const listVM = checkList
        listVM[index] ? listVM[index] = null : listVM[index] = item.id
        setCheckList([])
        setCheckList(listVM)
        setForceUpdate(Math.random())
    }

    return (
        <TreatmentHistoryListView
            viewOnly={viewOnly}
            mode={mode}
            list={_convertListForDisplay()}
            forceUpdate={forceUpdate}
            checkList={checkList}
            onItemPress={_onItemPress}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setErrorMessage={setErrorMessage}
            setMode={setMode}
            onCheckBoxPress={_onCheckBoxPress}
            removeItem={_removeItem}
            onSubmit={_onSubmit}
            initialCheckList={_initialCheckList} />
    )
} 
