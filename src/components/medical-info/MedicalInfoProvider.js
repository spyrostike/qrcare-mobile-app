import React from 'react'
import MedicalInfoController from './MedicalInfoController'
import MedicalInfoViewModel from './MedicalInfoViewModel'
import MedicalInfoAction from './MedicalInfoAction'

export default () => {
    const action = new MedicalInfoAction()
    const viewModel = new MedicalInfoViewModel(action)

    return (
        <MedicalInfoController viewModel={viewModel} />
    )
} 
