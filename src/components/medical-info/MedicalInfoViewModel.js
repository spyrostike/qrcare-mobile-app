export default class {
    
    constructor(store) {
        this.store = store
    }

    addMedical = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.addMedical(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editMedical = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.editMedical(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getMedicalByCreatureId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getMedicalByCreatureId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getDrugRecordNumber = () => {
        return this.store.getDrugRecordNumber()
    }

    getDrugAllergyRecordNumber = () => {
        return this.store.getDrugAllergyRecordNumber()
    }

    getFoodAllergyRecordNumber = () => {
        return this.store.getFoodAllergyRecordNumber()
    }

}
