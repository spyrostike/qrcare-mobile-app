import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    addMedicalAction = useStoreActions(actions => actions.medical.addMedical)
    editMedicalAction = useStoreActions(actions => actions.medical.editMedical)
    getMedicalByCreatureIdAction = useStoreActions(actions => actions.medical.getMedicalByCreatureId)
    drugRecordNumber = useStoreState(state => state.drug.recordNumber)
    drugAllergyRecordNumber = useStoreState(state => state.drugAllergy.recordNumber)
    foodAllergyRecordNumber = useStoreState(state => state.foodAllergy.recordNumber)

    addMedical = (payload) => {
        return this.addMedicalAction(payload)
    }

    editMedical = (payload) => {
        return this.editMedicalAction(payload)
    }

    getMedicalByCreatureId = (payload) => {
        return this.getMedicalByCreatureIdAction(payload)
    }

    getDrugRecordNumber = () => {
        return this.drugRecordNumber
    }

    getDrugAllergyRecordNumber = () => {
        return this.drugAllergyRecordNumber
    }

    getFoodAllergyRecordNumber = () => {
        return this.foodAllergyRecordNumber
    }

    
}