import React, { useMemo, useState } from 'react'
import MedicalInfoView from './MedicalInfoView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ id, setId ] = useState(null)
    const [ hospitalName, setHospitalName ] = useState(null)
    const [ hospitalTel, setHospitalTel ] = useState(null)
    const [ patientId, setPatientId ] = useState(null)
    const [ bloodType, setBloodType ] = useState(3)
    const [ bloodTypeName, setBloodTypeName ] = useState(null)
    const [ doctorName, setDoctorName ] = useState(null)
    const [ doctorContactNo, setDoctorContactNo ] = useState(null)
    const [ congenitalDisorder, setCongenitalDisorder ] = useState(null)
    const [ birthCertificate, setBirthCertificate ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ dialogMessage, setDialogMessage ] = useState(null)
    const [ isEdit, setIsEdit ] = useState(null)

    const { routes, index } = Navigator.getCurrentRoute()
    const { creatureId, bloodTypePR, onComplete, viewOnly } = routes[index].params

    const { viewModel } = props

    const _getMedicalByCreatureId = async () => {
        try {
            setIsLoading(true)
            
            const result = await viewModel.getMedicalByCreatureId(creatureId)
            _prepareData(result)
        } catch (error) {
            setDialogMessage(error)
        }
        setIsLoading(false)
    }

    useMemo(() => {
        _getMedicalByCreatureId()
    }, [])

    const _prepareData = (data) => {  
        const { id, 
            hospitalName, 
            hospitalTel, 
            patientId, 
            bloodType, 
            bloodTypeName, 
            doctorName, 
            doctorContactNo, 
            congenitalDisorder,
            birthCertificate } = data

        if (!id) setIsEdit(true)

        setId(id)
        setHospitalName(hospitalName)
        setHospitalTel(hospitalTel)
        setPatientId(patientId)
        setBloodType(!id ? bloodTypePR : bloodType)
        setBloodTypeName(bloodTypeName)
        setDoctorName(doctorName)
        setDoctorContactNo(doctorContactNo)
        setCongenitalDisorder(congenitalDisorder)
        setBirthCertificate(birthCertificate)
    }

    const _onSubmit = () => {
        if (!_validate()) return

        const data = { 
            id: id,
            hospitalName: hospitalName,
            hospitalTel: hospitalTel, 
            patientId: patientId, 
            bloodType: bloodType, 
            doctorName: doctorName, 
            doctorContactNo: doctorContactNo,
            congenitalDisorder: congenitalDisorder,
            creatureId: creatureId
        }
 
        if (id === null) _addMedical(data)
        else _editMedical(data)
        
    }
    
    const _addMedical = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.addMedical(data)
            onComplete(bloodType)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    } 

    const _editMedical = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.editMedical(data)
            onComplete(bloodType)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    } 

    const _validate = () => {
        if (hospitalName === null || hospitalName.trim().length === 0) { setErrorMessage('Hospital name is required'); return false; }
        if (hospitalTel === null || hospitalTel.trim().length === 0) { setErrorMessage('Hospital tel is required'); return false; }
        if (patientId === null || patientId.trim().length === 0) { setErrorMessage('Patient ID is required'); return false; }
        if (bloodType === null)  { setErrorMessage('Blood type is required'); return false; }
        if (doctorName === null || doctorName.trim().length === 0)  { setErrorMessage('Doctor name is required'); return false; }
        // if (congenitalDisorder === null || congenitalDisorder.trim().length === 0)  { setErrorMessage('Congenital disorder is required'); return false; }

        return true
    }

    _goToScreen = (screen) => {
        Navigator.navigate(screen, { creatureId: creatureId })
    }

    const _onCancel = () => {
        setDialogMessage(null)
        Navigator.back()
    }

    const _onRefresh = () => {
        setDialogMessage(null)
        setTimeout(() => {
            _getMedicalByCreatureId()
        }, 300)
    }
    
    return (
        <MedicalInfoView
            viewOnly={viewOnly}
            id={id}
            hospitalName={hospitalName}
            hospitalTel={hospitalTel}
            patientId={patientId}
            bloodType={bloodType}
            doctorName={doctorName}
            doctorContactNo={doctorContactNo}
            congenitalDisorder={congenitalDisorder}
            birthCertificate={birthCertificate}
            drug={viewModel.getDrugRecordNumber() === null || viewModel.getDrugRecordNumber() <= 0 ? false : true}
            drugAllergy={viewModel.getDrugAllergyRecordNumber() === null || viewModel.getDrugAllergyRecordNumber() <= 0 ? false : true}
            foodAllergy={viewModel.getFoodAllergyRecordNumber() === null || viewModel.getFoodAllergyRecordNumber() <= 0 ? false : true}
            drugViewOnly={viewModel.getDrugRecordNumber() === null || viewModel.getDrugRecordNumber() <= 0  ? 'None' : viewModel.getDrugRecordNumber() + ' items'}
            drugAllergyViewOnly={viewModel.getDrugAllergyRecordNumber() === null || viewModel.getDrugAllergyRecordNumber() <= 0 ? 'None' : viewModel.getDrugAllergyRecordNumber() + ' items'}
            foodAllergyViewOnly={viewModel.getFoodAllergyRecordNumber() === null || viewModel.getFoodAllergyRecordNumber()  <= 0 ? 'None' : viewModel.getFoodAllergyRecordNumber() + ' items'}
            isLoading={isLoading}
            isEdit={isEdit}
            errorMessage={errorMessage}
            dialogMessage={dialogMessage}
            setHospitalName={setHospitalName}
            bloodTypeName={bloodTypeName}
            setHospitalTel={setHospitalTel}
            setPatientId={setPatientId}
            setBloodType={setBloodType}
            setDoctorName={setDoctorName}
            setDoctorContactNo={setDoctorContactNo}
            setCongenitalDisorder={setCongenitalDisorder}
            setErrorMessage={setErrorMessage}
            setIsEdit={setIsEdit}
            goToScreen={_goToScreen}
            onConfirm={_onRefresh}
            onSubmit={_onSubmit} />
    )
} 
