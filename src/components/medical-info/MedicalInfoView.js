import React from 'react'
import MedicalInfoForm from '../UI/medical/MedicalInfoForm'
import MedicalInfoFormViewOnly from '../UI/medical/MedicalInfoFormViewOnly'

export default (props) => {
    const { 
        viewOnly,
        id,
        hospitalName,
        hospitalTel,
        patientId,
        bloodType,
        bloodTypeName,
        doctorName,
        doctorContactNo,
        congenitalDisorder,
        birthCertificate,
        drug,
        drugAllergy,
        foodAllergy,
        drugViewOnly,
        drugAllergyViewOnly,
        foodAllergyViewOnly,
        isLoading,
        isEdit,
        dialogMessage,
        errorMessage,
        setHospitalName,
        setHospitalTel,
        setPatientId,
        setBloodType,
        setDoctorName,
        setDoctorContactNo,
        setCongenitalDisorder,
        setErrorMessage, 
        setIsEdit,
        goToScreen,
        onCancel,
        onSubmit,
        onConfirm } = props

    return (
        <React.Fragment >
            {
                viewOnly ? 
                    <MedicalInfoFormViewOnly
                        hospitalName={hospitalName}
                        hospitalTel={hospitalTel}
                        patientId={patientId}
                        bloodTypeName={bloodTypeName}
                        doctorName={doctorName}
                        doctorContactNo={doctorContactNo}
                        congenitalDisorder={congenitalDisorder}
                        birthCertificate={birthCertificate}
                        drug={drugViewOnly}
                        drugAllergy={drugAllergyViewOnly}
                        foodAllergy={foodAllergyViewOnly}
                        isLoading={isLoading}
                        dialogMessage={dialogMessage} 
                        errorMessage={errorMessage}
                        setErrorMessage={setErrorMessage}
                        goToScreen={goToScreen}
                        onCancel={onCancel}
                        onConfirm={onConfirm} />
                    :
                    <MedicalInfoForm
                        id={id}
                        hospitalName={hospitalName}
                        hospitalTel={hospitalTel}
                        patientId={patientId}
                        bloodType={bloodType}
                        bloodTypeName={bloodTypeName}
                        doctorName={doctorName}
                        doctorContactNo={doctorContactNo}
                        congenitalDisorder={congenitalDisorder}
                        birthCertificate={birthCertificate}
                        drug={drug}
                        drugAllergy={drugAllergy}
                        foodAllergy={foodAllergy}
                        drugViewOnly={drugViewOnly}
                        drugAllergyViewOnly={drugAllergyViewOnly}
                        foodAllergyViewOnly={foodAllergyViewOnly}
                        isLoading={isLoading}
                        isEdit={isEdit}
                        dialogMessage={dialogMessage} 
                        errorMessage={errorMessage}
                        setHospitalName={setHospitalName}
                        setHospitalTel={setHospitalTel}
                        setPatientId={setPatientId}
                        setBloodType={setBloodType}
                        setDoctorName={setDoctorName}
                        setDoctorContactNo={setDoctorContactNo}
                        setCongenitalDisorder={setCongenitalDisorder}
                        setErrorMessage={setErrorMessage}
                        setIsEdit={setIsEdit}
                        goToScreen={goToScreen}
                        onCancel={onCancel}
                        onSubmit={onSubmit}
                        onConfirm={onConfirm} />

            }
        </React.Fragment>
    )
} 