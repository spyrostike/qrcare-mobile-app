export default class {
    
    constructor(store) {
        this.store = store
    }

    changePassword = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.changePassword(payload).then(async v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
