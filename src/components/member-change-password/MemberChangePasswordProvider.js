import React from 'react'
import MemberChangePasswordController from './MemberChangePasswordController'
import MemberChangePasswordViewModel from './MemberChangePasswordViewModel'
import MemberChangePasswordAction from './MemberChangePasswordAction'

export default () => {
    const action = new MemberChangePasswordAction()
    const viewModel = new MemberChangePasswordViewModel(action)

    return (
        <MemberChangePasswordController viewModel={viewModel} />
    )
} 
