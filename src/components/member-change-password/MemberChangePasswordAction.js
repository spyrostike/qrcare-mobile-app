
import { useStoreActions } from 'easy-peasy'

export default class {
    changePasswordAction = useStoreActions(actions => actions.member.changePassword)

    changePassword = paylod => this.changePasswordAction(paylod)

}