import React from 'react'
import ChangePasswordForm from '../UI/member/ChangePasswordForm'

export default (props) => {
    const { 
        currentPassword, 
        newPassword,
        repeatPassword, 
        isLoading, 
        errorMessage, 
        setCurrentPassword,
        setNewPassword,
        setRepeatPassword,
        setErrorMessage,
        onSubmit,
        onBack } = props
    
    return (
        <ChangePasswordForm 
            currentPassword={currentPassword}
            newPassword={newPassword}
            repeatPassword={repeatPassword}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setCurrentPassword={setCurrentPassword}
            setNewPassword={setNewPassword}
            setRepeatPassword={setRepeatPassword}
            setErrorMessage={setErrorMessage}
            onSubmit={onSubmit}
            onLogoPress={onBack} />
    )
} 