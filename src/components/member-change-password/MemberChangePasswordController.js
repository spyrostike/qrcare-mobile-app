import React, { useMemo, useState } from 'react'
import MemberChangePasswordView from './MemberChangePasswordView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ currentPassword, setCurrentPassword ] = useState(null)
    const [ newPassword, setNewPassword ] = useState(null)
    const [ repeatPassword, setRepeatPassword ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { viewModel } = props

    useMemo(() => { }, [])

    const _onSubmit = () => {
        if (!_validate()) return
        _changePassword()
    }

    const _changePassword = async () => {
        
        setIsLoading(true)
        
        const data = { 
            currentPassword: currentPassword,
            newPassword: newPassword
        }

        try {
            await viewModel.changePassword(data)
            setIsLoading(false)
            Navigator.navigate('MemberCreatureList')
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }


    const _validate = () => {
        if (currentPassword === null || currentPassword.trim().length === 0) { setErrorMessage('currentPassword is required.'); return false; }
        if (newPassword === null || newPassword.trim().length === 0) { setErrorMessage('New password is required.'); return false; }
        if (newPassword.trim().length < 6 || newPassword.trim().length > 30) { setErrorMessage('New password should be between 6-30.'); return false; }
        if (!/^[a-zA-Z0-9!@#$%\^&*+=._-]*$/.test(newPassword)) { setErrorMessage('New password should be between a-z 0-9, symbol(!@#$%^&*+=._-)'); return false; }
        // if (!new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})').test(newPassword)) { 
        //     setErrorMessage('New password must have at least one capital, symbol and number.')
        //     return false
        // }
        if (newPassword !== repeatPassword) {
            setErrorMessage('Password not match.')
            return false
        }


        return true
    }

    const _onBack = () => {
        Navigator.back()
    }

    return (
        <MemberChangePasswordView
            currentPassword={currentPassword}
            newPassword={newPassword}
            repeatPassword={repeatPassword}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setCurrentPassword={setCurrentPassword}
            setNewPassword={setNewPassword}
            setRepeatPassword={setRepeatPassword}
            setErrorMessage={setErrorMessage}
            onSubmit={_onSubmit}
            onBack={_onBack} />
    )
} 
