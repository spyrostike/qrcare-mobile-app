import { useStoreActions } from 'easy-peasy'

export default class {
    setProfileAction = useStoreActions(actions => actions.member.setProfile)
    getMemberByIdAction = useStoreActions(actions => actions.member.getMemberById)
    getMemberProfileAction = useStoreActions(actions => actions.member.getMemberProfile)
    editMemberProfileAction = useStoreActions(actions => actions.member.editMemberProfile)
    addFileAction = useStoreActions(actions => actions.file.addFile)
    checkEmailExistAction = useStoreActions(actions => actions.member.checkEmailExist)

    setProfile = payload => {
        this.setProfileAction(payload)
    }

    getMemberById = payload => this.getMemberByIdAction(payload)

    getMemberProfile = () => this.getMemberProfileAction()

    editMemberProfile = payload => this.editMemberProfileAction(payload)

    addFile = payload => this.addFileAction(payload)

    checkEmailExist = paylod => {
        return this.checkEmailExistAction(paylod)
    }
}