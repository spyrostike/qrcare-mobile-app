import React, { useMemo, useState } from 'react'
import MemberProfileView from './MemberProfileView'
import moment from 'moment'
import dayjs from 'dayjs'
import Navigator from '../../services/Navigator'

export default (props) => {
    // const [ firstName, setFirstName ] = useState('tanat')
    // const [ lastName, setLastName ] = useState('tang')
    // const [ gender, setGender ] = useState(1)
    // const [ genderName, setGenderName ] = useState(null)
    // const [ bloodType, setBloodType ] = useState(1)
    // const [ bloodTypeName, setBloodTypeName ] = useState(null)
    // const [ birthDate, setBirthDate ] = useState(moment().format('DD/MM/YYYY'))
    // const [ email, setEmail ] = useState('spyrostike@gmail.com')
    // const [ relationship, setRelationship ] = useState('father')
    // const [ identificationNo, setIdentificationNo ] = useState('xxxxxxxxxxxxx')
    // const [ contactNo1, setContactNo1 ] = useState('xxxxxxxxxx')
    // const [ contactNo2, setContactNo2 ] = useState(null)
    // const [ address1, setAddress1 ] = useState('xxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxxxx xxxxxxxxxxxxx xxxxxxxxxx')
    // const [ address2, setAddress2 ] = useState(null)
    // const [ address3, setAddress3 ] = useState(null)

    const [ id, setId ] = useState(null)
    const [ profileImageId, setProfileImageId ] = useState(null)
    const [ oldProfileImageId, setOldProfileImageId ] = useState(null)
    const [ isImageChange, setIsImageChange ] = useState(false)
    const [ profileImage, setProfileImage ] = useState(null)
    const [ firstName, setFirstName ] = useState(null)
    const [ lastName, setLastName ] = useState(null)
    const [ gender, setGender ] = useState(1)
    const [ genderName, setGenderName ] = useState(null)
    const [ bloodType, setBloodType ] = useState(1)
    const [ bloodTypeName, setBloodTypeName ] = useState(null)
    const [ birthDate, setBirthDate ] = useState(moment().format('DD/MM/YYYY'))
    const [ email, setEmail ] = useState(null)
    const [ relationship, setRelationship ] = useState(null)
    const [ identificationNo, setIdentificationNo ] = useState(null)
    const [ contactNo1, setContactNo1 ] = useState(null)
    const [ contactNo2, setContactNo2 ] = useState(null)
    const [ address1, setAddress1 ] = useState(null)
    const [ address2, setAddress2 ] = useState(null)
    const [ address3, setAddress3 ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const { viewModel } = props

    const { routes, index } = Navigator.getCurrentRoute()
    const { methodType, memberId, viewOnly } = routes[index].params

    const _getMemberById = async () => {
        try {
            
            if (!memberId) { setErrorMessage('ID not found'); return; } 
            setIsLoading(true)
            const result = await viewModel.getMemberById(memberId)
            
            _prepareData(result)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _getMemberProfile = async () => {
        try {
            setIsLoading(true)
            const result = await viewModel.getMemberProfile()
            _prepareData(result)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }   

    useMemo(() => {
        if (viewOnly) _getMemberById()
        if (methodType === 'edit') _getMemberProfile()
    }, [])

    const _prepareData = (data) => {
        setId(data.profile.id)
        setProfileImage(data.profile.profileImage)
        setProfileImageId(data.profile.profileImageId)
        setOldProfileImageId(data.profile.profileImageId)
        setFirstName(data.profile.firstName)
        setLastName(data.profile.lastName)
        setGender(data.profile.gender)
        setEmail(data.profile.email)
        setBloodType(data.profile.bloodType)
        setBirthDate(dayjs(data.profile.birthDate).format('DD/MM/YYYY'))
        setRelationship(data.profile.relationship)
        setGenderName(data.profile.genderName)
        setBloodTypeName(data.profile.bloodTypeName)
        setIdentificationNo(data.profile.identificationNo)
        setContactNo1(data.profile.contactNo1)
        setContactNo2(data.profile.contactNo2)
        setAddress1(data.profile.address1)
        setAddress2(data.profile.address2)
        setAddress3(data.profile.address3)
    }
    
    const _onSubmit = async () => {
        if (!_validate()) return
        
        const data = { 
            profileImageId: profileImageId,
            oldProfileImageId: isImageChange ? oldProfileImageId : null,
            firstName: firstName, 
            lastName: lastName, 
            gender: gender, 
            email: email,
            bloodType: bloodType, 
            birthDate: moment(birthDate, 'DD/MM/YYYY').format(), 
            relationship: relationship, 
            identificationNo: identificationNo, 
            contactNo1: contactNo1, 
            contactNo2: contactNo2, 
            address1: address1, 
            address2: address2, 
            address3: address3
        }

        if (methodType === 'register') {
            const isExist = await _checkEmailExist()
            if (isExist) return
            viewModel.setProfile(data)
            
            Navigator.navigate('VisitorMemberLocation', { methodType: 'register' })
        } 

        if (methodType === 'edit' && id) {
            await _editMemberProfile(data)
        }
    }

    const _checkEmailExist = async () => {
        setIsLoading(true)
        try {
            await viewModel.checkEmailExist({ email: email })
            setIsLoading(false)
            return false
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
            return true
        }
        
    }

    const _editMemberProfile = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.editMemberProfile(data)
            setIsLoading(false)
            Navigator.back()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    } 
    
    const _setProfileImage = async (image) => {
        setIsLoading(true)

        try {
            const data = {
                name: 'member-profile.js',
                tag: 'member-profile-image',
                type: image.mime,
                uri: image.path
 
            }
            
            const file = await viewModel.addFile(data)
            
            setProfileImageId(file.id)
            setProfileImage(file.src)
            setIsImageChange(true)

            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _onBack = () => {
        Navigator.back()
    }

    const _validate = () => {
        if (profileImageId === null) { setErrorMessage('ProfileImage is required'); return false }
        if (firstName === null || firstName.trim().length === 0) { setErrorMessage('First name is required'); return false }
        if (lastName === null || lastName.trim().length === 0) { setErrorMessage('Last name is required'); return false }
        if (gender === null) { setErrorMessage('Gender is required'); return false }
        if (email === null || email.trim().length === 0) { setErrorMessage('Email is required'); return false }
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) { setErrorMessage('Email is invalid'); return false }
        if (bloodType === null) { setErrorMessage('Blood type is required'); return false }
        if (birthDate === null || birthDate.trim('Birth date name is required').length === 0) { setErrorMessage(''); return false }
        if (relationship === null || relationship.trim().length === 0) { setErrorMessage('Reletionship is required'); return false }
        if (identificationNo === null || identificationNo.trim().length === 0) { setErrorMessage('ID no. is required'); return false }
        if (contactNo1 === null || contactNo1.trim().length === 0) { setErrorMessage('Contact no1 is required'); return false }
        // if (contactNo2 === null || contactNo2.trim().length === 0) return false
        if (address1 === null || address1.trim().length === 0) { setErrorMessage('Adress1 is required'); return false }
        // if (address2 === null || address2.trim().length === 0) return false
        // if (address3 === null || address3.trim().length === 0) return false

        return true
    }

    return (
        <MemberProfileView
            methodType={methodType}
            viewOnly={viewOnly}
            profileImage={profileImage}
            firstName={firstName}
            lastName={lastName} 
            gender={gender}
            genderName={genderName}
            email={email}
            bloodType={bloodType}
            bloodTypeName={bloodTypeName}
            birthDate={birthDate}
            relationship={relationship}
            identificationNo={identificationNo}
            contactNo1={contactNo1}
            contactNo2={contactNo2}
            address1={address1}
            address2={address2}
            address3={address3}
            isLoading={isLoading}
            errorMessage={errorMessage}
            setProfileImage={_setProfileImage}
            setFirstName={setFirstName}
            setLastName={setLastName}
            setGender={setGender}
            setEmail={setEmail}
            setBloodType={setBloodType}
            setBirthDate={setBirthDate}
            setRelationship={setRelationship}
            setIdentificationNo={setIdentificationNo}
            setContactNo1={setContactNo1}
            setContactNo2={setContactNo2}
            setAddress1={setAddress1}
            setAddress2={setAddress2}
            setAddress3={setAddress3}
            setErrorMessage={setErrorMessage}
            onBack={_onBack}
            onSubmit={_onSubmit} />
    )
} 
