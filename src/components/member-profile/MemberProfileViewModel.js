import { genenrateFileObjToServer } from '../../utils/ObjectUtil'

export default class {
    
    constructor(store) {
        this.store = store
    }

    setProfile = (value) => {
        this.store.setProfile(value)
    }

    getMemberById = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getMemberById(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getMemberProfile = () => {
        return new Promise((resolve, reject) => {
            this.store.getMemberProfile().then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editMemberProfile = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.editMemberProfile(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    addFile = (payload) => {
        const formData = genenrateFileObjToServer(payload)
        return new Promise((resolve, reject) => {
            this.store.addFile(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    checkEmailExist = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.checkEmailExist(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
