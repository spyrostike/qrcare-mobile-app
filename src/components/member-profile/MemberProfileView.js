import React from 'react'
import ProfileForm from '../UI/member/ProfileForm'
import ProfileFormViewOnly from '../UI/member/ProfileFormViewOnly'

export default (props) => {
    const { 
        methodType,
        viewOnly,
        profileImage,
        firstName,
        lastName,
        gender,
        genderName,
        email,
        bloodType,
        bloodTypeName,
        birthDate,
        relationship,
        identificationNo,
        contactNo1,
        contactNo2,
        address1,
        address2,
        address3,
        isLoading,
        setProfileImage,
        setFirstName,
        setLastName,
        setGender,
        setEmail,
        setBloodType,
        setBirthDate,
        setRelationship,
        setIdentificationNo,
        setContactNo1,
        setContactNo2,
        setAddress1,
        setAddress2,
        setAddress3,
        errorMessage,
        setErrorMessage,
        onBack,
        onSubmit } = props

    return (
        <React.Fragment >
            {
                viewOnly ? 
                    <ProfileFormViewOnly
                        profileImage={profileImage}
                        firstName={firstName}
                        lastName={lastName} 
                        gender={genderName}
                        email={email}
                        bloodType={bloodTypeName}
                        birthDate={birthDate}
                        relationship={relationship}
                        identificationNo={identificationNo}
                        contactNo1={contactNo1}
                        contactNo2={contactNo2}
                        address1={address1}
                        address2={address2}
                        address3={address3}
                        isLoading={isLoading}
                        errorMessage={errorMessage}
                        setErrorMessage={setErrorMessage}
                        onLogoPress={onBack}
                        onSubmit={onSubmit} />
                    :
                    <ProfileForm
                        methodType={methodType}
                        profileImage={profileImage}
                        firstName={firstName}
                        lastName={lastName} 
                        gender={gender}
                        email={email}
                        bloodType={bloodType}
                        birthDate={birthDate}
                        relationship={relationship}
                        identificationNo={identificationNo}
                        contactNo1={contactNo1}
                        contactNo2={contactNo2}
                        address1={address1}
                        address2={address2}
                        address3={address3}
                        isLoading={isLoading}
                        setProfileImage={setProfileImage}
                        setFirstName={setFirstName}
                        setLastName={setLastName}
                        setGender={setGender}
                        setEmail={setEmail}
                        setBloodType={setBloodType}
                        setBirthDate={setBirthDate}
                        setRelationship={setRelationship}
                        setIdentificationNo={setIdentificationNo}
                        setContactNo1={setContactNo1}
                        setContactNo2={setContactNo2}
                        setAddress1={setAddress1}
                        setAddress2={setAddress2}
                        setAddress3={setAddress3}
                        errorMessage={errorMessage}
                        setErrorMessage={setErrorMessage}
                        onLogoPress={onBack}
                        onSubmit={onSubmit} />
            }
        </React.Fragment>
    )
} 