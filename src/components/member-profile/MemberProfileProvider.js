import React from 'react'
import MemberProfileController from './MemberProfileController'
import MemberProfileViewModel from './MemberProfileViewModel'
import MemberProfileAction from './MemberProfileAction'

export default () => {
    const action = new MemberProfileAction()
    const viewModel = new MemberProfileViewModel(action)

    return (
        <MemberProfileController viewModel={viewModel} />
    )
} 
