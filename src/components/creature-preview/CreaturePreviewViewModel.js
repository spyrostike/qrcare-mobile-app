import { genenrateFileObjToServer } from '../../utils/ObjectUtil'

export default class {
    
    constructor(store) {
        this.store = store
    }

    getCreature = () => {
        return this.store.getCreature()
    }

    getDrugRecordNumber = () => {
        return this.store.getDrugRecordNumber()
    }

    getDrugAllergyRecordNumber = () => {
        return this.store.getDrugAllergyRecordNumber()
    }

    getFoodAllergyRecordNumber = () => {
        return this.store.getFoodAllergyRecordNumber()
    }

    getCreatureById = (payload) => {

        return new Promise((resolve, reject) => {
            this.store.getCreatureById(payload).then(async v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editCreature = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.editCreature(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    countDrugByCreatureId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.countDrugByCreatureId(payload).then(async v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    countDrugAllergyByCreatureId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.countDrugAllergyByCreatureId(payload).then(async v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    countFoodAllergyByCreatureId = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.countFoodAllergyByCreatureId(payload).then(async v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    addFile = (payload) => {
        const formData = genenrateFileObjToServer(payload)
        return new Promise((resolve, reject) => {
            this.store.addFile(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
