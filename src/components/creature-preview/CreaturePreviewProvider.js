import React from 'react'
import CreaturePreviewController from './CreaturePreviewController'
import CreaturePreviewViewModel from './CreaturePreviewViewModel'
import CreaturePreviewAction from './CreaturePreviewAction'

export default () => {
    const action = new CreaturePreviewAction()
    const viewModel = new CreaturePreviewViewModel(action)

    return (
        <CreaturePreviewController viewModel={viewModel} />
    )
} 
