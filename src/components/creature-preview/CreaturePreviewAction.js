import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    creature = useStoreState(state => state.creature.item)
    drugRecordNumber = useStoreState(state => state.drug.recordNumber)
    drugAllergyRecordNumber = useStoreState(state => state.drugAllergy.recordNumber)
    foodAllergyRecordNumber = useStoreState(state => state.foodAllergy.recordNumber)
    getCreatureByIdAction = useStoreActions(actions => actions.creature.getCreatureById)
    editCreatureAction = useStoreActions(actions => actions.creature.editCreature)
    countDrugByCreatureIdAction = useStoreActions(actions => actions.drug.countDrugByCreatureId)
    countDrugAllergyByCreatureIdAction = useStoreActions(actions => actions.drugAllergy.countDrugAllergyByCreatureId)
    countFoodAllergyByCreatureIdAction = useStoreActions(actions => actions.foodAllergy.countFoodAllergyByCreatureId)
    addFileAction = useStoreActions(actions => actions.file.addFile)

    getCreature = () => this.creature

    getDrugRecordNumber = () => this.drugRecordNumber

    getDrugAllergyRecordNumber = () => this.drugAllergyRecordNumber

    getFoodAllergyRecordNumber = () => this.foodAllergyRecordNumber

    getCreatureById = (payload) => this.getCreatureByIdAction(payload)

    editCreature = (payload) => this.editCreatureAction(payload)

    countDrugByCreatureId = (payload) => this.countDrugByCreatureIdAction(payload)

    countDrugAllergyByCreatureId = (payload) => this.countDrugAllergyByCreatureIdAction(payload)

    countFoodAllergyByCreatureId = payload => this.countFoodAllergyByCreatureIdAction(payload)

    addFile = payload => this.addFileAction(payload)
}