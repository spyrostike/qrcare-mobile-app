import React from 'react'
import PreviewForm from '../UI/creature/PreviewForm'

export default (props) => {
    const { 
        viewOnly,
        creatureImage,
        firstName,
        lastName,
        birthCertificate,
        gender,
        bloodType,
        birthDate,
        genderName,
        bloodTypeName,
        weight,
        height,
        drug,
        drugAllergy,
        foodAllergy,
        errorMessage,
        isLoading,
        isEdit,
        setIsEdit,
        setCreatureImage,
        setFirstName,
        setLastName,
        setBirthCertificate,
        setGender,
        setBloodType,
        setBirthDate,
        setWeight,
        setHeight,
        setErrorMessage,
        goToScreen,
        onSubmit } = props

    return (
        <PreviewForm
            viewOnly={viewOnly}
            creatureImage={creatureImage}
            firstName={firstName}
            lastName={lastName}
            birthCertificate={birthCertificate}
            gender={gender}
            bloodType={bloodType}
            birthDate={birthDate}
            genderName={genderName}
            bloodTypeName={bloodTypeName}
            weight={weight}
            height={height}
            isLoading={isLoading}
            errorMessage={errorMessage}
            drug={drug}
            drugAllergy={drugAllergy}
            foodAllergy={foodAllergy}
            isEdit={isEdit}
            setErrorMessage={setErrorMessage}
            setIsEdit={setIsEdit}
            setCreatureImage={setCreatureImage}
            setFirstName={setFirstName}
            setLastName={setLastName}
            setBirthCertificate={setBirthCertificate}
            setGender={setGender}
            setBloodType={setBloodType}
            setBirthDate={setBirthDate}
            setWeight={setWeight}
            setHeight={setHeight}
            goToScreen={goToScreen}
            onSubmit={onSubmit} />
    )
} 