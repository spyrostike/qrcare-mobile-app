import React, { useMemo, useState } from 'react'
import dayjs from 'dayjs'
import moment from 'moment'
import CreaturePreviewView from './CreaturePreviewView'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ id, setId ] = useState(null)
    const [ creatureImage, setCreatureImage ] = useState(null)
    const [ creatureImageId, setCreatureImageId ] = useState(null)
    const [ isCreatureImageChange, setIsCreatureImageChange ] = useState(false)
    const [ oldCreatureImageId, setOldCreatureImageId ] = useState(null)
    const [ firstName, setFirstName ] = useState(null)
    const [ lastName, setLastName ] = useState(null)
    const [ birthCertificate, setBirthCertificate ] = useState(null)
    const [ birthCertificateId, setBirthCertificateId ] = useState(null)
    const [ isBirthCertificateChange, setIsBirthCertificateChange ] = useState(false)
    const [ oldBirthCertificateId, setOldBirthCertificateId ] = useState(null)
    const [ gender, setGender ] = useState(null)
    const [ bloodType, setBloodType ] = useState(null)
    const [ birthDate, setBirthDate ] = useState(null)
    const [ genderName, setGenderName ] = useState(null)
    const [ bloodTypeName, setBloodTypeName ] = useState(null)
    const [ weight, setWeight ] = useState(null)
    const [ height, setHeight ] = useState(null)
    const [ memberId, setMemberId ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ isEdit, setIsEdit ] = useState(null)

    const { viewModel } = props

    const { routes, index } = Navigator.getCurrentRoute()
    const { creatureId, viewOnly } = routes[index].params
    const prefixScreen = viewOnly ? 'ViewOnly' : 'Member'

    useMemo(() => {
        
        const _getCreatureById = async () => {
            try {
                if (!creatureId) { setErrorMessage('ID not found'); return; } 
                setIsLoading(true)
                const result = await viewModel.getCreatureById(creatureId)
                
                await viewModel.countDrugByCreatureId(creatureId)
                await viewModel.countDrugAllergyByCreatureId(creatureId)
                await viewModel.countFoodAllergyByCreatureId(creatureId)
                
                _prepareData(result)
            } catch (error) {
                setErrorMessage(error)
            }
    
            setIsLoading(false)
        }

        _getCreatureById()

    }, [])

    const _prepareData = (data) => {

        setId(data.id)
        setCreatureImage(data.creatureImage)
        setOldCreatureImageId(data.creatureImageId)
        setCreatureImageId(data.creatureImageId)
        setFirstName(data.firstName)
        setLastName(data.lastName)
        setBirthCertificate(data.birthCertificate)
        setBirthCertificateId(data.birthCertificateId)
        setOldBirthCertificateId(data.birthCertificateId)
        setGender(data.gender)
        setBloodType(data.bloodType)
        setBirthDate(dayjs(data.birthDate).format('DD/MM/YYYY'))
        setGenderName(data.genderName)
        setBloodTypeName(data.bloodTypeName)
        setWeight(data.weight)
        setHeight(data.height)
        setMemberId(data.memberId)
    }
    
    const _goToScreen = (screen) => {
        Navigator.navigate(prefixScreen + screen, { 
            creatureId: creatureId,
            memberId: memberId,
            bloodTypePR: bloodType,
            onComplete: async (bloodType) => {
                if (bloodType) {
                    setBloodType(bloodType)
                    if (bloodType === 1) setBloodTypeName('AB')
                    if (bloodType === 2) setBloodTypeName('A')
                    if (bloodType === 3) setBloodTypeName('B')
                    if (bloodType === 4) setBloodTypeName('O')
                }

                Navigator.back()
            }
        })
    }

    const _onSubmit = () => {
        if (!_validate()) return
        
        const data = { 
            id: id,
            creatureImageId: creatureImageId,
            oldCreatureImageId: isCreatureImageChange ? oldCreatureImageId : null,
            firstName: firstName, 
            lastName: lastName,
            birthCertificateId: birthCertificateId,
            oldBirthCertificateId: isBirthCertificateChange ? oldBirthCertificateId : null,
            gender: gender, 
            bloodType: bloodType, 
            birthDate: moment(birthDate, 'DD/MM/YYYY').format(), 
            weight: weight,
            height: height
        }

        if (id) {
            _editCreature(data)
        }
    }

    const _editCreature = async (data) => {
        setIsLoading(true)

        try {
            const result = await viewModel.editCreature(data)
            setIsLoading(false)

            const { routes, index } = Navigator.getCurrentRoute()
            const { onComplete } = routes[index].params

            onComplete(result.id)
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    } 

    const _validate = () => {
        if (creatureImageId === null) { setErrorMessage('Image is required'); return false; }
        if (firstName === null || firstName.trim().length === 0) { setErrorMessage('Firstname is required'); return false; }
        if (lastName === null || lastName.trim().length === 0) { setErrorMessage('Lastname is required'); return false; }
        if (birthCertificateId === null) { setErrorMessage('Birth certificate is required'); return false; }
        if (gender === null) { setErrorMessage('Gender certificate is required'); return false; }
        if (bloodType === null) { setErrorMessage('Blood type is required'); return false; }
        if (birthDate === null || birthDate.trim().length === 0) { setErrorMessage('Birthdate is required'); return false; }
        if (weight === null || weight.trim().length === 0) { setErrorMessage('Weight is required'); return false; }
        if (!/^\d+(\.\d{1,2})?$/.test(weight)) { setErrorMessage('Username should be between 0-9 and (.) only. '); return false; }
        if (height === null || height.trim().length === 0) { setErrorMessage('Height is required'); return false; }

        return true
    }

    const _setCreatureImage = async (image) => {
        const file = await _setImage(image, 'creature-profile.js', 'creature-profile-image')
        setCreatureImageId(file.id)
        setCreatureImage(file.src)
        setIsCreatureImageChange(true)
    }
    
    const _setBirthCertificate = async (image) => {
        const file = await _setImage(image, 'creature-birth-certificate.js', 'creature-birth-certificate-image')
        setBirthCertificateId(file.id)
        setBirthCertificate(file.src)
        setIsBirthCertificateChange(true)
    }

    const _setImage = async (image, name, tag) => {
        try {
            setIsLoading(true)

            const data = {
                name: name,
                tag: tag,
                type: image.mime,
                uri: image.path
 
            }
            
            const file = await viewModel.addFile(data)
            setIsLoading(false)
            return file
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    return (
        <CreaturePreviewView
            viewOnly={viewOnly}
            creatureImage={creatureImage}
            firstName={firstName}
            lastName={lastName}
            birthCertificate={birthCertificate}
            gender={gender}
            bloodType={bloodType}
            birthDate={birthDate}
            weight={weight}
            height={height}
            genderName={genderName}
            bloodTypeName={bloodTypeName}
            isLoading={isLoading}
            isEdit={isEdit}
            drug={viewModel.getDrugRecordNumber() === null || viewModel.getDrugRecordNumber() <= 0  ? 'None' : viewModel.getDrugRecordNumber() + (viewModel.getDrugRecordNumber() > 1 ? ' items' : ' item')}
            drugAllergy={viewModel.getDrugAllergyRecordNumber() === null || viewModel.getDrugAllergyRecordNumber() <= 0 ? 'None' : viewModel.getDrugAllergyRecordNumber() + (viewModel.getDrugAllergyRecordNumber() > 1 ? ' items' : ' item')}
            foodAllergy={viewModel.getFoodAllergyRecordNumber() === null || viewModel.getFoodAllergyRecordNumber()  <= 0 ? 'None' : viewModel.getFoodAllergyRecordNumber() + (viewModel.getFoodAllergyRecordNumber() > 1 ? ' items' : ' item')}
            errorMessage={errorMessage}
            goToScreen={_goToScreen}
            setIsEdit={setIsEdit}
            setCreatureImage={_setCreatureImage}
            setFirstName={setFirstName}
            setLastName={setLastName}
            setBirthCertificate={_setBirthCertificate}
            setGender={setGender}
            setBloodType={setBloodType}
            setBirthDate={setBirthDate}
            setWeight={setWeight}
            setHeight={setHeight}
            onSubmit={_onSubmit}
            setErrorMessage={setErrorMessage} />
    )
} 
