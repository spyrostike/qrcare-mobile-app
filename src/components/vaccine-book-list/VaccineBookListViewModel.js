export default class {
    
    constructor(store) {
        this.store = store
    }

    getVaccineBookList = () => this.store.getVaccineBookList()

    setVaccineBookList = (payload) => this.store.setVaccineBookList(payload)

    getVaccineBookListByCreatureId = (creatureId) => {
        return new Promise((resolve, reject) => {
            this.store.getVaccineBookListByCreatureId(creatureId).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeVaccineBook = payload => {
        return new Promise((resolve, reject) => {
            this.store.removeVaccineBook(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeVaccineBooks = payload => {
        return new Promise((resolve, reject) => {
            this.store.removeVaccineBooks(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
