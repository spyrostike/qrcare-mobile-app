import React from 'react'
import VaccineBookListController from './VaccineBookListController'
import VaccineBookListViewModel from './VaccineBookListViewModel'
import VaccineBookListAction from './VaccineBookListAction'

export default () => {
    const action = new VaccineBookListAction()
    const viewModel = new VaccineBookListViewModel(action)

    return (
        <VaccineBookListController viewModel={viewModel} />
    )
} 
