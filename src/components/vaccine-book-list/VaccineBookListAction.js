import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    vaccineBookList = useStoreState(state => state.vaccine.bookList)
    setVaccineBookListAction = useStoreActions(actions => actions.vaccine.setBookList)
    getVaccineBookListByCreatureIdAction = useStoreActions(actions => actions.vaccine.getVaccineBookListByCreatureId)
    removeVaccineBookAction = useStoreActions(actions => actions.vaccine.removeVaccineBook)
    removeVaccineBooksAction = useStoreActions(actions => actions.vaccine.removeVaccineBooks)
    
    getVaccineBookList = () => this.vaccineBookList

    setVaccineBookList = payload => this.setVaccineBookListAction(payload)

    getVaccineBookListByCreatureId = payload => this.getVaccineBookListByCreatureIdAction(payload)
    
    removeVaccineBook = (payload) => this.removeVaccineBookAction(payload)
    
    removeVaccineBooks = payload => this.removeVaccineBooksAction(payload)
    
}