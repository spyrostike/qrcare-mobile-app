import React from 'react'
import VaccineBookList from '../UI/vaccine/VaccineBookList'

export default (props) => {
    const { 
        viewOnly,
        mode,
        list, 
        checkList,
        onItemPress, 
        isLoading, 
        errorMessage, 
        setErrorMessage,
        setMode,
        onSubmit,
        onCheckBoxPress,
        initialCheckList } = props
    
    return (
        <React.Fragment >
            <VaccineBookList 
                viewOnly={viewOnly}
                mode={mode}
                items={list} 
                checkItems={checkList} 
                onItemPress={onItemPress} 
                isLoading={isLoading}
                errorMessage={errorMessage}
                setErrorMessage={setErrorMessage}
                onItemLongPress={setMode}
                onCheckBoxPress={onCheckBoxPress}
                onSubmit={onSubmit}
                onClearCheckList={initialCheckList} />
        </React.Fragment>
    )
} 