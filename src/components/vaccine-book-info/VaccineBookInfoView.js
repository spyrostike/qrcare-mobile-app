import React from 'react'
import VaccineBookInfoForm from '../UI/vaccine/VaccineBookInfoForm'
import VaccineBookInfoFormViewOnly from '../UI/vaccine/VaccineBookInfoFormViewOnly'

export default (props) => {
    const { 
        viewOnly,
        methodType,
        vaccineType,
        vaccineTypeName,
        other,
        list,
        isLoading,
        isEdit,
        errorMessage,
        dialogMessage,
        setErrorMessage,
        setIsEdit,
        setVaccineType,
        setOther,
        onItemPress,
        removeItem,
        onSubmit, 
        onCancel,
        onConfirm } = props

    return (
        <React.Fragment >
            {
                viewOnly ? 
                    <VaccineBookInfoFormViewOnly 
                        vaccineType={vaccineType}
                        vaccineTypeName={vaccineTypeName}
                        other={other}
                        items={list}
                        isLoading={isLoading}
                        errorMessage={errorMessage}
                        dialogMessage={dialogMessage}
                        setErrorMessage={setErrorMessage}
                        onItemPress={onItemPress} 
                        onConfirm={onConfirm}
                        onCancel={onCancel} />
                    :
                    <VaccineBookInfoForm 
                        methodType={methodType}
                        vaccineType={vaccineType}
                        vaccineTypeName={vaccineTypeName}
                        other={other}
                        items={list}
                        isLoading={isLoading}
                        isEdit={isEdit}
                        errorMessage={errorMessage}
                        dialogMessage={dialogMessage}
                        isEdit={isEdit}
                        setVaccineType={setVaccineType}
                        setOther={setOther}
                        setErrorMessage={setErrorMessage}
                        setIsEdit={setIsEdit}
                        onItemPress={onItemPress} 
                        onItemLongPress={removeItem}
                        onConfirm={onConfirm}
                        onCancel={onCancel}
                        onSubmit={onSubmit} />
            }
        </React.Fragment>
    )
} 