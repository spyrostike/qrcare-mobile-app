import React from 'react'
import VaccineBookInfoController from './VaccineBookInfoController'
import VaccineBookInfoViewModel from './VaccineBookInfoViewModel'
import VaccineBookInfoAction from './VaccineBookInfoAction'

export default () => {
    const action = new VaccineBookInfoAction()
    const viewModel = new VaccineBookInfoViewModel(action)

    return (
        <VaccineBookInfoController viewModel={viewModel} />
    )
} 
