import React, { useMemo, useState } from 'react'
import VaccineBookInfoView from './VaccineBookInfoView'
import Navigator from '../../services/Navigator'
import _ from 'lodash'

export default (props) => {
    const [ id, setId ] = useState(null)
    const [ vaccineType, setVaccineType ] = useState(1)
    const [ vaccineTypeName, setVaccineTypeName ] = useState(null)
    const [ other, setOther ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(null)
    const [ dialogMessage, setDialogMessage ] = useState(null)
    const [ isEdit, setIsEdit ] = useState(null)

    const { routes, index } = Navigator.getCurrentRoute()
    let { methodType, creatureId, vaccineBookId, onComplete, viewOnly } = routes[index].params

    const prefixScreen = viewOnly ? 'ViewOnly' : 'Member'

    const { viewModel } = props

    const _getVaccineBookById = async () => {
        try {
            if (methodType === 'add') { setIsEdit(true); return; }
            if (typeof vaccineBookId === 'undefined' || vaccineBookId === null) { setDialogMessage('ID not found'); return; } 
            setIsLoading(true)
            const result = await viewModel.getVaccineBookById(vaccineBookId)
            await _getVaccineListByBookId(vaccineBookId)
            _prepareData(result)
        } catch (error) {
            setDialogMessage(error)
        }

        setIsLoading(false)
    }

    const _getVaccineBookListByCreatureId = async () => {
        try {
            setIsLoading(true)
            await viewModel.getVaccineBookListByCreatureId(creatureId)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _getVaccineListByBookId = async (vaccineBookId) => {
        setIsLoading(true)
        try {
            await viewModel.getVaccineListByBookId(vaccineBookId)
        } catch (error) {
            setDialogMessage(error)
        }
        
        setIsLoading(false)
    }

    useMemo(() => {
        viewModel.setVaccineBook(null)
        viewModel.setVaccineList([])
        _getVaccineBookById()
        
    }, [])

    const _prepareData = (data) => {  
        const { id, vaccineType, vaccineTypeName, other } = data

        setId(id)
        setVaccineType(vaccineType)
        setVaccineTypeName(vaccineTypeName)
        setOther(other)
    }

    const _onSubmit = () => {
        if (!_validate()) return
        if (isLoading) return

        let data = {
            id: id,
            vaccineType: vaccineType,
            other: other,
            creatureId: creatureId
        }

        if (id === null) _delayAdd(data)
        else _delayEdit(data)
    }

    const _addVaccineBook = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.addVaccineBook(data)
            setIsLoading(false)
            onComplete()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    } 

    const _editVaccineBook = async (data) => {
        setIsLoading(true)

        try {
            await viewModel.editVaccineBook(data)
            setIsLoading(false)
            onComplete()
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _delayAdd = _.debounce( _addVaccineBook, 500 )
    const _delayEdit = _.debounce( _editVaccineBook, 500 )

    const _validate = () => {
        if (vaccineType === null || vaccineType.length === 0) { setErrorMessage('Vaccine is required'); return false; }
        if (vaccineType === 14 && (other === null || other.length === 0)) { setErrorMessage('Other is required'); return false; }
        return true
    }

    const _onItemPress = async (value) => {
        if (value) {
            Navigator.navigate(prefixScreen + 'VaccineInfo', { 
                methodType: 'edit', 
                vaccineId: value.id,
                creatureId: creatureId,
                onComplete: async () => {
                    Navigator.back()
                }
            })
        } else {
            if (viewOnly) return 
            if (!id) {
                if (!_validate()) return

                let data = {
                    id: id,
                    vaccineType: vaccineType,
                    other: other,
                    creatureId: creatureId
                }

                setIsLoading(true)

                try {
                    let vaccineBook = await viewModel.addVaccineBook(data)
                    vaccineBookId = vaccineBook.id
                    setId(vaccineBookId)
                    await _getVaccineBookListByCreatureId()
                    // _prepareData(vaccineBook)
                } catch (error) {
                    setErrorMessage(error)
                }

                setIsLoading(false)

                Navigator.navigate(prefixScreen + 'VaccineInfo', { 
                    methodType: 'add',
                    vaccineBookId: vaccineBookId,
                    creatureId: creatureId,
                    onComplete: async () => {
                        Navigator.back()
                        await _getVaccineListByBookId(vaccineBookId)
                    }
                })
            } else {
                Navigator.navigate(prefixScreen + 'VaccineInfo', { 
                    methodType: 'add',
                    vaccineBookId: id,
                    creatureId: creatureId,
                    onComplete: async () => {
                        Navigator.back()
                        await _getVaccineListByBookId(id)
                    }
                })
            }
            
        }
    }

    const _removeItem = async (value) => {
        setIsLoading(true)

        try {
            await viewModel.removeVaccine(value.id)
            await _getVaccineListByBookId(id)
        } catch (error) {
            setErrorMessage(error)
        }

        setIsLoading(false)
    }

    const _onRefresh = () => {
        setDialogMessage(null)
        setTimeout(() => {
            _getVaccineBookById()
        }, 300)
        
    }

    const _onCancel = () => {
        setDialogMessage(null)
        Navigator.back()
    }

    const _setVaccineType = (value) => {
        setVaccineType(value)
        setOther(null)
    }

    return (
        <VaccineBookInfoView 
            viewOnly={viewOnly}
            methodType={methodType}
            vaccineType={vaccineType}
            vaccineTypeName={vaccineTypeName}
            other={other}
            list={viewModel.getVaccineList()}
            isLoading={isLoading}
            errorMessage={errorMessage}
            dialogMessage={dialogMessage}
            isEdit={isEdit}
            setVaccineType={_setVaccineType}
            setOther={setOther}
            setErrorMessage={setErrorMessage}
            setIsEdit={setIsEdit}
            onSubmit={_onSubmit}
            onItemPress={_onItemPress}
            removeItem={_removeItem}
            onConfirm={_onRefresh}
            onRefresh={_onRefresh}
            onCancel={_onCancel} />
    )
} 
