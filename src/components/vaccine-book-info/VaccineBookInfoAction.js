import { useStoreActions, useStoreState } from 'easy-peasy'

export default class {
    vaccineBook = useStoreState(state => state.vaccine.book)
    vaccineList = useStoreState(state => state.vaccine.list)
    setVaccineBookAction = useStoreActions(actions => actions.vaccine.setBook)
    setVaccineListAction = useStoreActions(actions => actions.vaccine.setList)
    addVaccineBookAction = useStoreActions(actions => actions.vaccine.addVaccineBook)
    editVaccineBookAction = useStoreActions(actions => actions.vaccine.editVaccineBook)
    getVaccineBookByIdAction = useStoreActions(actions => actions.vaccine.getVaccineBookById)
    getVaccineListByBookIdAction = useStoreActions(actions => actions.vaccine.getVaccineListByBookId)
    removeVaccineAction = useStoreActions(actions => actions.vaccine.removeVaccine)
    getVaccineBookListByCreatureIdAction = useStoreActions(actions => actions.vaccine.getVaccineBookListByCreatureId)

    getVaccineBook = () => this.vaccineBook

    getVaccineList = () => this.vaccineList

    setVaccineBook = payload => this.setVaccineBookAction(payload)
    
    setVaccineList = payload => this.setVaccineListAction(payload)

    addVaccineBook = payload => this.addVaccineBookAction(payload)

    editVaccineBook = payload => this.editVaccineBookAction(payload)

    getVaccineBookById = payload => this.getVaccineBookByIdAction(payload)

    getVaccineListByBookId = payload => this.getVaccineListByBookIdAction(payload)

    removeVaccine = payload => this.removeVaccineAction(payload)

    getVaccineBookListByCreatureId = (payload) => this.getVaccineBookListByCreatureIdAction(payload)
    
}