export default class {
    
    constructor(store) {
        this.store = store
    }

    getVaccineBook = () => {
        return this.store.getVaccineBook()
    }
    
    getVaccineList = () => {
        return this.store.getVaccineList()
    }

    setVaccineBook = (payload) => {
        return this.store.setVaccineBook(payload)
    }
    
    setVaccineList = (payload) => {
        return this.store.setVaccineList(payload)
    }

    addVaccineBook = payload => {
        return new Promise((resolve, reject) => {
            this.store.addVaccineBook(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editVaccineBook = payload => {
        return new Promise((resolve, reject) => {
            this.store.editVaccineBook(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getVaccineBookById = payload => {
        return new Promise((resolve, reject) => {
            this.store.getVaccineBookById(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getVaccineListByBookId = payload => {
        return new Promise((resolve, reject) => {
            this.store.getVaccineListByBookId(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    removeVaccine = payload => {
        return new Promise((resolve, reject) => {
            this.store.removeVaccine(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getVaccineBookListByCreatureId = (creatureId) => {
        return new Promise((resolve, reject) => {
            this.store.getVaccineBookListByCreatureId(creatureId).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
