import { useStoreActions } from 'easy-peasy'

export default class {
    addDrugAction = useStoreActions(actions => actions.drug.addDrug)
    editDrugAction = useStoreActions(actions => actions.drug.editDrug)
    getDrugByIdAction = useStoreActions(actions => actions.drug.getDrugById)
    addFileAction = useStoreActions(actions => actions.file.addFile)

    addDrug = (value) => this.addDrugAction(value)

    editDrug = payload => this.editDrugAction(payload)

    getDrugById = (payload) => this.getDrugByIdAction(payload)

    addFile = payload => this.addFileAction(payload)

}