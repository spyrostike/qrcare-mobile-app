import React from 'react'
import DrugInfoController from './DrugInfoController'
import DrugInfoViewModel from './DrugInfoViewModel'
import DrugInfoAction from './DrugInfoAction'

export default () => {
    const action = new DrugInfoAction()
    const viewModel = new DrugInfoViewModel(action)

    return (
        <DrugInfoController viewModel={viewModel} />
    )
} 
