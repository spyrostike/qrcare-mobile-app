import { genenrateFileObjToServer } from '../../utils/ObjectUtil'

export default class {
    
    constructor(store) {
        this.store = store
    }

    addDrug = payload => {
        return new Promise((resolve, reject) => {
            this.store.addDrug(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    editDrug = payload => {
        return new Promise((resolve, reject) => {
            this.store.editDrug(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    getDrugById = (payload) => {
        return new Promise((resolve, reject) => {
            this.store.getDrugById(payload).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

    addFile = (payload) => {
        const formData = genenrateFileObjToServer(payload)
        return new Promise((resolve, reject) => {
            this.store.addFile(formData).then(v => {
                resolve(v)
            }).catch(error => {
                reject(error)
            })
        })
    }

}
