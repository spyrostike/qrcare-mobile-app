import HttpRequest from './http-request'

class TreatmentHistoryProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    addTreatmentHistory = payload => {
        return new Promise((resolve, reject) => {
                this.create('/treatment-history/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editTreatmentHistory = payload => {
        return new Promise((resolve, reject) => {
                this.update('/treatment-history/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getTreatmentHistoryById = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/treatment-history/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getTreatmentHistoryListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/treatment-history/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeTreatmentHistory = payload => {
        return new Promise((resolve, reject) => {
                this.delete(`/treatment-history/delete/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeTreatmentHistoryArray = payload => {
        return new Promise((resolve, reject) => {
                this.update(`/treatment-history/delete`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    sendMail = payload => {
        return new Promise((resolve, reject) => {
                this.create(`/treatment-history/send-mail`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default TreatmentHistoryProvider