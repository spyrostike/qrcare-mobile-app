import HttpRequest from './http-request'

class MedicalProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    addMedical (payload) {
        return new Promise((resolve, reject) => {
                this.create('/medical/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editMedical (payload) {
        return new Promise((resolve, reject) => {
                this.update('/medical/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getMedicalByCreatureId (payload) {
        return new Promise((resolve, reject) => {
                this.get(`/medical/get-medical-by-creature-id/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default MedicalProvider