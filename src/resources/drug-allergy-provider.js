import HttpRequest from './http-request'

class DrugAllergyProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    addDrugAllergy = payload => {
        return new Promise((resolve, reject) => {
                this.create('/drug-allergy/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editDrugAllergy = payload => {
        return new Promise((resolve, reject) => {
                this.update('/drug-allergy/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getDrugAllergyById = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug-allergy/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getDrugAllergyListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug-allergy/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    countDrugAllergyByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug-allergy/count/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeDrugAllergy = payload => {
        return new Promise((resolve, reject) => {
                this.delete(`/drug-allergy/delete/${payload}` ).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeDrugAllergies = payload => {
        return new Promise((resolve, reject) => {
                this.update(`/drug-allergy/delete`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default DrugAllergyProvider