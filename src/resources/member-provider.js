import HttpRequest from './http-request'

class MemberProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    checkUserExist (payload) {
        return new Promise((resolve, reject) => {
                this.request('POST', '/member/check-username-exist', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    checkEmailExist (payload) {
        return new Promise((resolve, reject) => {
                this.request('POST', '/member/check-email-exist', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    register (payload) {
        return new Promise((resolve, reject) => {
                this.request('POST', '/member/register', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    authen (payload) {
        return new Promise((resolve, reject) => {
                this.request('POST', '/member/authen', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getMemberProfile () {
        return new Promise((resolve, reject) => {
                this.get('/member/profile').then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getMemberById = payload => {
            return new Promise((resolve, reject) => {
                this.get(`/member/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editMemberProfile = payload => {
        return new Promise((resolve, reject) => {
                this.update('/member/update-profile', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editMemberLocation = payload => {
        return new Promise((resolve, reject) => {
                this.update('/member/update-location', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    changePassword = payload => {
        return new Promise((resolve, reject) => {
                this.update('/member/change-password', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    forgetPassword = payload => {
        return new Promise((resolve, reject) => {
                this.update('/member/forget-password', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default MemberProvider