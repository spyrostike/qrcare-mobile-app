import HttpRequest from './http-request'

class QRCodeItemProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    getQRCodeItemById (id) {
        return new Promise((resolve, reject) => {
                this.request('GET', `/qrcode/get-qrcode-item-by-id/${id}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default QRCodeItemProvider