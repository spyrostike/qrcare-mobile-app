import axios from 'axios'
import { API_ENDPOINT } from '../../appConfig'
import { getUserToken } from '../services/Token'

class HttpRequest {
    constructor (url = API_ENDPOINT) {
        // this.axios = axios
        
        this.axiosInstance = axios.create({
            baseURL: url,
            timeout: 30000,
            headers: { 'Content-Type': 'application/json' }
        })

        this.axiosInstance.interceptors.request.use(async function (config) {
            // Do something before request is sent
            const token = await getUserToken()
            
            if(token) {
                config.headers.common['Authorization'] = `Bearer ${token}`
            }

            return config
        }, function (error) {
            // Do something with request error
            return Promise.reject(error)
        })

        // Add a response interceptor
        this.axiosInstance.interceptors.response.use(function (response) {
            // Do something with response data
            
            return response
        }, function (error) {
            // Do something with response error
            // if (error.response.status === 401) {

            // }
            return Promise.reject(error)
        })
    }

    setHeader (key, value) {
        // this.axiosInstance.defaults.headers.common[header.key] = header.value
        // this.axiosInstance.defaults.headers.common = header
        
        this.axiosInstance.defaults.headers.common[key] = value

        // this.axiosInstance.defaults.headers.post['Content-Type'] = 'application/json'

        // const { authId } = header

        // if (authId) {
        //   this.axiosInstance.defaults.headers.common['authId'] = authId
        // }

        // this.axiosInstance.defaults.headers.common['accessId'] = APP_ID
    
    }

    removeAllHeaders = () => {
        delete instance.defaults.headers.common['authId']
        delete instance.defaults.headers.common['accessId']
    }

    get (methodName, data) {
            return this.axiosInstance.get(methodName, {
            params: data
        })
    }

    create (methodName, data) {
        return this.axiosInstance.post(methodName, data)
    }

    update (methodName, data) {
        return this.axiosInstance.put(methodName, data)
    }

    delete (methodName, params, data) {
        return this.axiosInstance.delete(methodName, {
            params: params,
            data: data
        })
    }

    request (type, url, data) {
        let promise = null
        axios.defaults.baseURL = API_ENDPOINT

        // axios.defaults.headers.common['regsToken'] = `${clientGUID()}`
        // axios.defaults.headers.common['accessId'] = APP_ID

        switch (type) {
            case 'GET': promise = axios.get(url, { params: data }); break
            case 'POST': promise = axios.post(url, data); break
            case 'PUT': promise = axios.put(url, data); break
            case 'DELETE': promise = axios.delete(url, data); break
            default : promise = axios.get(url, { params: data }); break
        }
        return promise
    }
}

export default HttpRequest