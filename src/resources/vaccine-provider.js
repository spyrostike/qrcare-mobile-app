import HttpRequest from './http-request'

class VaccineProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// vaccine book ////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    addVaccineBook = payload => {
        return new Promise((resolve, reject) => {
                this.create('/vaccine/book/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editVaccineBook = payload => {
        return new Promise((resolve, reject) => {
                this.update('/vaccine/book/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeVaccineBook = payload => {
        return new Promise((resolve, reject) => {
                this.delete(`/vaccine/book/${payload}` ).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeVaccineBooks = payload => {
        return new Promise((resolve, reject) => {
                this.update(`/vaccine/book`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
    getVaccineBookListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/vaccine/book/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getVaccineBookById = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/vaccine/book/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    ///////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// vaccine ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    addVaccine = payload => {
        return new Promise((resolve, reject) => {
                this.create('/vaccine/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editVaccine = payload => {
        return new Promise((resolve, reject) => {
                this.update('/vaccine/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeVaccine = payload => {
        return new Promise((resolve, reject) => {
                this.delete(`/vaccine/${payload}` ).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getVaccineListByBookId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/vaccine/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getVaccineById = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/vaccine/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
}

export default VaccineProvider