import HttpRequest from './http-request'

class DrugProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    addDrug = payload => {
        return new Promise((resolve, reject) => {
                this.create('/drug/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editDrug = payload => {
        return new Promise((resolve, reject) => {
                this.update('/drug/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getDrugById = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getDrugListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    countDrugByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/drug/count/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeDrug = payload => {
        return new Promise((resolve, reject) => {
                this.delete(`/drug/delete/${payload}` ).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeDrugs = payload => {
        return new Promise((resolve, reject) => {
                this.update(`/drug/delete`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default DrugProvider