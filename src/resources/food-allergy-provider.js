import HttpRequest from './http-request'

class FoodAllergyProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    addFoodAllergy = payload => {
        return new Promise((resolve, reject) => {
                this.create('/food-allergy/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editFoodAllergy = payload => {
        return new Promise((resolve, reject) => {
                this.update('/food-allergy/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getFoodAllergyById = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/food-allergy/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getFoodAllergyListByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/food-allergy/list/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    countFoodAllergyByCreatureId = payload => {
        return new Promise((resolve, reject) => {
                this.get(`/food-allergy/count/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeFoodAllergy = payload => {
        return new Promise((resolve, reject) => {
                this.delete(`/food-allergy/delete/${payload}` ).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeFoodAllergies = payload => {
        return new Promise((resolve, reject) => {
                this.update(`/food-allergy/delete`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default FoodAllergyProvider