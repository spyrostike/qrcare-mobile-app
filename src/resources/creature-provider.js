import HttpRequest from './http-request'

class CreatureProvider extends HttpRequest {
    constructor () {
        // api api
        super()
    }

    addCreature (payload) {
        return new Promise((resolve, reject) => {
                this.create('/creature/add', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    editCreature (payload) {
        return new Promise((resolve, reject) => {
                this.update('/creature/update', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getCreatureById (payload) {
        return new Promise((resolve, reject) => {
                this.get(`/creature/${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getCreatureListByMemberId () {
        return new Promise((resolve, reject) => {
                this.get('/creature/list').then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getCreatureByQRCodeId = payload => {
        return new Promise((resolve, reject) => {
            this.get(`/creature/creature-by-qrcode-id/${payload}`).then(v => {
            resolve(v.data) 
        }).catch((error) => {
            reject(error)
        })
    })
    }

    removeCreature (payload) {
        return new Promise((resolve, reject) => {
                this.delete(`/creature/delete/${payload}` ).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    removeCreatures = payload => {
        return new Promise((resolve, reject) => {
                this.update(`/creature/delete`, payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default CreatureProvider