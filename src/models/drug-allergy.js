import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import DrugAllergyProvider from '../resources/drug-allergy-provider'
import { convertDrugFromServerToApp } from '../utils/ObjectUtil'

const drugAllergyService = new DrugAllergyProvider()

export default {
    list: [],
    item: null,
    recordNumber: null,
    setItem: action((state, payload) => {
        state.item = payload    
    }),
    setList: action((state, payload) => {
        state.list = payload    
    }),
    setRecordNumber: action((state, payload) => {
        state.recordNumber = payload    
    }),
    addDrugAllergy: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugAllergyService.addDrugAllergy(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editDrugAllergy: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugAllergyService.editDrugAllergy(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getDrugAllergyById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugAllergyService.getDrugAllergyById(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    
                    const obj = convertDrugFromServerToApp(data.result)
                    actions.setItem(obj)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getDrugAllergyListByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugAllergyService.getDrugAllergyListByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setList(data.result)
                    actions.setRecordNumber(data.result.length)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    countDrugAllergyByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugAllergyService.countDrugAllergyByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setRecordNumber(data.result)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeDrugAllergy: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugAllergyService.removeDrugAllergy(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeDrugAllergies: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugAllergyService.removeDrugAllergies(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    })
}