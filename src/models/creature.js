import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import CreatureProvider from '../resources/creature-provider'
import { convertCreatureProfileFromServerToApp } from '../utils/ObjectUtil'

const creatureService = new CreatureProvider()

export default {
    list: [],
    item: null,
    setList: action((state, payload) => {
        state.list = payload    
    }),
    setItem: action((state, payload) => {
        state.item = payload    
    }),
    addCreature: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            creatureService.addCreature(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editCreature: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            creatureService.editCreature(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getCreatureById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            creatureService.getCreatureById(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    const obj = convertCreatureProfileFromServerToApp(data.result)
                    actions.setItem(obj)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getCreatureByQRCodeId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            creatureService.getCreatureByQRCodeId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    const obj = convertCreatureProfileFromServerToApp(data.result)
                    actions.setItem(obj)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getCreatureListByMemberId: thunk(async (actions) => {
        return new Promise((resolve, reject) => {
            creatureService.getCreatureListByMemberId().then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setList(data.result)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeCreature: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            creatureService.removeCreature(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeCreatures: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            creatureService.removeCreatures(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    })
}
