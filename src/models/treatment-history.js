import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import TreatmentHistoryProvider from '../resources/treatment-history-provider'
import { convertTreatmentHistoryFromServerToApp } from '../utils/ObjectUtil'

const treamemtHistoryService = new TreatmentHistoryProvider()

export default {
    list: [],
    item: null,
    setItem: action((state, payload) => {
        state.item = payload    
    }),
    setList: action((state, payload) => {
        state.list = payload    
    }),
    addTreatmentHistory: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            treamemtHistoryService.addTreatmentHistory(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editTreatmentHistory: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            treamemtHistoryService.editTreatmentHistory(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getTreatmentHistoryById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            treamemtHistoryService.getTreatmentHistoryById(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    
                    const obj = convertTreatmentHistoryFromServerToApp(data.result)
                    actions.setItem(obj)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getTreatmentHistoryListByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            treamemtHistoryService.getTreatmentHistoryListByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setList(data.result)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeTreatmentHistory: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            treamemtHistoryService.removeTreatmentHistory(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeTreatmentHistoryArray: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            treamemtHistoryService.removeTreatmentHistoryArray(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    sendMail: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            treamemtHistoryService.sendMail(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    })
}