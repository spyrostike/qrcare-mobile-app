import creature from './creature'
import drug from './drug'
import drugAllergy from './drug-allergy'
import file from './file'
import foodAllergy from './food-allergy'
import medical from './medical'
import member from './member'
import qrcode from './qrcode'
import treatmentHistory from './treatment-history'
import vaccine from './vaccine'

export default  {
    creature: creature,
    drug: drug,
    drugAllergy: drugAllergy,
    file: file,
    foodAllergy: foodAllergy,
    member: member,
    medical: medical,
    qrcode: qrcode,
    treatmentHistory: treatmentHistory,
    vaccine: vaccine
}