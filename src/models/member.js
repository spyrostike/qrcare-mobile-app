import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import MemberProvider from '../resources/member-provider'
import { convertMemberProfileFromServerToApp } from '../utils/ObjectUtil'

const memberService = new MemberProvider()

export default {
    token: null,
    authentication: null,
    profile: null,
    location: null,
    setToken: action((state, payload) => {
        state.token = payload    
    }),
    setAuthentication: action((state, payload) => {
        state.authentication = payload 
    }), 
    setProfile: action((state, payload) => {
        state.profile = payload    
    }), 
    setLocation: action((state, payload) => {
        state.location = payload    
    }),
    checkUserExist: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.checkUserExist({ username: payload.username }).then(v => {
                const { status, data, errorMessage } = v
                
                if (status === API_STATUS_SUCCESS) {
                    actions.setAuthentication(payload)
                    resolve(data)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    checkEmailExist: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.checkEmailExist({ email: payload.email }).then(v => {
                const { status, data, errorMessage } = v
                
                if (status === API_STATUS_SUCCESS) {
                    resolve(data)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    register: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.register(payload).then(v => {
                const { status, data, errorMessage } = v

                if (status === API_STATUS_SUCCESS) {
                    actions.setToken(data)
                    resolve(data)
                } else {
                    reject(errorMessage)
                }
                
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    authen: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.authen(payload).then(v => {
                const { status, data, errorMessage } = v

                if (status === API_STATUS_SUCCESS) {
                    actions.setToken(data)
                    resolve(data)
                } else {
                    reject(errorMessage)
                }
                
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getMemberProfile: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.getMemberProfile(payload).then(v => {
                const { status, data, errorMessage } = v
                
                if (status === API_STATUS_SUCCESS) {
                    const obj = convertMemberProfileFromServerToApp(data.result)
                    actions.setAuthentication(obj.authentication)
                    actions.setProfile(obj.profile)
                    actions.setLocation(obj.location)
                    actions.setToken(payload)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
                
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getMemberById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.getMemberById(payload).then(v => {
                const { status, data, errorMessage } = v
                
                if (status === API_STATUS_SUCCESS) {
                    const obj = convertMemberProfileFromServerToApp(data.result)
                    actions.setAuthentication(obj.authentication)
                    actions.setProfile(obj.profile)
                    actions.setLocation(obj.location)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
                
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editMemberProfile: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.editMemberProfile(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editMemberLocation: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.editMemberLocation(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    changePassword: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.changePassword(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    forgetPassword: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            memberService.forgetPassword(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    })
}