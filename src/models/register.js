import { action } from 'easy-peasy'

export default {
    authentication: null,
    profile: null,
    location: null,
    creatureInfo: null,
    creatureMDInfo: null,
    setAuthentication: action((state, payload) => {
        state.authentication = payload    
    }),
    setProfile: action((state, payload) => {
        state.profile = payload    
    }),
    setLocation: action((state, payload) => {
        state.location = payload    
    }),
    setCreatureInfo: action((state, payload) => {
        state.creatureInfo = payload    
    }),
    setCreatureMDInfo: action((state, payload) => {
        state.creatureMDInfo = payload    
    })
}