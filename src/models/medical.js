import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import MedicalProvider from '../resources/medical-provider'
import { convertMedicalFromServerToApp } from '../utils/ObjectUtil'

const medicalService = new MedicalProvider()

export default {
    item: null,

    setItem: action((state, payload) => {
        state.item = payload    
    }),
    addMedical: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            medicalService.addMedical(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editMedical: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            medicalService.editMedical(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getMedicalByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            medicalService.getMedicalByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    const obj = convertMedicalFromServerToApp(data.result)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    })
}