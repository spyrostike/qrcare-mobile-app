import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import QRCodeProvider from '../resources/qrcode-provider'

const qrcodeService = new QRCodeProvider()

export default {
    item: null,
    setInfo: action((state, payload) => {
        state.item = payload 
    }),
    getQRCodeItemById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            qrcodeService.getQRCodeItemById(payload).then(v => {
                const { status, data, errorMessage } = v
                
                if (status === API_STATUS_SUCCESS) {
                    actions.setInfo(data.result)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }

            }).catch(error => {
                reject(error.message)
            })
        })

    })
}