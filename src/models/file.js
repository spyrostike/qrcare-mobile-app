import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import FileProvider from '../resources/file-provider'
import { convertFileObjFromServerToApp } from '../utils/ObjectUtil'

const fileService = new FileProvider()

export default {
    addFile: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            fileService.addFile(payload).then(v => {
                const { status, data, errorMessage } = v
                
                if (status === API_STATUS_SUCCESS) {
                    const obj = convertFileObjFromServerToApp(data.result)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }

            }).catch(error => {
                console.log('error', error)
                reject('Error, Maybe file\'s size is maximum than 5mb.')
            })
        })
    }),
    setFilesRefId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            fileService.setFilesRefId(payload).then(v => {
                const { status, data, errorMessage } = v
                
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }

            }).catch(error => {
                reject(error.message)
            })
        })

    }),
    getFileList: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            fileService.getFileList(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeFile: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            fileService.removeFile(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeFiles: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            fileService.removeFiles(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
}