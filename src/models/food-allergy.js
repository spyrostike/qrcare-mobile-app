import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import FoodAllergyProvider from '../resources/food-allergy-provider'
import { convertDrugFromServerToApp } from '../utils/ObjectUtil'

const foodAllergyService = new FoodAllergyProvider()

export default {
    list: [],
    item: null,
    recordNumber: null,
    setItem: action((state, payload) => {
        state.item = payload    
    }),
    setList: action((state, payload) => {
        state.list = payload    
    }),
    setRecordNumber: action((state, payload) => {
        state.recordNumber = payload    
    }),
    addFoodAllergy: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            foodAllergyService.addFoodAllergy(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editFoodAllergy: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            foodAllergyService.editFoodAllergy(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getFoodAllergyById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            foodAllergyService.getFoodAllergyById(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    
                    const obj = convertDrugFromServerToApp(data.result)
                    actions.setItem(obj)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getFoodAllergyListByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            foodAllergyService.getFoodAllergyListByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setList(data.result)
                    actions.setRecordNumber(data.result.length)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    countFoodAllergyByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            foodAllergyService.countFoodAllergyByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setRecordNumber(data.result)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeFoodAllergy: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            foodAllergyService.removeFoodAllergy(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeFoodAllergies: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            foodAllergyService.removeFoodAllergies(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    })
}