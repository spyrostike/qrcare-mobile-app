import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import VaccineProvider from '../resources/vaccine-provider'
import { convertVaccineBookFromServerToApp, convertVaccineFromServerToApp } from '../utils/ObjectUtil'

const vaccineService = new VaccineProvider()

export default {
    bookList: [],
    book: null,
    list: [],
    item: null,
    setBookList: action((state, payload) => {
        state.bookList = payload    
    }),
    setBook: action((state, payload) => {
        state.book = payload    
    }),
    setList: action((state, payload) => {
        state.list = payload    
    }),
    setItem: action((state, payload) => {
        state.item = payload    
    }),
    addVaccineBook: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.addVaccineBook(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editVaccineBook: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.editVaccineBook(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getVaccineBookListByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.getVaccineBookListByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setBookList(data.result)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getVaccineBookById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.getVaccineBookById(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    
                    const obj = convertVaccineBookFromServerToApp(data.result)
                    actions.setBook(obj)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeVaccineBook: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.removeVaccineBook(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeVaccineBooks: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.removeVaccineBooks(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    addVaccine: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.addVaccine(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editVaccine: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.editVaccine(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getVaccineListByBookId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.getVaccineListByBookId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setList(data.result)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getVaccineById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.getVaccineById(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    
                    const obj = convertVaccineFromServerToApp(data.result)
                    actions.setBook(obj)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeVaccine: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            vaccineService.removeVaccine(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    })
}