import { action, thunk } from 'easy-peasy'
import { API_STATUS_SUCCESS } from '../../appConfig'
import DrugProvider from '../resources/drug-provider'
import { convertDrugFromServerToApp } from '../utils/ObjectUtil'

const drugService = new DrugProvider()

export default {
    list: [],
    item: null,
    recordNumber: null,
    initialState: action((state, payload) => {
        state.list = []
        state.item = null,
        state.recordNumber = null
    }),
    setItem: action((state, payload) => {
        state.item = payload    
    }),
    setList: action((state, payload) => {
        state.list = payload    
    }),
    setRecordNumber: action((state, payload) => {
        state.recordNumber = payload    
    }),
    addDrug: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugService.addDrug(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    editDrug: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugService.editDrug(payload).then(v => {
                const { status, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve()
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getDrugById: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugService.getDrugById(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    
                    const obj = convertDrugFromServerToApp(data.result)
                    actions.setItem(obj)
                    resolve(obj)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    getDrugListByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugService.getDrugListByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setList(data.result)
                    actions.setRecordNumber(data.result.length)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    countDrugByCreatureId: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugService.countDrugByCreatureId(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    actions.setRecordNumber(data.result)
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeDrug: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugService.removeDrug(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    }),
    removeDrugs: thunk(async (actions, payload) => {
        return new Promise((resolve, reject) => {
            drugService.removeDrugs(payload).then(v => {
                const { status, data, errorMessage } = v
                if (status === API_STATUS_SUCCESS) {
                    resolve(data.result)
                } else {
                    reject(errorMessage)
                }
            }).catch(error => {
                reject(error.message)
            })
        })
    })
}