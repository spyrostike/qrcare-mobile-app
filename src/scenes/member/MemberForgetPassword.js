import React, { useMemo, useState } from 'react'
import { Keyboard, SafeAreaView, StyleSheet, TouchableWithoutFeedback, View } from 'react-native'
import { Label, Text } from 'native-base'
import { useStoreActions } from 'easy-peasy'
import { ConfirmDialog } from 'react-native-simple-dialogs'
import ITextInputWithLabel from '../../components/UI/input/ITextInputWithLabel'
import IButton from '../../components/UI/button/IButton'
import ILoadingDialog from '../../components/UI/dialog/ILoadingDialog'
import IErrorDialog from '../../components/UI/dialog/IErrorDialog'
import Navigator from '../../services/Navigator'

export default (props) => {
    const [ email, setEmail ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ dialogMessage, setDialogMessage ] = useState(null)
    const [ errorMessage, setErrorMessage ] = useState(null)

    const forgetPasswordAction = useStoreActions(actions => actions.member.forgetPassword)

    const onSubmit = () => {
        console.log('ccc')
        if (!_validate()) return
        _forgetPassword()
    }

    const _forgetPassword = async () => {
        
        setIsLoading(true)
        
        const data = { 
            email: email
        }

        try {
            await forgetPasswordAction(data)
            setIsLoading(false)
            setDialogMessage('We have already sent new password to your email.')
            // Navigator.navigate('MemberCreatureList')
        } catch (error) {
            setIsLoading(false)
            setErrorMessage(error)
        }
    }

    const _validate = () => {
        if (email === null || email.trim().length === 0) { setErrorMessage('Email is required'); return false }
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) { setErrorMessage('Email is invalid'); return false }
        return true
    }

    const _onConfirm = () => {
        setDialogMessage(null)
        Navigator.back()
    }

    return (
        <SafeAreaView style={styles.container}>
            <TouchableWithoutFeedback style={styles.fullFlex} onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.fullFlex} >
                    <View style={styles.body}>
                        <Label style={[ styles.label, { fontSize: 18 } ]} >Forgot password</Label>
                        <Label style={[ styles.label, { fontSize: 12 } ]} >Please fill your email</Label>
                        <ITextInputWithLabel 
                            label='Email : ' 
                            value={email} 
                            onChangeText={setEmail} />
                    </View>
                    <View style={styles.footer}>
                        <IButton text='Submit' onPress={onSubmit} width={'50%'} />
                    </View>
                </View>
            </TouchableWithoutFeedback>
            <ILoadingDialog isLoading={isLoading} />
            <ConfirmDialog
                title='Success'
                visible={dialogMessage !== null}
                positiveButton={{
                    title: 'ok',
                    titleStyle: { color: '#000000' },
                    onPress: () => _onConfirm()
                }}
                animationType={'fade'}
                dialogStyle={{ borderRadius: 5 }} >
                <View>
                    <Text>{dialogMessage !== null ? dialogMessage: ''}</Text>
                </View>
            </ConfirmDialog>
            <IErrorDialog errorMessage={errorMessage} setErrorMessage={setErrorMessage} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    fullFlex: { flex: 1 },
    body: { flex: 0.9, padding: 15 },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' },
    label: { color: '#FFFFFF', marginVertical: 10 }
})