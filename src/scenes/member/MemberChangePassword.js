import React from 'react'
import MemberChangePasswordProvider from '../../components/member-change-password/MemberChangePasswordProvider'

export default () => {

    return (
        <MemberChangePasswordProvider />
    )
} 