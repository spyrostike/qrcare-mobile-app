import React from 'react'
import MemberAuthenticationProvider from '../../components/member-authentication/MemberAuthenticationProvider'

export default () => {

    return (
        <MemberAuthenticationProvider />
    )
}