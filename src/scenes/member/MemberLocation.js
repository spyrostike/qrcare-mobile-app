import React from 'react'
import MemberLocationProvider from '../../components/member-location/MemberLocationProvider'

export default () => {

    return (
        <MemberLocationProvider />
    )
} 