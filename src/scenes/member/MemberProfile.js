import React from 'react'
import MemberProfileProvider from '../../components/member-profile/MemberProfileProvider'

export default () => {

    return (
        <MemberProfileProvider />
    )
} 