import React from 'react'
import DrugListProvider from '../../components/drug-list/DrugListProvider'

export default () => {
    
    return (
        <DrugListProvider />
    )
} 