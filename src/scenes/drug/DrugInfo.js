import React from 'react'
import DrugInfoProvider from '../../components/drug-info/DrugInfoProvider'

export default () => {

    return (
        <DrugInfoProvider />
    )
} 