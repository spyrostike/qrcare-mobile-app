import React from 'react'
import SettingListProvider from '../../components/setting-list/SettingListProvider'

export default () => {
    
    return (
        <SettingListProvider />
    )
} 