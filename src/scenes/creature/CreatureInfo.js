import React from 'react'
import CreatureInfoProvider from '../../components/creature-info/CreatureInfoProvider'

export default () => {

    return (
        <CreatureInfoProvider />
    )
} 