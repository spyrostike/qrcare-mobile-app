import React from 'react'
import CreaturePreviewProvider from '../../components/creature-preview/CreaturePreviewProvider'

export default () => {

    return (
        <CreaturePreviewProvider />
    )
} 