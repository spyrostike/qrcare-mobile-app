import React from 'react'
import CreatureTMEntranceProvider from '../../components/creature-tm-entrance/CreatureTMEntranceProvider'

export default () => {

    return (
        <CreatureTMEntranceProvider />
    )
}