import React from 'react'
import CreatureVCREntranceProvider from '../../components/creature-vcr-entrance/CreatureVCREntranceProvider'

export default () => {

    return (
        <CreatureVCREntranceProvider />
    )
}