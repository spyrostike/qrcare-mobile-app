import React from 'react'
import CreatureListProvider from '../../components/creature-list/CreatureListProvider'

export default () => {

    return (
        <CreatureListProvider />
    )
} 