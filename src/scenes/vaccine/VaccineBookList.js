import React from 'react'
import VaccineBookListProvider from '../../components/vaccine-book-list/VaccineBookListProvider'

export default () => {

    return (
        <VaccineBookListProvider />
    )
}