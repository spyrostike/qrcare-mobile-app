import React from 'react'
import VaccineInfoProvider from '../../components/vaccine-info/VaccineInfoProvider'

export default () => {

    return (
        <VaccineInfoProvider />
    )
}