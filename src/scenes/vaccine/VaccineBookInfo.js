import React from 'react'
import VaccineBookInfoProvider from '../../components/vaccine-book-info/VaccineBookInfoProvider'

export default () => {

    return (
        <VaccineBookInfoProvider />
    )
}