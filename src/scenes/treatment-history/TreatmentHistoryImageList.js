import React from 'react'
import TreatmentHistoryImageListProvider from '../../components/treatment-history-image-list/TreatmentHistoryImageListProvider'

export default () => {

    return (
        <TreatmentHistoryImageListProvider />
    )
}