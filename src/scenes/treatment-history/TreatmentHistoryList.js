import React from 'react'
import TreatmentHistoryListProvider from '../../components/treatment-history-list/TreatmentHistoryListProvider'

export default () => {

    return (
        <TreatmentHistoryListProvider />
    )
}