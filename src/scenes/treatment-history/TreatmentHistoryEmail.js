import React from 'react'
import TreatmentHistoryEmailProvider from '../../components/treatment-history-email/TreatmentHistoryEmailProvider'

export default () => {

    return (
        <TreatmentHistoryEmailProvider />
    )
}