import React from 'react'
import TreatmentHistoryInfoProvider from '../../components/treatment-history-info/TreatmentHistoryInfoProvider'

export default () => {

    return (
        <TreatmentHistoryInfoProvider />
    )
}