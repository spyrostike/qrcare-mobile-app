import React from 'react'
import FoodAllergyListProvider from '../../components/food-allergy-list/FoodAllergyListProvider'

export default () => {
    
    return (
        <FoodAllergyListProvider />
    )
} 