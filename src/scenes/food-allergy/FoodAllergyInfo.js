import React from 'react'
import FoodAllergyInfoProvider from '../../components/food-allergy-info/FoodAllergyInfoProvider'

export default () => {

    return (
        <FoodAllergyInfoProvider />
    )
} 