import React from 'react'
import MedicalInfoProvider from '../../components/medical-info/MedicalInfoProvider'

export default () => {

    return (
        <MedicalInfoProvider />
    )
} 