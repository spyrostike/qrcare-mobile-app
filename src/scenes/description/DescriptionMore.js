import React from 'react'
import { SafeAreaView, ScrollView, StyleSheet, View, Linking } from 'react-native'
import { Label, Text } from 'native-base'
import ParsedText from 'react-native-parsed-text'
import IButton from '../../components/UI/button/IButton'
import Navigator from '../../services/Navigator'

export default (props) => {
    const { routes, index } = Navigator.getCurrentRoute()
    const { description } = routes[index].params

    const _handleUrlPress = (url, matchIndex) => Linking.openURL(url)

    return (
        <SafeAreaView  style={styles.container} >
            <View style={{ flexl: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                <Label style={styles.label}>Description</Label>
            </View>
            <ScrollView style={styles.scroll} >
                <ParsedText
                    style={{ color: '#FFFFFF', fontSize: 14 }}
                    parse={ [ {type: 'url', style: styles.url, onPress: _handleUrlPress} ]}
                    childrenProps={{allowFontScaling: false}}>
                        { description }
                </ParsedText>
            </ScrollView>
            <View style={styles.footer} >
                <IButton text='Done' onPress={() => Navigator.back()} width={'50%'} />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#000000' },
    scroll: { 
        flex: 0.8, 
        paddingHorizontal: 20
    },
    footer: { flex: 0.1, flexDirection: 'row', justifyContent: 'space-around' },
    label: { color: '#FFFFFF', marginVertical: 10 },
    url: {
        color: '#3BB9FF',
        textDecorationLine: 'underline',
    }
})