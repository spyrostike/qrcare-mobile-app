import React from 'react'

import ICamera from '../../components/UI/camera/ICamera'
import Navigator from '../../services/Navigator'

export default () => {
    const { routes, index } = Navigator.getCurrentRoute()
    const { takePicture, barcodeFinderVisible, onBarCodeRead, onClose } = routes[index].params
    return (
        <ICamera 
            reverseCamera={true} 
            takePicture={takePicture} 
            barcodeFinderVisible={barcodeFinderVisible} 
            onBarCodeRead={onBarCodeRead}
            onClose={onClose} />
    )
}