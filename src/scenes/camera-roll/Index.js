import React from 'react'

import ICameraRoll from '../../components/UI/camera-roll/ICameraRoll'
import Navigator from '../../services/Navigator'

export default () => {
    const { routes, index } = Navigator.getCurrentRoute()
    const { selectSingleItem, onSelect } = routes[index].params
    
    return (
        <ICameraRoll 
            selectSingleItem={selectSingleItem} 
            onSelect={onSelect} />
    )
}