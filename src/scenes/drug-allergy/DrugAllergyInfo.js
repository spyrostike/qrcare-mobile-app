import React from 'react'
import DrugAllergyInfoProvider from '../../components/drug-allergy-info/DrugAllergyInfoProvider'

export default () => {

    return (
        <DrugAllergyInfoProvider />
    )
} 