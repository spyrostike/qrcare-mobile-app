import React from 'react'
import DrugAllergyListProvider from '../../components/drug-allergy-list/DrugAllergyListProvider'

export default () => {
    
    return (
        <DrugAllergyListProvider />
    )
} 