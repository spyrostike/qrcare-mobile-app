export const API_ENDPOINT = 'https://qrcare-official.com/api'
// export const API_ENDPOINT = 'http://172.20.10.4:3002/api'

export const API_IMAGE_PATH = 'https://qrcare-official.com/uploads'
export const API_VIDEO_PATH = 'https://qrcare-official.com/uploads/video'
export const API_STATUS_SUCCESS = '00'
export const API_STATUS_ERROR = '10'

export const DOMAIN_URL = 'https://qrcare-official.com'
